//
//  LibraryController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 9/2/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import Photos

protocol LibraryControllerDelegate {
    func reloadAssets()
}

class LibraryController: NSObject, PHPhotoLibraryChangeObserver {
    
    static var shared = LibraryController()
    
    var images: PHFetchResult<PHAsset>?
    var imageManager: PHCachingImageManager?
    var phAsset: PHAsset?
    var selectedImages: [UIImage] = []
    var selectedAssets: [PHAsset] = []
    var delegate: LibraryControllerDelegate?
    
    //MARK: - Methods
    
    func initialize() {
        
        if images != nil { return }
        
        // Never load photos Unless the user allows to access to photo album
        checkPhotoAuth()
        
        getImages()
        
        PHPhotoLibrary.shared().register(self)
    }
    
    deinit {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            PHPhotoLibrary.shared().unregisterChangeObserver(self)
        }
    }
    
    //MARK: - Private 
    
    private func getImages() {
        // Sorting condition
        let options = PHFetchOptions()
        options.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        
        images = PHAsset.fetchAssets(with: .image, options: options)
        let count = images?.count ?? 0
        if count > 0 {
            guard let image = images?[0] else { return }
            changeImage(image)
        }
    }
    
    private func checkPhotoAuth() {
        PHPhotoLibrary.requestAuthorization { (status) -> Void in
            switch status {
            case .authorized:
                self.imageManager = PHCachingImageManager()
                self.getImages()
                self.delegate?.reloadAssets()
            case .restricted, .denied:
                break
            default:
                break
            }
        }
    }
    
    private func changeImage(_ asset: PHAsset) {
        phAsset = asset
        DispatchQueue.global(qos: .default).async(execute: {
            let options = PHImageRequestOptions()
            options.isNetworkAccessAllowed = true
            self.imageManager?.requestImage(for: asset, targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight), contentMode: .aspectFill, options: options) { result, info in
                DispatchQueue.main.async(execute: {
                    if let result = result, !self.selectedAssets.contains(asset) {
                        self.selectedAssets.append(asset)
                        self.selectedImages.append(result)
                    }
                })
            }
        })
    }
    
    
    //MARK: - PHPhotoLibraryChangeObserver
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async {
            guard let images = self.images else { return }
            guard let collectionChanges = changeInstance.changeDetails(for: images) else { return }
            
            self.selectedImages.removeAll()
            self.selectedAssets.removeAll()
            self.images = collectionChanges.fetchResultAfterChanges
            
            if collectionChanges.hasIncrementalChanges || collectionChanges.hasMoves {
                self.delegate?.reloadAssets()
            }
        }
    }

}
