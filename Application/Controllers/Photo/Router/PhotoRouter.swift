//
//  PhotoRouter.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/12/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import Alamofire

enum PhotoRouter: Router {
    
    case getPhotos
    case uploadPhoto
    
    var query: APIQuery {
        switch self {
        case .getPhotos:
            return APIQuery(httpMethod: .get, path: "v1/data/photo")
        case .uploadPhoto:
            return APIQuery(httpMethod: .post, path: "v1/files/media/temp.png", parameters: ["overwrite":true as AnyObject])
        }
    }
    
    var config: APIConfig {
        switch self {
        default:
            return .default
        }
    }
}
