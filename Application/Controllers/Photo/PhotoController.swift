//
//  PhotoController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/12/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import Alamofire
//import ObjectMapper

struct PhotoController {
    
    private static let dataStore = Backendless.sharedInstance().data.of(Photo.ofClass())

    static func getAllPhotos(_ completion: @escaping AppCompletion) {
        dataStore?.find({ response in
            guard let photos = response as? [Photo] else { return }
            completion(photos, nil)
            return
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getPhotosByCoordinates(_ coordinates: CLLocationCoordinate2D, distance: Double, completion: @escaping AppCompletion) {
        let whereClause = "distance(\(coordinates.latitude), \(coordinates.longitude), location.latitude, location.longitude ) < km(\(distance))"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(1)
        
        dataStore?.find(dataQuery, response: { response in
            guard var photos = response as? [Photo] else { return }
        
            ReportUserController.getReportedUsers(with: User.current, completion: { (result, error) in
                guard let reportedUsers = result as? [ReportUser] else {
                    debugPrint(error as Any)
                    completion(nil, error)
                    return
                }
                
                for reported in reportedUsers {
                    let indexes = photos.enumerated().flatMap { $0.element.user?.objectId == reported.userReported?.objectId ? $0.offset : nil }
                    for index in indexes.reversed() {
                        photos.remove(at: index)
                    }
                }
                
                ReportPostController.getReportedPosts(with: User.current, completion: { (result, error) in
                    guard let reportedPosts = result as? [ReportPost] else {
                        debugPrint(error as Any)
                        completion(nil, error)
                        return
                    }
                    
                    for reported in reportedPosts {
                        let indexes = photos.enumerated().flatMap { $0.element.objectId == reported.photo?.objectId ? $0.offset : nil }
                        for index in indexes.reversed() {
                            photos.remove(at: index)
                        }
                    }
                    
                    print("No error getting photos")
                    completion(photos, nil)
                    return
                })
            })
        }, error: { error in
            print("Error 3")
            debugPrint(error as Any)
            completion(nil, error)
            return
        })
    }
    
    static func uploadPhoto(_ data: Data, name: String, completion: @escaping AppCompletion) {
        Backendless.sharedInstance().fileService.saveFile("media/"+name+".png", content: data, response: { (response) in
            debugPrint(response as Any)
            guard let fileURL = response?.fileURL else { return }
            completion(fileURL, nil)
            return
        }) { (error) in
            debugPrint(error as Any)
            completion(nil, error)
            return
        }
    }
    
    static func uploadVideo(_ data: Data, name: String, completion: @escaping AppCompletion) {
        Backendless.sharedInstance().fileService.saveFile("media/"+name+".mov", content: data, response: { (response) in
            debugPrint(response as Any)
            guard let fileURL = response?.fileURL else { return }
            completion(fileURL, nil)
            return
        }) { (error) in
            debugPrint(error as Any)
            completion(nil, error)
            return
        }
    }
    
    static func uploadVoiceNote(_ data: Data, name: String, completion: @escaping AppCompletion) {
        Backendless.sharedInstance().fileService.saveFile("media/voicenotes/"+name+".m4a", content: data, response: { response in
            debugPrint(response as Any)
            guard let fileURL = response?.fileURL else { return }
            completion(fileURL, nil)
            return
        }, error: { error in
            debugPrint(error as Any)
            completion(nil, error)
            return
        })
    }
    
    static func savePhotoWithUser(_ photo: Photo, completion: @escaping AppCompletion) {
        guard let location = photo.location else { return }
        photo.location = nil
        
        dataStore?.save(photo, response: { savedPhoto in
            let photo = savedPhoto as! Photo
            let currentUser = User.current
            dataStore?.setRelation("user", parentObjectId: photo.objectId, childObjects: [currentUser.objectId], response: { response in
                let geoPoint = location
                let savedGeopoint = Backendless.sharedInstance().geoService.save(geoPoint)!
                let _ = dataStore?.setRelation("location", parentObjectId: photo.objectId, childObjects: [savedGeopoint.objectId])
                completion(true, nil)
                return
            }, error: { error in
                debugPrint(error as Any)
                completion(false, error)
                return
            })
        }, error: { error in
            debugPrint(error as Any)
            completion(false, error)
            return
        })
        
    }
    
    static func getPhotosByCurrentUser(_ completion: @escaping AppCompletion) {
        let objectId = "\(User.current.objectId!)"
        let whereClause = "user.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let photos = response as? [Photo] else { return }
            completion(photos, nil)
            return
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getPhotos(with user: BackendlessUser, completion: @escaping AppCompletion) {
        let objectId = "\(user.objectId!)"
        let whereClause = "user.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(1)
        
        dataStore?.find(dataQuery, response: { response in
            guard var photos = response as? [Photo] else { return }
            
            ReportPostController.getReportedPosts(with: User.current, completion: { (result, error) in
                guard let reportedPosts = result as? [ReportPost] else {
                    debugPrint(error as Any)
                    completion(nil, error)
                    return
                }
                
                for reported in reportedPosts {
                    let indexes = photos.enumerated().flatMap { $0.element.objectId == reported.photo?.objectId ? $0.offset : nil }
                    for index in indexes.reversed() {
                        photos.remove(at: index)
                    }
                }
                
                print("No error getting photos")
                completion(photos, nil)
                return
            })
            
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getPhoto(with object: Photo, completion: @escaping AppCompletion) {
        dataStore?.find(byId: object.objectId, response: { (object) in
            guard let photo = object as? Photo else { return }
            completion(photo, nil)
            return
        }, error: { (error) in
            completion(nil, error)
            return
        })
    }
    
    static func likePhoto(with object: Photo, completion: @escaping AppCompletion) {
        dataStore?.save(object, response: { response in
            debugPrint(response as Any)
            completion(true, nil)
            return
        }, error: { (error) in
            completion(false, error)
            return
        })
    }
    
    static func deletePhoto(with object: Photo, completion: @escaping AppCompletion) {
        let geopoint = object.location
        dataStore?.remove(object, response: { (response) in
            debugPrint(response as Any)
            if geopoint != nil {
                Backendless.sharedInstance().geoService.remove(geopoint!, response: { (repsonse) in
                    debugPrint(response as Any)
                }, error: { (error) in
                    debugPrint(error?.description ?? "")
                })
            }
            completion(true, nil)
            return
        }, error: { (error) in
            debugPrint(error.debugDescription)
            completion(false, error)
            return
        })
    }

}
