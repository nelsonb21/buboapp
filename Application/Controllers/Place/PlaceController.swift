//
//  PlaceController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct PlaceController {
    
    static var likePlaces: [Place] = []
    private static let dataStore = Backendless.sharedInstance().data.of(Place.ofClass())
    
    static func bookmarkPlace(place: Place, completion: @escaping AppCompletion) {
        guard let location = place.coordinates else { return }
        place.coordinates = nil
        
        dataStore?.save(place, response: { response in
            debugPrint(response ?? "")
            guard let place = response as? Place else { completion(false, nil); return }
            let currentUser = User.current
            
            Backendless.sharedInstance().geoService.save(location, response: { (savedGeopoint) in
                guard let savedGeopoint = savedGeopoint else { return }
                let _ = dataStore?.setRelation("coordinates", parentObjectId: place.objectId, childObjects: [savedGeopoint.objectId])
                let _ = dataStore?.setRelation("user", parentObjectId: place.objectId, childObjects: [currentUser.objectId])
                getPlaces(with: currentUser, completion: { (_, _) in })
                completion(place, nil)
                return
            }, error: { (error) in
                print(error.debugDescription)
                deletePlace(with: place, completion: { (_, _) in })
                completion(false, error)
                return
            })
        }, error: { (error) in
            debugPrint(error as Any)
            completion(false, error)
            return
        })
    }
    
    static func getPlaces(with user: BackendlessUser, completion: @escaping AppCompletion) {
        let objectId = "\(user.objectId!)"
        let whereClause = "user.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let places = response as? [Place] else { return }
            self.likePlaces = places
            completion(places, nil)
            return
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getPlace(with user: BackendlessUser, coordinates: CLLocationCoordinate2D, completion: @escaping AppCompletion) {
        let latitude = "\(coordinates.latitude)"
        let longitude = "\(coordinates.longitude)"
        let objectId = "\(user.objectId!)"
        
        let whereClause = "user.objectId='"+objectId+"' AND coordinates.latitude='"+latitude+"' AND coordinates.longitude='"+longitude+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let places = response as? [Place] else { return }
            completion(places, nil)
            return
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func deletePlace(with place: Place, completion: @escaping AppCompletion) {
        dataStore?.remove(place, response: { (number) in
            let index = self.likePlaces.index(where: { (placeToRemove) -> Bool in
                placeToRemove.objectId == place.objectId
            })
            if index != nil { self.likePlaces.remove(at: index!) }
            
            completion(number, nil)
        }, error: { (error) in
            debugPrint(error as Any)
            completion(nil, error)
        })
    }

}
