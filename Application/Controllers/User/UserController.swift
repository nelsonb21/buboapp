//
//  UserController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/27/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import Alamofire

enum UserKeys: String {
    case username
    case profileImage
    case userID
}

struct UserController {
    
    private static let userService = Backendless.sharedInstance().userService
    
    static func logUserIn(with email: String, password: String, completion: @escaping AppCompletion) {
        userService?.login(email, password: password, response: { backendlessUser in
            guard let user = backendlessUser else { return }
            print("User has been logged in (ASYNC): \(user)")
            User.updateUserLocally(user: user)
            completion(user, nil)
            return
        }, error: { error in
            debugPrint(error ?? "Fail to Login")
            completion(nil, error)
            return
        })
    }
    
    static func logIn(with facebookUser: String, token: String, expiration: Date, fields: [String: String], completion: @escaping AppCompletion) {
        userService?.login(withFacebookSDK: facebookUser, tokenString: token, expirationDate: expiration, fieldsMapping: fields, response: { (backendlessUser) in
            guard let user = backendlessUser else { return }
            print("User has been logged in (ASYNC) with FACEBOOK: \(user)")
            User.updateUserLocally(user: user)
            self.updateUser(with: user)
            completion(user, nil)
            return
        }, error: { (error) in
            debugPrint(error ?? "Fail to Login with FACEBOOK")
            completion(nil, error)
            return
        })
    }
    
    static func signUserUp(with user: BackendlessUser, completion: @escaping AppCompletion) {
        userService?.register(user, response: { (registeredUser) in
            guard let user = registeredUser else { return }
            print("User has been signup")
            User.updateUserLocally(user: user)
            completion(user, nil)
            return
        }, error: { (error) in
            debugPrint(error ?? "Fail to SignUp")
            completion(nil, error)
            return
        })
    }
    
    static func updateUser(with user: BackendlessUser, completion: @escaping AppCompletion) {
        userService?.update(user, response: { (user) in
            guard let user = user else { return }
            User.updateUserLocally(user: user)
            completion(user, nil)
            return
        }) { (error) in
            print("Error in updateUser with completion: " + error.debugDescription)
            completion(nil, error)
            return
        }
    }
    
    static func updateUser(with user: BackendlessUser) {
        
        let properties = [User.Attributes.bio.rawValue: "",
                          User.Attributes.website.rawValue: "",
                          User.Attributes.profileImageUrl.rawValue: (self.getProfileImage() ?? ""),
                          User.Attributes.osType.rawValue: "iOS",
                          User.Attributes.osVersion.rawValue: UIDevice.current.systemVersion,
                          User.Attributes.phoneModel.rawValue: UIDevice().modelName,
                          User.Attributes.appVersion.rawValue: Bundle.main.fullAppVersion]
        
        user.updateProperties(properties)
        
        userService?.update(user, response: { (user) in
            guard let user = user else { return }
            User.updateUserLocally(user: user)
        }) { (error) in
            print("Error in updateUser: " + error.debugDescription)
        }
    }
    
    static func updateUserPhoto(with data: Data, name: String, completion: @escaping AppCompletion) {
        Backendless.sharedInstance().fileService.saveFile("profileImages/"+name+".png", content: data, overwriteIfExist: true, response: { (response) in
            guard let fileURL = response?.fileURL else { return }
            completion(fileURL, nil)
            return
        }) { (error) in
            debugPrint(error as Any)
            completion(nil, error)
            return
        }
    }
    
    static func logUserOut(completion: @escaping AppCompletion) {
        userService?.logout({ (_) in
            KeychainWrapper.removeAll()
            User.deleteUserData()
            print("User logged out.")
            completion(true, nil)
            return
        }, error: { (error) in
            print("Server reported an error: \(error?.detail ?? "" )")
            completion(nil, error)
            return
        })
    }
    
    static func showUser(with email: String, completion: @escaping AppCompletion) {
        var whereDic = [String: Any]()
        var parameters = [String: Any]()
        parameters["email"] = email
        whereDic["where"] = "email='\(email)'"
        
        Alamofire.request(UserRouter.getUser(parameters: whereDic)).responseJSON { (response) in
            do {
                guard let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String: Any] else { return }
                guard let data = json["data"] as? [[String: Any]] else { completion(false, nil); return }
                guard let _ = data.first?["email"] as? String else { completion(false, nil); return }
                completion(true, nil)
                return
            } catch {
                print(error.localizedDescription)
                completion(false, nil)
            }
        }
    }
    
    static func setupProfileImage(with url: String) {
        let profileImage = UserDefaultWrapper.user.value(forKey: UserKeys.profileImage.rawValue) as? String
        if profileImage == nil { UserDefaultWrapper.user.set(url, forKey: UserKeys.profileImage.rawValue) }
    }
    
    static func setupNewProfileImage(with url: String) {
        UserDefaultWrapper.user.set(url, forKey: UserKeys.profileImage.rawValue)
    }
    
    static func getProfileImage() -> String? {
        guard let profileImage = UserDefaultWrapper.user.value(forKey: UserKeys.profileImage.rawValue) as? String else { return nil }
        return profileImage
    }
    
    static func resetPassword(with email: String, completion: @escaping AppCompletion) {
        userService?.restorePassword(email, response: { (result) in
            print("Check your email address! result = \(result ?? "nil")")
            completion(result, nil)
            return
        }, error: { (error) in
            print("Server reported an error: \(error?.detail ?? "" )")
            completion(nil, error)
            return
        })
    }

}
