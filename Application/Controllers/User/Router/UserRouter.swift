//
//  UserRouter.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 6/14/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import Alamofire

enum UserRouter: Router {
    
    case getUser(parameters: [String: Any])
    
    var query: APIQuery {
        switch self {
        case .getUser(let parameters):
            return APIQuery(httpMethod: .get, path: "data/Users", parameters: parameters)
        }
    }
    
    var config: APIConfig {
        switch self {
        default:
            return .default
        }
    }
}
