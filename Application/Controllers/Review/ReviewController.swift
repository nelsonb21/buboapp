//
//  ReviewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 5/6/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import StoreKit
import Armchair

class ReviewController: NSObject {
    
    static let shared = ReviewController()
    
    func reviewApp() {
        if #available( iOS 10.3,*){
            SKStoreReviewController.requestReview()
        } else {
            showReviewApp()
        }
    }
    
    func showReviewApp() {
        Armchair.appID("1294428250")
        Armchair.debugEnabled(true)
        Armchair.opensInStoreKit(false)
        Armchair.userDidSignificantEvent(true)
    }
    
}
