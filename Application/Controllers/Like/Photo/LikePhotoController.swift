//
//  LikePhotoController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/22/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct LikePhotoController {
    
    static var likePhoto: [LikePhoto] = []
    private static let dataStore = Backendless.sharedInstance().data.of(LikePhoto.ofClass())
    
    static func likePhoto(like: LikePhoto, completion: @escaping AppCompletion) {
        guard let photoID = like.photo?.objectId else { return }
        like.photo = nil
        
        dataStore?.save(like, response: { response in
            guard let likePhoto = response as? LikePhoto else { completion(false, nil); return }
            let currentUser = User.current
            
            let _ = dataStore?.setRelation("user", parentObjectId: likePhoto.objectId, childObjects: [currentUser.objectId])
            let _ = dataStore?.setRelation("photo", parentObjectId: likePhoto.objectId, childObjects: [photoID])
            
            self.getLikes(with: currentUser)
            debugPrint(response as Any)
            completion(true, nil)
            return
        }, error: { (error) in
            debugPrint(error as Any)
            completion(false, error)
            return
        })
    }
    
    static func getLikes(with user: BackendlessUser, completion: @escaping AppCompletion) {
        let objectId = "\(user.objectId!)"
        let whereClause = "user.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard var likes = response as? [LikePhoto] else { return }
            
            ReportPostController.getReportedPosts(with: User.current, completion: { (result, error) in
                guard let reportedPosts = result as? [ReportPost] else {
                    debugPrint(error as Any)
                    completion(nil, error)
                    return
                }
                
                for reported in reportedPosts {
                    let indexes = likes.enumerated().flatMap { $0.element.photo?.objectId == reported.photo?.objectId ? $0.offset : nil }
                    for index in indexes.reversed() {
                        likes.remove(at: index)
                    }
                }
                self.likePhoto = likes
                completion(likes, nil)
                return
            })
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getLikes(with user: BackendlessUser) {
        let objectId = "\(user.objectId!)"
        let whereClause = "user.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return}
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let likes = response as? [LikePhoto] else { return }
            self.likePhoto = likes
            return
        }, error: { _ in
            return
        })
    }
    
    static func deleteLike(with like: LikePhoto, completion: @escaping AppCompletion) {
        dataStore?.remove(like, response: { (number) in
            let index = self.likePhoto.index(where: { (likePhotoToRemove) -> Bool in
                likePhotoToRemove.objectId == like.objectId
            })
            if index != nil { self.likePhoto.remove(at: index!) }
            
            completion(number, nil)
        }, error: { (error) in
            debugPrint(error as Any)
            completion(nil, error)
        })
    }

}
