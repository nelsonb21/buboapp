//
//  LikePeopleController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct LikePeopleController {

    static var likePeople: [LikePeople] = []
    private static let dataStore = Backendless.sharedInstance().data.of(LikePeople.ofClass())
    
    static func likePeople(like: LikePeople, completion: @escaping AppCompletion) {
        guard let userLikedID = like.userLiked?.objectId else { return }
        like.userLiked = nil
        
        dataStore?.save(like, response: { response in
            guard let likePeople = response as? LikePeople else { completion(false, nil); return }
            let currentUser = User.current
            
            let _ = dataStore?.setRelation("userLiking", parentObjectId: likePeople.objectId, childObjects: [currentUser.objectId])
            let _ = dataStore?.setRelation("userLiked", parentObjectId: likePeople.objectId, childObjects: [userLikedID])
            
            debugPrint(response as Any)
            self.getLikes(with: currentUser)
            completion(true, nil)
            return
        }, error: { (error) in
            debugPrint(error as Any)
            completion(false, error)
            return
        })
    }
    
    static func getLikes(with user: BackendlessUser, completion: @escaping AppCompletion) {
        let objectId = "\(user.objectId!)"
        let whereClause = "userLiking.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard var likes = response as? [LikePeople] else { return }
            
            ReportUserController.getReportedUsers(with: User.current, completion: { (result, error) in
                guard let reportedUsers = result as? [ReportUser] else {
                    debugPrint(error as Any)
                    completion(nil, error)
                    return
                }
                
                for reported in reportedUsers {
                    let indexes = likes.enumerated().flatMap { $0.element.userLiked?.objectId == reported.userReported?.objectId ? $0.offset : nil }
                    for index in indexes.reversed() {
                        likes.remove(at: index)
                    }
                }
                self.likePeople = likes
                completion(likes, nil)
                return
            })
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getLikes(with user: BackendlessUser) {
        let objectId = "\(user.objectId!)"
        let whereClause = "userLiking.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let likes = response as? [LikePeople] else { return }
            self.likePeople = likes
            return
        }, error: { _ in
            return
        })
    }
    
    static func deleteLike(with like: LikePeople, completion: @escaping AppCompletion) {
        dataStore?.remove(like, response: { (number) in
            let index = self.likePeople.index(where: { (likePeopleToRemove) -> Bool in
                likePeopleToRemove.objectId == like.objectId
            })
            if index != nil { self.likePeople.remove(at: index!) }
            
            completion(number, nil)
        }, error: { (error) in
            debugPrint(error as Any)
            completion(nil, error)
        })
    }
    
}
