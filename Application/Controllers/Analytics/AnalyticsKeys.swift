//
//  AnalyticsKeys.swift
//  Bubo
//
//  Created by Nelson Bolivar on 9/21/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

enum AnalyticsKeys: String {
    
    case appStarted = "App Opened"
    
    case onboarding = "Onboarding - Opened"
    
    case signUpOpened = "Sign Up - Opened"
    case signUpCompleted = "Sign Up - Completed"
    case signUpLegal = "Sign Up - Legal Opened"
    
    case signInOpened = "Sign In - Opened"
    case signInCompleted = "Sign In - Completed"
    case signInWithFacebookCompleted = "Sign In With Facebook - Completed"
    
    case forgotPasswordOpened = "Forgot Password - Opened"
    case forgotPasswordCompleted = "Forgot Password - Completed"
    
    case augmentedRealityOpened = "Augmented Reality Mode - Opened"
    case augmentedRealityGalleryOpened = "Augmented Reality Mode - Gallery Opened"
    case augmentedRealityRefresh = "Augmented Reality Mode - Refresh"
    case augmentedRealityImagePickerOpened = "Augmented Reality Mode - Image Picker Opened"
    
    case mapModeOpened = "Map Mode - Opened"
    case mapModeGalleryOpened = "Map Mode - Gallery Opened"
    case mapModeRefresh = "Map Mode - Refresh"
    case mapModeImagePickerOpened = "Map Mode - Image Picker Opened"
    case mapModeSearchSelected = "Map Mode - Search Result Selected"
    case mapModeGalleryAddFavoritePlace = "Map Mode - Add Favorite place from Gallery"
    case mapModeGalleryRemoveFavoritePlace = "Map Mode - Remove Favorite place from Gallery"
    
    case photoDetailOpened = "Photo Detail - Opened"
    case photoDetailLikePlace = "Photo Detail - Like Place"
    case photoDetailDislikePlace = "Photo Detail - Dislike Place"
    case photoDetailAddressPress = "Photo Detail - Collection of photos around an address opened"
    case photoDetailProfileOpened = "Photo Detail - Profile Opened"
    case photoDetailLikePhoto = "Photo Detail - Photo Liked"
    case photoDetailDislikePhoto = "Photo Detail - Photo Disliked"
    case photoDetailAudioPlayed = "Photo Detail - Audio Played"
    case photoDetailEditOpened = "Photo Detail - Edit Opened"
    case photoDetailEditCompleted = "Photo Detail - Edit Completed"
    case photoDetailDeleteCompleted = "Photo Detail - Delete Completed"
    case photoDetailReportPost = "Photo Detail - Report Post"
    
    case profileOpened = "Profile - Opened"
    case profileMyPhotosOpened = "Profile - My Photos Opened"
    case profileFavoritePlacesOpened = "Profile - Favorite Places Opened"
    case profileFollowedPeopleOpened = "Profile - Followed People Opened"
    case profileFavoritePhotosOpened = "Profile - Favorite Photos Opened"
    case profilePhotoOpened = "Profile - Photo Opened"
    case profilePlaceOpened = "Profile - Favorite Place Opened"
    case profilePeopleOpened = "Profile - Followed People profile opened"
    case profileEditOpened = "Profile - Edit Opened"
    case profileEditCompleted = "Profile - Edit Completed"
    case profileLikePlace = "Profile - Like Place"
    case profileDislikePlace = "Profile - Dislike Place"
    case profileFollowUser = "Profile - Follow User"
    case profileUnfolloweUser = "Profile - Unfollow User"
    case profileReportUserSpam = "Profile - Report User Spam"
    case profileReportUserInappropriate = "Profile - Report User Inappropriate"
    
    case settingsOpened = "Settings - Opened"
    case settingsShare = "Settings - Share"
    case settingsRateUs = "Settings - Rate Us"
    case settingsContactUs = "Settings - Contact Us"
    case settingsLegal = "Settings - Legal Opened"
    case settingsLogout = "Settings - Logout"
    
    case photoCreateOpened = "Photo Created - Opened"
    case photoCreateIsVideo = "Photo Created - Video"
    case photoCreateIsImage = "Photo Created - Image"
    case photoCreateHideAddress = "Photo Created - Hide Address"
    case photoCreateHasAudio = "Photo Created - With Audio"
    case photoCreatedPosted = "Photo Created - Posted"
    
}

struct Analytics {
    
    static func logEvent(with key: AnalyticsKeys) {
        Flurry.logEvent(key.rawValue)
    }
    
}
