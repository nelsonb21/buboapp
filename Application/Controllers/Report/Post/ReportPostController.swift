//
//  ReportPostController.swift
//  Bubo
//
//  Created by Nelson Bolivar on 1/3/18.
//  Copyright © 2018 Nelson Bolivar. All rights reserved.
//

import UIKit

struct ReportPostController {
    
    static var reported: [ReportPost] = []
    private static let dataStore = Backendless.sharedInstance().data.of(ReportPost.ofClass())
    
    static func reportPost(post: ReportPost, completion: @escaping AppCompletion) {
        guard let photoID = post.photo?.objectId else { return }
        post.photo = nil
        
        dataStore?.save(post, response: { (response) in
            guard let reportedPost = response as? ReportPost else { completion(false, nil); return }
            let currentUser = User.current
            
            let _ = dataStore?.setRelation("reportedByUser", parentObjectId: reportedPost.objectId, childObjects: [currentUser.objectId])
            let _ = dataStore?.setRelation("photo", parentObjectId: reportedPost.objectId, childObjects: [photoID])
            
            self.getReportedPosts(with: currentUser, completion: { (result, error) in
                if error == nil {
                    debugPrint(response as Any)
                    completion(true, nil)
                    return
                } else {
                    debugPrint(error as Any)
                    completion(false, error)
                    return
                }
            })
        }, error: { (error) in
            debugPrint(error as Any)
            completion(false, error)
            return
        })
    }
    
    static func getReportedPosts(with user: BackendlessUser, completion: @escaping AppCompletion) {
        let objectId = user.objectId as String
        let whereClause = "reportedByUser.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let reported = response as? [ReportPost] else { return }
            self.reported = reported
            completion(reported, nil)
            return
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getReportedPosts(with user: BackendlessUser) {
        let objectId = user.objectId as String
        let whereClause = "reportedByUser.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let reported = response as? [ReportPost] else { return }
            self.reported = reported
            return
        }, error: { _ in
            return
        })
    }
    
}
