//
//  ReportUserController.swift
//  Bubo
//
//  Created by Nelson Bolivar on 1/10/18.
//  Copyright © 2018 Nelson Bolivar. All rights reserved.
//

import UIKit

struct ReportUserController {
    
    static var reported: [ReportUser] = []
    private static let dataStore = Backendless.sharedInstance().data.of(ReportUser.ofClass())
    
    static func reportUser(user: ReportUser, completion: @escaping AppCompletion) {
        guard let userID = user.userReported?.objectId else { return }
        user.userReported = nil
        
        dataStore?.save(user, response: { (response) in
            guard let userReported = response as? ReportUser else { completion(false, nil); return }
            let currentUser = User.current
            
            let _ = dataStore?.setRelation("reportedByUser", parentObjectId: userReported.objectId, childObjects: [currentUser.objectId])
            let _ = dataStore?.setRelation("userReported", parentObjectId: userReported.objectId, childObjects: [userID])
            
            self.getReportedUsers(with: currentUser, completion: { (result, error) in
                if error == nil {
                    debugPrint(response as Any)
                    completion(true, nil)
                    return
                } else {
                    debugPrint(error as Any)
                    completion(false, error)
                    return
                }
            })
        }, error: { (error) in
            debugPrint(error as Any)
            completion(false, error)
            return
        })
    }
    
    static func getReportedUsers(with user: BackendlessUser, completion: @escaping AppCompletion) {
        let objectId = user.objectId as String
        let whereClause = "reportedByUser.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let reported = response as? [ReportUser] else { return }
            self.reported = reported
            completion(reported, nil)
            return
        }, error: { error in
            completion(nil, error)
            return
        })
    }
    
    static func getReportedPosts(with user: BackendlessUser) {
        let objectId = user.objectId as String
        let whereClause = "reportedByUser.objectId='"+objectId+"'"
        guard let dataQuery = DataQueryBuilder() else { return }
        dataQuery.setWhereClause(whereClause)
        dataQuery.setSortBy(["created desc"])
        dataQuery.setPageSize(50)
        dataQuery.setRelationsDepth(2)
        
        dataStore?.find(dataQuery, response: { response in
            guard let reported = response as? [ReportUser] else { return }
            self.reported = reported
            return
        }, error: { _ in
            return
        })
    }
    
}
