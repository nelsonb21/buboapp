//
//  PlacemarkController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 5/2/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct PlacemarkController {
    
    static func getReverseGeocodeLocation(with location: CLLocation, completion: @escaping AppCompletion) {
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if (error != nil) {
                print(error!.localizedDescription)
                completion(nil, nil)
                return
            }
            
            if (placemarks?.count)! > 0 {
                guard let placemark = placemarks?.first else { return }
                completion(placemark, nil)
                return
            }
            
            completion(nil, nil)
            return
        }
    }
    
    static func getAddress(of placemark: CLPlacemark) -> String {
        var address = "Location name not available"
        guard let street = placemark.thoroughfare else { return address }
        address = "Close to " + street
        guard let name = placemark.name else { return address }
        address = address + ", " + name
        return address
    }
    
    static func getCity(of placemark: CLPlacemark) -> String {
        guard let city = placemark.locality else { return "" }
        return city
    }
    
    static func getCountry(of placemark: CLPlacemark) -> String {
        guard let country = placemark.country else { return "" }
        return country
    }

}
