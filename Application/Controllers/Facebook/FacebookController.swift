//
//  FacebookController.swift
//  Bubo
//
//  Created by Nelson Bolivar on 10/27/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct FacebookController {

    private static let loginManager = FBSDKLoginManager()
    
    static func logIn(with viewController: UIViewController, completion: @escaping AppCompletion) {
        loginManager.logIn(withReadPermissions: ["public_profile", "email"], from: viewController) { (loginResult, error) in
            guard error == nil else { return }
            guard let isCancelled = loginResult?.isCancelled, !isCancelled else { completion(nil, error as? Fault); return }
            
            self.getUserInfo(completion: { (_, _) in })
            
            self.loginIn(with: { (user, error) in
                completion(user, error)
                return
            })
        }
    }
    
    static func accessToken() -> FBSDKAccessToken? {
        guard FBSDKAccessToken.current() != nil else { return nil }
        return FBSDKAccessToken.current()
    }
    
    //MARK: - Private Methods
    
    private static func loginIn(with completion: @escaping AppCompletion) {
        guard let token = accessToken() else { completion(nil, nil); return }
        let userId: String = token.userID
        let tokenString: String = token.tokenString
        let expirationDate: Date = token.expirationDate
        let fieldsMapping = ["email": "email", "name": "name", "picture": "profileImageUrl"]
        
        UserController.logIn(with: userId, token: tokenString, expiration: expirationDate, fields: fieldsMapping) { (user, error) in
            completion(user, error)
            return
        }
    }
    
    private static func getUserInfo(completion: @escaping AppCompletion) {
        guard let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"], httpMethod: "GET") else { return }
        request.start(completionHandler: { (requestConnection, result, error) in
            guard error == nil else {
                print(error?.localizedDescription ?? "Error in Facebook getUserInfo")
                completion(nil, error as? Fault)
                return
            }
            
            guard let result = result as? [String: Any] else { return }
            var user = [String: Any]()
            
            guard let id = result["id"] as? String else { return }
            user["id"] = id
            
            guard let username = result["name"] as? String else { return }
            user["username"] = username
            
            guard let email = result["email"] as? String else { return }
            user["email"] = email
            
            guard let picture = result["picture"] as? [String: Any] else { return }
            guard let data = picture["data"] as? [String: Any] else { return }
            guard let image = data["url"] as? String else { return }
            user["image"] = image
            UserController.setupProfileImage(with: image)
            
            completion(user, nil)
        })
    }
    
}
