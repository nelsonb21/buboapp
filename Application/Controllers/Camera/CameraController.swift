//
//  CameraController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 8/27/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion
import Photos

protocol CameraControllerDelegate {
    func videoRecordFinished(with urlFile: URL)
    func updateVideoProgressView(with value: Float)
}

class CameraController: NSObject {
    
    static let shared = CameraController()
    
    var previewViewContainer: UIView?
    var videoLayer: AVCaptureVideoPreviewLayer?
    
    var session: AVCaptureSession?
    var device: AVCaptureDevice?
    var imageInput: AVCaptureDeviceInput?
    var imageOutput: AVCaptureStillImageOutput?
    var focusView: UIView?
    
    var videoInput: AVCaptureDeviceInput?
    var videoOutput: AVCaptureMovieFileOutput?

    var motionManager: CMMotionManager?
    var currentDeviceOrientation: UIDeviceOrientation?
    
    var previewViewFrame: CGRect?
    
    fileprivate var isRecording = false
    fileprivate var cameraSelected: AVCaptureDevice.Position = .back
    var shouldRotate = false
    var delegate: CameraControllerDelegate?
    
    //ProgressView
    var videoTimeCounter =  0.0
    var videoLimitTime = 15.0
    var timer = Timer()
    var timerIsOn = false

    func initialize(_ previewViewFrame: CGRect?) {
        self.previewViewFrame = previewViewFrame
        session = AVCaptureSession()
        
        for device in AVCaptureDevice.devices() {
            if device.position == .back {
                self.device = device
            }
        }
        
        for device in AVCaptureDevice.devices(for: AVMediaType.audio) {
            do {
                guard let audioInput = try? AVCaptureDeviceInput(device: device) else { return }
                session?.addInput(audioInput)
            }
        }
        
        setupPhotoCamera()
        
        guard let frame = previewViewFrame else { return }
        setupVideoLayer(frame)
        
        session?.startRunning()
        flashConfiguration()
        startCamera()
    }
    
    func setupVideoLayer(_ frame: CGRect) {
        videoLayer = nil
        guard let session = self.session else { return }
        videoLayer = AVCaptureVideoPreviewLayer(session: session)
        videoLayer?.frame = frame
        videoLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
    }
    
    public func startCamera() {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            session?.startRunning()
            motionManager = CMMotionManager()
            motionManager?.accelerometerUpdateInterval = 0.2
            motionManager?.startAccelerometerUpdates(to: OperationQueue()) { [unowned self] (data, _) in
                if let data = data {
                    if abs(data.acceleration.y) < abs(data.acceleration.x) {
                        self.currentDeviceOrientation = data.acceleration.x > 0 ? .landscapeRight : .landscapeLeft
                    } else {
                        self.currentDeviceOrientation = data.acceleration.y > 0 ? .portraitUpsideDown : .portrait
                    }
                }
            }
        case .denied, .restricted:
            stopCamera()
        default:
            break
        }
    }
    
    func stopCamera() {
        session?.stopRunning()
        motionManager?.stopAccelerometerUpdates()
        currentDeviceOrientation = nil
    }
    
    func shotButtonPressed(completion: @escaping AppCompletion) {
        session?.sessionPreset = AVCaptureSession.Preset.photo
        session?.commitConfiguration()
        guard let imageOutput = imageOutput else { return }
        
        DispatchQueue.global(qos: .default).async(execute: { () -> Void in
            let videoConnection = imageOutput.connection(with: AVMediaType.video)
            let orientation = self.currentDeviceOrientation ?? UIDevice.current.orientation
            
            switch (orientation) {
            case .portrait:
                videoConnection?.videoOrientation = .portrait
            case .portraitUpsideDown:
                videoConnection?.videoOrientation = .portraitUpsideDown
            case .landscapeRight:
                videoConnection?.videoOrientation = .landscapeLeft
            case .landscapeLeft:
                videoConnection?.videoOrientation = .landscapeRight
            default:
                videoConnection?.videoOrientation = .portrait
            }
            
            guard let connection = videoConnection else { return }
            
            imageOutput.captureStillImageAsynchronously(from: connection) { (buffer, error) -> Void in
                self.stopCamera()
                guard let buffer = buffer, let data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer), let image = UIImage(data: data) else { return }
                DispatchQueue.main.async(execute: { () -> Void in
                    self.saveImageToCameraRoll(image: image)
                    completion(image, nil)
                    return
                })
            }
            completion(nil, nil)
            return
        })
    }
    
    func startRecording() {
        setupVideoRecord()
        guard let videoOutput = videoOutput else { return }
        isRecording = !isRecording
        
        if isRecording {
            updateProgressView()
            shouldRotate = true
            
            let outputPath = "\(NSTemporaryDirectory())output.mov"
            let outputURL = URL(fileURLWithPath: outputPath)
            
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: outputPath) {
                do {
                    try fileManager.removeItem(atPath: outputPath)
                } catch {
                    print("error removing item at path: \(outputPath)")
                    self.isRecording = false
                    return
                }
            }
            videoOutput.startRecording(to: outputURL, recordingDelegate: self)
        }
    }
    
    func stopRecording() {
        stopTimer()
        guard let videoOutput = videoOutput else { return }
        isRecording = !isRecording
        shouldRotate = false
        videoOutput.stopRecording()
        setupPhotoCamera()
    }
    
    func flipButtonPressed() {
        if !cameraIsAvailable { return }
        session?.stopRunning()
        
        do {
            session?.beginConfiguration()
            if let session = session {
                for input in session.inputs {
                    session.removeInput(input )
                }
            }
            
            let position: AVCaptureDevice.Position = (imageInput?.device.position == AVCaptureDevice.Position.front) ? .back : .front
            cameraSelected = position
            
            for device in AVCaptureDevice.devices(for: AVMediaType.video) {
                if device.position == position {
                    self.device = device
                    imageInput = try AVCaptureDeviceInput(device: device)
                    guard let imageInput = imageInput else { return }
                    session?.addInput(imageInput)
                }
            }
            
            for device in AVCaptureDevice.devices(for: AVMediaType.audio) {
                do {
                    let audioInput = try AVCaptureDeviceInput(device: device)
                    session?.addInput(audioInput)
                }
            }
            
            session?.commitConfiguration()
        } catch {
            print("Error FlipButtonPressed: \(error.localizedDescription)")
        }
        session?.startRunning()
    }
    
    func flashButtonPressed() {
        if !cameraIsAvailable { return }
        do {
            guard let device = device, device.hasFlash else { return }
            try device.lockForConfiguration()
            
            switch device.flashMode {
            case .off:
                device.flashMode = AVCaptureDevice.FlashMode.on
            case .on:
                device.flashMode = AVCaptureDevice.FlashMode.off
            default:
                break
            }
            device.unlockForConfiguration()
        } catch {
            print("Error FlashButtonPressed: \(error.localizedDescription)")
            return
        }
    }
    
    private func setupVideoRecord() {
        do {
            guard let device = device, let imageInput = imageInput, let imageOutput = imageOutput else { return }
            videoInput = try AVCaptureDeviceInput(device: device)
            session?.beginConfiguration()
            session?.removeInput(imageInput)
            session?.removeOutput(imageOutput)
            
            for device in AVCaptureDevice.devices(for: AVMediaType.video) {
                if device.position == cameraSelected {
                    videoInput = try AVCaptureDeviceInput(device: device)
                    if let videoInput = videoInput {
                        session?.addInput(videoInput)
                    }
                }
            }
            
            videoOutput = AVCaptureMovieFileOutput()
            let totalSeconds = 16.0 //Total Seconds of capture time
            let timeScale: Int32 = 30 //FPS
            let maxDuration = CMTimeMakeWithSeconds(totalSeconds, timeScale)
            
            videoOutput?.maxRecordedDuration = maxDuration
            videoOutput?.minFreeDiskSpaceLimit = 1024 * 1024 //SET MIN FREE SPACE IN BYTES FOR RECORDING TO CONTINUE ON A VOLUME
            
            guard let videoOutput = videoOutput else { return }
            if (session?.canAddOutput(videoOutput)) != nil {
                session?.addOutput(videoOutput)
            }
            session?.sessionPreset = AVCaptureSession.Preset.hd1280x720
            session?.commitConfiguration()
        } catch{
            print("Error setupVideoRecord: \(error.localizedDescription)")
        }
    }
    
    private func setupPhotoCamera() {
        do {
            session?.beginConfiguration()
            if let videoInput = videoInput {
                session?.removeInput(videoInput)
            }
            if let videoOutput = videoOutput {
                session?.removeOutput(videoOutput)
            }
            imageInput = nil
            guard let device = device else { return }
            imageInput = try AVCaptureDeviceInput(device: device)
            guard let imageInput = imageInput else { return }
            session?.addInput(imageInput)
            imageOutput = nil
            imageOutput = AVCaptureStillImageOutput()
            guard let imageOutput = imageOutput else { return }
            session?.addOutput(imageOutput)
            session?.sessionPreset = AVCaptureSession.Preset.photo
            session?.commitConfiguration()
        } catch {
            print("Error setupPhotoCamera: \(error.localizedDescription)")
        }
    }
    
    func resetImageOutput() {
        session?.beginConfiguration()
        guard let imageOutput = self.imageOutput else { return }
        session?.removeOutput(imageOutput)
        self.imageOutput = AVCaptureStillImageOutput()
        guard let imageOutput2 = self.imageOutput else { return }
        session?.addOutput(imageOutput2)
        session?.commitConfiguration()
    }

}

extension CameraController: AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        delegate?.videoRecordFinished(with: outputFileURL)
    }
}

extension CameraController {
    
    func saveImageToCameraRoll(image: UIImage) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: image)
        }, completionHandler: nil)
    }
    
    func flashConfiguration() {
        do {
            if let device = device {
                guard device.hasFlash else { return }
                try device.lockForConfiguration()
                device.flashMode = AVCaptureDevice.FlashMode.off
                device.unlockForConfiguration()
            }
        } catch {
            print("Error FlashConfiguration: \(error.localizedDescription)")
            return
        }
    }
    
    var cameraIsAvailable: Bool {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if status == .authorized { return true }
        return false
    }
    
}

fileprivate extension CameraController {
    
    func updateProgressView() {
        if !timerIsOn {
            timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
            timerIsOn = true
        }
    }
    
    @objc func timerRunning() {
        videoTimeCounter += 0.0001
        let completionPercentage = (videoTimeCounter * 100) / videoLimitTime
        delegate?.updateVideoProgressView(with: Float(completionPercentage / 100))
        if videoTimeCounter >= videoLimitTime {
            stopTimer()
            stopRecording()
        }
    }
    
    func stopTimer() {
        timer.invalidate()
        timerIsOn = false
        resetVideoView()
    }
    
    func resetVideoView() {
        videoTimeCounter = 0.0
    }
    
}
