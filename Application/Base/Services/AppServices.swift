//
//  AppServices.swift
//  Bubo
//
//  Created by Nelson Bolivar on 9/21/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import Fabric
import Crashlytics

extension AppDelegate {
    
    func setupServices() {
        Fabric.with([Crashlytics.self])
        Backendless.sharedInstance().initApp(appID, apiKey: apiKey)
        IQKeyboardManager.sharedManager().enable = true
        GMSServices.provideAPIKey(googleMaps)
        setupAnalytics()
    }
    
    private func setupAnalytics() {
        let builder = FlurrySessionBuilder.init()
            .withAppVersion("1.0")
            .withLogLevel(FlurryLogLevelAll)
            .withCrashReporting(true)
            .withSessionContinueSeconds(10)
        
        Flurry.startSession("NY48DRN4D7GMB5YPVQKV", with: builder)
        Analytics.logEvent(with: .appStarted)
    }
    
}
