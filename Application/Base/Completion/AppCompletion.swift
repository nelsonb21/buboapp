//
//  AppCompletion.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/13/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import Foundation

typealias AppCompletion = (_ object: Any?, _ error: Fault?) -> Void
