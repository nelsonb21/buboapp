//
//  AppViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/5/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

@objc protocol AppViewControllerSetup: NSObjectProtocol {
    @objc optional func setupView()
}

class AppViewController: UIViewController, AppViewControllerSetup, UINavigationControllerDelegate {
    
    var loadingImage: UIImageView?
    var refreshControl: UIRefreshControl?
    var navigationBarBackgroundImageView: UIImageView?
    var navigationBarHeightConstraint: NSLayoutConstraint?
    var emptyStateImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarStyles()
        self.view.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.9725490196, blue: 1, alpha: 1)
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        let selector = #selector(AppViewControllerSetup.setupView)
        if responds(to: selector) {
            perform(selector)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = !(self == navigationController?.viewControllers.first)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadingImage?.removeFromSuperview()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { _ in
            let selector = #selector(AppViewControllerSetup.setupView)
            if self.responds(to: selector) {
                self.perform(selector)
            }
        }
        
    }
    
    //MARK: - Actions
    
    func close() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func back() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func backToRoot() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
}
