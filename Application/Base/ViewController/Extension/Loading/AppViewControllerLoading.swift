//
//  AppViewControllerLoading.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 8/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension AppViewController {
    
    func addLoadingAnimation(toView view: UIView?) {
        guard let view = view else { return }
        loadingImage = UIImageView(frame: CGRect(x: view.bounds.width/2 - 45, y: view.bounds.height/2 - 45, width: 90, height: 90))
        loadingImage?.backgroundColor = .clear
        loadingImage?.contentMode = .scaleToFill
        loadingImage?.isHidden = true
        loadingImage?.setupGif()
        guard let loadingImage = loadingImage else { return }
        view.addSubview(loadingImage)
    }
    
}
