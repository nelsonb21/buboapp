//
//  AppViewControllerNavigationBarSyle.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 5/14/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension AppViewController {
    
    func setupNavigationBarStyles() {
        guard let navigationController = navigationController, !navigationController.isNavigationBarHidden else { return }
        navigationController.navigationBar.isTranslucent = true
        setupBarButtonStyles()
        setupNavigationBackgroundView()
    }
    
    private func setupBarButtonStyles() {
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        
        if let count = navigationController?.viewControllers.count, count > 1 {
            navigationItem.leftBarButtonItems?.removeAll()
            let backButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "btn_back"), target: self, action: #selector(back))
            navigationItem.leftBarButtonItem = backButton
        }
    }
    
    private func setupNavigationBackgroundView() {
        navigationBarBackgroundImageView = UIImageView()
        navigationBarBackgroundImageView?.backgroundColor = #colorLiteral(red: 0.6196078431, green: 0.5294117647, blue: 0.7568627451, alpha: 1)
        navigationBarBackgroundImageView?.translatesAutoresizingMaskIntoConstraints = false
        navigationBarBackgroundImageView?.layer.borderColor = UIColor.clear.cgColor
        navigationBarBackgroundImageView?.layer.borderWidth = 0
        
        view.addSubview(navigationBarBackgroundImageView!)
        
        let views = ["backgroundView": navigationBarBackgroundImageView!]
        let height = (navigationController?.navigationBar.frame.height ?? 0) + (UIDevice.current.safeAreaHasTopInsetValue ? 44 : 20)
        let metrics = ["height": height]
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
        navigationBarHeightConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|[backgroundView(height)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views).last
        NSLayoutConstraint.activate([navigationBarHeightConstraint!])
    }
    
}
