//
//  AppViewControllerAlert.swift
//  TapApp
//
//  Created by Nelson Bolívar on 8/13/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension AppViewController {

    func showAlertWithTitle(title: String?, message: String?) {
        showAlertWithTitle(title: title, message: message, handler: nil)
    }
    
    func showAlertWithTitle(title: String?, message: String?, handler: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithTitle(title: String?, message: String?, actionTitle: String?, handler: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: actionTitle, style: .default, handler: handler))
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithMessage(message: String?) {
        showAlertWithTitle(title: "", message: message)
    }
    
    func showErrorAlertWithMessage(message: String?) {
        showAlertWithTitle(title: "Oops!", message: message)
    }
    
}
