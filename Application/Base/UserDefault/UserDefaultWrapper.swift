//
//  UserDefaultWrapper.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct UserDefaultWrapper {
    
    @nonobjc static let user = UserDefaults()
    
    static func setData(value: Any, key: UserDefaultKey) {
        UserDefaults().set(value, forKey: key.rawValue)
    }
    
    static func removeData(key: UserDefaultKey) {
        UserDefaults().removeObject(forKey: key.rawValue)
    }
    
}
