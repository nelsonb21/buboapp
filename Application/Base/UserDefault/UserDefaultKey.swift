//
//  UserDefaultKey.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

enum UserDefaultKey: String {
    case userDataArray
    case isFirstTime
}
