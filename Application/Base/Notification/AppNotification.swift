//
//  AppNotification.swift
//  Bubo
//
//  Created by Nelson Bolivar on 9/8/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension AppDelegate: UNUserNotificationCenterDelegate {

    func setupNotification()  {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                DispatchQueue.main.sync(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        } else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
}
