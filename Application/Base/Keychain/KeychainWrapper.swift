//
//  KeychainWrapper.swift
//  Shopify
//
//  Created by Nelson Bolívar on 10/28/16.
//  Copyright © 2016 Koombea. All rights reserved.
//

import UIKit
import KeychainAccess

struct KeychainWrapper {
    
    private static let keychain = Keychain()
    
    static func save(data: Data, key: KeychainKey) {
        keychain[data: key.rawValue] = data
    }
    
    static func save(string: String, key: KeychainKey) {
        keychain[string: key.rawValue] = string
    }
    
    static func getData(key: KeychainKey) -> Data? {
        return keychain[data: key.rawValue]
    }
    
    static func getString(key: KeychainKey) -> String? {
        return keychain[string: key.rawValue]
    }
    
    @discardableResult
    static func remove(key: KeychainKey) -> Bool {
        do {
            try keychain.remove(key.rawValue)
            return true
        } catch {
            print("\(error.localizedDescription)")
            return false
        }
    }
    
    @discardableResult
    static func removeAll() -> Bool {
        do {
            try keychain.removeAll()
            return true
        } catch {
            print("\(error.localizedDescription)")
            return false
        }
    }
    
    @discardableResult
    static func contains(key: KeychainKey) -> Bool {
        do {
            return try keychain.contains(key.rawValue)
        } catch {
            print("\(error.localizedDescription)")
            return false
        }
    }
    
}
