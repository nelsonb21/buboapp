//
//  KeychainKey.swift
//  Shopify
//
//  Created by Nelson Bolívar on 10/28/16.
//  Copyright © 2016 Koombea. All rights reserved.
//

import UIKit

enum KeychainKey: String {
    case CustomerTokenData
}
