//
//  UserTextField.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

@IBDesignable
class UserTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 0.5
        layer.cornerRadius = 4
        textAlignment = .center
        textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        attributedPlaceholder = NSAttributedString(string: placeholder!, attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)])
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

}
