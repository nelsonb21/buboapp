//
//  UserTextView.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class UserTextView: UITextView {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 0.5
        layer.borderColor = #colorLiteral(red: 0.5019607843, green: 0.3960784314, blue: 1, alpha: 1).cgColor
        layer.cornerRadius = 4
        
        textContainerInset = UIEdgeInsetsMake(6, 6, 6, 6)
    }

}
