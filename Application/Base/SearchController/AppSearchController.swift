//
//  AppSearchController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 4/29/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class AppSearchController: UISearchController {

    let appSearchBar = AppSearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var searchBar: UISearchBar { return appSearchBar }

}
