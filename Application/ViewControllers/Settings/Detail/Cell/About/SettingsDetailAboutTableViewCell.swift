//
//  SettingsDetailAboutTableViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 6/10/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class SettingsDetailAboutTableViewCell: UITableViewCell {

    @IBOutlet weak var versionLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        versionLabel?.text = "Bubo Version " + Bundle.main.appVersion
    }

    
}
