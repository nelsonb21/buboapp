//
//  SettingsDetailTableViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class SettingsDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var settingsDetailOptionLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(option: SettingsOptions) {
        settingsDetailOptionLabel?.text = option.rawValue
        defaultStyle()
        setupCell(with: option)
    }
    
    private func setupCell(with option: SettingsOptions) {
        switch option {
        case .about:
            setupAboutStyle()
            break
        case .shareThisApp:
            break
        case .contactUs:
            break
        case .rateUs:
            break
        case .legal:
            setupLegaStyle()
            break
        case .logout:
            setupLogoutStyle()
            break
        }
    }
    
    private func defaultStyle() {
        accessoryType = .none
        settingsDetailOptionLabel?.textAlignment = .left
        settingsDetailOptionLabel?.textColor = .black
    }
    
    private func setupAboutStyle() {
        accessoryType = .disclosureIndicator
    }
    
    private func setupLegaStyle() {
        accessoryType = .disclosureIndicator
    }
    
    private func setupLogoutStyle() {
        settingsDetailOptionLabel?.textAlignment = .center
        settingsDetailOptionLabel?.textColor = #colorLiteral(red: 0.1411764706, green: 0.4941176471, blue: 0.9137254902, alpha: 1)
    }
    
}
