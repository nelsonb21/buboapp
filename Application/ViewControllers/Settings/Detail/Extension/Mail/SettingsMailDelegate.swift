//
//  SettingsMailDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import MessageUI

typealias SettingsMailDelegate = SettingsDetailViewController

enum ContactEmail: String {
    case contact = "contact@appbubo.com"
}

extension SettingsMailDelegate: MFMailComposeViewControllerDelegate {
    
    func sendEmail(email: ContactEmail) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email.rawValue])
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

}
