//
//  SettingsDetailTableViewDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias SettingsDetailTableViewDelegate = SettingsDetailViewController

extension SettingsDetailTableViewDelegate: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = SettingsSections.sections[indexPath.section]
        switch section {
        case .app:
            return 120
        default:
            return 64
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = SettingsSections.sections[indexPath.section]
        var option: SettingsOptions?
        switch section {
        case .app:
            break
        case .social:
            option = SettingsSections.socialOptions[indexPath.row]
        case .support:
            option = SettingsSections.supportOptions[indexPath.row]
        case .legal:
            option = SettingsSections.legalOptions[indexPath.row]
        case .session:
            option = SettingsSections.sessionOptions[indexPath.row]
        }
        guard let opt = option else { return }
        optionTapped(option: opt)
    }
    
    //MARK: - Private Methods
    
    private func optionTapped(option: SettingsOptions) {
        switch option {
        case .about:
            break
        case .shareThisApp:
            Analytics.logEvent(with: .settingsShare)
            openShareOptions()
            break
        case .contactUs:
            Analytics.logEvent(with: .settingsContactUs)
            sendEmail(email: .contact)
            break
        case .rateUs:
            Analytics.logEvent(with: .settingsRateUs)
            reviewApp()
            break
        case .legal:
            Analytics.logEvent(with: .settingsLegal)
            openLegal()
        case .logout:
            Analytics.logEvent(with: .settingsLogout)
            logoutAction()
            break
        }
    }
    
}
