//
//  SettingsDetailActions.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias SettingsDetailActions = SettingsDetailViewController

extension SettingsDetailActions {
    
    func logoutAction() {
        UserController.logUserOut { (result, error) in
            if error == nil {
                self.presentOnboarding()
            }
        }
    }
    
    func openAboutView() {
        guard let settingsAboutViewController = UIStoryboard.viewController(withIdentifier: String(describing: SettingsAboutViewController.self)) as? SettingsAboutViewController else { return }
        navigationController?.pushViewController(settingsAboutViewController, animated: true)
    }
    
    func openShareOptions() {
        let firstActivityItem = "Discover what's around you... Check out Bubo!"
        let secondActivityItem: URL = URL(string: "https://www.appbubo.com")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            .postToWeibo,
            .print,
            .assignToContact,
            .saveToCameraRoll,
            .addToReadingList,
            .postToFlickr,
            .postToVimeo,
            .postToTencentWeibo
        ]
        
        present(activityViewController, animated: true, completion: nil)
    }

    //MARK: - Private Methods
    
    private func presentOnboarding() {
        guard let onBoardingViewController = UIStoryboard.viewController(withIdentifier: String(describing: OnBoardingViewController.self)) as? OnBoardingViewController else { return }
        present(onBoardingViewController, animated: true, completion: nil)
    }
    
}
