//
//  SettingsDetailViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class SettingsDetailViewController: AppViewController {
    
    @IBOutlet weak var tableView: UITableView?
    
    func setupView() {
        title = "Settings"
        tableView?.register(withIdentifier: "SettingsDetailTableViewCell")
        tableView?.register(withIdentifier: "SettingsDetailAboutTableViewCell")
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.tableFooterView = UIView()
    }

    //MARK: - Methods

    func reviewApp() {
        ReviewController.shared.reviewApp()
    }
    
    func openLegal() {
        guard let settingsWebViewController = UIStoryboard.viewController(withIdentifier: String(describing: SettingsWebViewController.self)) as? SettingsWebViewController else { return }
        settingsWebViewController.link = .legal
        navigationController?.pushViewController(settingsWebViewController, animated: true)
    }
    
}
