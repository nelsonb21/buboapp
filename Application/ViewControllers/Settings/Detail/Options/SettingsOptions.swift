//
//  SettingsOptions.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

enum SettingsOptions: String {

    case about = "About"
    case shareThisApp = "Share This App"
    case contactUs = "Contact Us"
    case rateUs = "Rate Us"
    case legal = "Legal"
    case logout = "Logout"
    
    static let options = [shareThisApp, contactUs, rateUs, legal, about, logout]
    
}
