//
//  SettingsSections.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

enum SettingsSections: String {
    
    case app = "The App"
    case social = "Social"
    case support = "Support"
    case legal = "Legal"
    case session = ""
    
    static let appOptions = [SettingsOptions.about]
    static let socialOptions = [SettingsOptions.shareThisApp, SettingsOptions.rateUs]
    static let supportOptions = [SettingsOptions.contactUs]
    static let legalOptions = [SettingsOptions.legal]
    static let sessionOptions = [SettingsOptions.logout]
    
    static let sections = [social, support, legal, app, session]

}
