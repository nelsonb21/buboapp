//
//  SettingsDetailTableViewDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias SettingsDetailDataSource = SettingsDetailViewController

extension SettingsDetailDataSource: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return SettingsSections.sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = SettingsSections.sections[section]
        return title == .app ? "" : SettingsSections.sections[section].rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = SettingsSections.sections[section]
        switch section {
        case .app:
            return SettingsSections.appOptions.count
        case .social:
            return SettingsSections.socialOptions.count
        case .support:
            return SettingsSections.supportOptions.count
        case .legal:
            return SettingsSections.legalOptions.count
        case .session:
            return SettingsSections.sessionOptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = SettingsSections.sections[indexPath.section]
        var option: SettingsOptions?
        switch section {
        case .app:
            option = SettingsSections.appOptions[indexPath.row]
            return setupAboutCell(with: option!, tableView: tableView, indexPath: indexPath)
        case .social:
            option = SettingsSections.socialOptions[indexPath.row]
        case .support:
            option = SettingsSections.supportOptions[indexPath.row]
        case .legal:
            option = SettingsSections.legalOptions[indexPath.row]
        case .session:
           option = SettingsSections.sessionOptions[indexPath.row]
        }
        return setupCell(with: option!, tableView: tableView, indexPath: indexPath)
    }
    
    private func setupCell(with option: SettingsOptions, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsDetailTableViewCell", for: indexPath) as! SettingsDetailTableViewCell
        cell.setupCell(option: option)
        return cell
    }
    
    private func setupAboutCell(with option: SettingsOptions, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsDetailAboutTableViewCell", for: indexPath) as! SettingsDetailAboutTableViewCell
        return cell
    }
    
}
