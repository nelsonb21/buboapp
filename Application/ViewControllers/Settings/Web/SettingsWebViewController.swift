//
//  SettingsWebViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 8/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

enum appLinks: String {
    case legal = "https://www.appbubo.com/terms-use/"
}

class SettingsWebViewController: AppViewController {

    @IBOutlet weak var webView: UIWebView?
    var link: appLinks?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - Setup
    func setupView() {
        guard let link = link else { return }
        guard let url = URL(string: link.rawValue) else { return }
        webView?.loadRequest(URLRequest(url: url))
    }
    
    //MARK: - IBActions
    
    @IBAction func close(_ sender: UIButton) {
        close()
    }
}
