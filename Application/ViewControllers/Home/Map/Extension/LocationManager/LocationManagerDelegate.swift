//
//  LocationManagerDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import GoogleMaps

typealias LocationManagerDelegate = HomeMapViewController

extension LocationManagerDelegate: CLLocationManagerDelegate {
    
    func loadLocationManager() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.distanceFilter = kCLDistanceFilterNone
        locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorized, .authorizedWhenInUse:
            manager.startUpdatingLocation()
        default:
            manager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        setupMapView(manager.location?.coordinate)
        stablishCurrentLocation(manager.location?.coordinate, getPhotos: true)
        manager.stopUpdatingLocation()
    }
    
}
