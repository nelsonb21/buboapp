//
//  MapViewDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import GoogleMaps

typealias MapViewDelegate = HomeMapViewController

extension MapViewDelegate: GMSMapViewDelegate {
    
    //MARK: - Setup
    
    func setupMapView(_ coordinate: CLLocationCoordinate2D?) {
        guard let latitude = coordinate?.latitude else { return }
        guard let longitude = coordinate?.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 10.0)
        mapView?.camera = camera
        mapView?.delegate = self
        mapView?.settings.myLocationButton = false
        mapView?.isMyLocationEnabled = true
        mapView?.setMinZoom(3.0, maxZoom: 15.0)
    }
    
    func stablishCurrentLocation(_ coordinate: CLLocationCoordinate2D?, getPhotos: Bool = false) {
        guard let latitude = coordinate?.latitude else { return }
        guard let longitude = coordinate?.longitude else { return }
        
        currentLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        guard let currentLocation = currentLocation else { return }
        selectedLocation = nil
        getPhotos ? loadPinSelectedPhotos(currentLocation) : ()
        keepCurrentLocationMarker()
    }
    
    func stablishSelectedLocation(_ coordinate: CLLocationCoordinate2D, getPhotos: Bool = false, location: CLLocation?) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        marker.map = mapView
        marker.icon = GMSMarker.markerImage(with: UIColor.red)
        
        getPhotos ? loadPinSelectedPhotos(coordinate) : ()
        
        if selectedLocation?.latitude != coordinate.latitude && selectedLocation?.longitude != coordinate.longitude {
            selectedLocation = coordinate
            
            if location != nil {
                PlacemarkController.getReverseGeocodeLocation(with: location!) { (placemark, _) in
                    self.selectedPlacemark = placemark as! CLPlacemark?
                    self.setupPlaceInformation(placemark: placemark as! CLPlacemark?, marker: marker)
                }
            }
            getPlace(with: selectedLocation!)
        }
    }
    
    func keepCurrentLocationMarker() {
        let marker = GMSMarker()
        guard let currentLocation = currentLocation else { return }
        marker.position = currentLocation
        marker.icon = GMSMarker.markerImage(with: UIColor.red)
        marker.map = mapView
        
        if currentPlacemark == nil {
            let location = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
            PlacemarkController.getReverseGeocodeLocation(with: location) { (placemark, _) in
                guard let placemark = placemark as? CLPlacemark else { return }
                self.currentPlacemark = placemark
                self.setupPlaceInformation(placemark: self.currentPlacemark, marker: marker)
            }
            getPlace(with: currentLocation)
        }
    }
    
    func setupPlaceInformation(placemark: CLPlacemark? , marker: GMSMarker) {
        guard let placemark = placemark else { return }
        let city = placemark.locality
        let country = placemark.country

        placemarkLabel?.text = PlacemarkController.getAddress(of: placemark)
        
        if city != nil {
            marker.title = city!
            if country != nil {
                marker.snippet = country!
            }
            mapView?.selectedMarker = marker
        }
    }
    
    private func getPlace(with coordinates: CLLocationCoordinate2D) {
        guard let currentLocation = currentLocation else { return }
        PlaceController.getPlace(with: User.current, coordinates: currentLocation) { (response, error) in
            if error == nil {
                guard let places = response as? [Place] else { return }
                if places.count > 0 {
                    self.place = places.first
                    self.setupBookmarkButton(isBookmarked: true)
                } else {
                    self.place = nil
                    self.setupBookmarkButton(isBookmarked: false)
                }
            }
        }
    }
    
    //MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        clearMap()
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        stablishSelectedLocation(coordinate, getPhotos: true, location: location)
        photosViewBottomConstraint?.constant = 0
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        clearMap()
        guard let currentLocation = currentLocation else { return true }
        stablishCurrentLocation(currentLocation, getPhotos: true)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        clearMap()
        let location = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
        stablishSelectedLocation(marker.position, getPhotos: true, location: location)
        photosViewBottomConstraint?.constant = 0
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        searchController?.searchBar.resignFirstResponder()
    }
    
    //MARK: - Actions
    
    func clearMap() {
        mapView?.clear()
    }
    
    func loadPinSelectedPhotos(_ coordinates: CLLocationCoordinate2D) {        
        photosViewReference?.showActivityIndicator()
        PhotoController.getPhotosByCoordinates(coordinates, distance: defaultDistance, completion: { response, error in
            self.photosViewReference?.stopActivityIndicator()
            guard let photos = response as? [Photo] else { return }
            self.photosArray.removeAll()
            self.photosArray = photos
            
            self.photosAnnotation.removeAll()
            photos.forEach({ (photo) in
                guard let photoAnnotation = PhotoAnnotation(photo: photo) else { return }
                self.photosAnnotation.append(photoAnnotation)
            })
            
            self.photosViewReference?.photosArray = self.photosArray
            self.photosViewReference?.reloadCollectionView()
        })
    }
    
}
