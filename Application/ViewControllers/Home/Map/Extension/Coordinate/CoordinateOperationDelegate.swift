//
//  CoordinateOperationDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import GoogleMaps

typealias CoordinateOperationDelegate = HomeMapViewController

extension CoordinateOperationDelegate {
    
    func coordinateFromCoord(_ fromCoord: CLLocationCoordinate2D, distanceKm: Double, bearingDegrees: Double) -> CLLocationCoordinate2D {
        
        let distanceRadians = distanceKm / 6371.0
        
        let bearingRadians = radiansFromDegrees(bearingDegrees)
        let fromLatRadians = radiansFromDegrees(fromCoord.latitude)
        let fromLonRadians = radiansFromDegrees(fromCoord.longitude)
        
        let toLatRadians = asin( sin(fromLatRadians) * cos(distanceRadians) + cos(fromLatRadians) * sin(distanceRadians) * cos(bearingRadians) )
        
        var toLonRadians = fromLonRadians + atan2(sin(bearingRadians) * sin(distanceRadians) * cos(fromLatRadians), cos(distanceRadians) - sin(fromLatRadians) * sin(toLatRadians));
        
        toLonRadians = fmod((toLonRadians + 3*Double.pi), (2*Double.pi)) - Double.pi;
        
        var result = CLLocationCoordinate2D()
        result.latitude = degreesFromRadians(toLatRadians)
        result.longitude = degreesFromRadians(toLonRadians)
        return result;
    }
    
    func radiansFromDegrees(_ degrees: Double) -> Double {
        return degrees * (Double.pi/180.0)
    }
    
    func degreesFromRadians(_ radians: Double) -> Double {
        return radians * (180.0/Double.pi);
    }
    
}
