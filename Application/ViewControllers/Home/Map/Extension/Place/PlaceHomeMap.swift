//
//  PlaceHomeMap.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias PlaceHomeMap = HomeMapViewController

extension PlaceHomeMap {

    //MARK: - Place Methods
    
    func bookmarkPlace() {
        bookmarkButton?.setImage(#imageLiteral(resourceName: "btn_bookmark_place_fill"), for: .normal)
        let place = Place()
        if selectedLocation == nil {
            if let city = currentPlacemark?.locality { place.city = city }
            if let country = currentPlacemark?.country { place.country = country }
            if let street = currentPlacemark?.thoroughfare { place.street = street }
            if let name = currentPlacemark?.name { place.name = name }
            place.coordinates = GeoPoint().getGeopoint(with: currentLocation!)
        } else {
            if let city = selectedPlacemark?.locality { place.city = city }
            if let country = selectedPlacemark?.country { place.country = country }
            if let street = selectedPlacemark?.thoroughfare { place.street = street }
            if let name = selectedPlacemark?.name { place.name = name }
            place.coordinates = GeoPoint().getGeopoint(with: selectedLocation!)
        }
        place.imageUrl = photosArray.first?.imageUrl
        
        place.user = nil//User.current
        
        PlaceController.bookmarkPlace(place: place) { (result, error) in
            guard let result = result as? Bool else { return }
            if !result {
                print("Place couldn't be save")
            }
            Analytics.logEvent(with: .mapModeGalleryAddFavoritePlace)
            self.setupBookmarkButton(isBookmarked: result)
        }
    }
    
    func setupBookmarkButton(isBookmarked: Bool) {
        let image = isBookmarked ? #imageLiteral(resourceName: "btn_bookmark_place_fill") : #imageLiteral(resourceName: "btn_bookmark_place_empty")
        bookmarkButton?.setImage(image, for: .normal)
    }
    
    func deleteBookmarkedPlace() {
        guard let place = place else { return }
        PlaceController.deletePlace(with: place) { (result, error) in
            guard error == nil else { return }
            Analytics.logEvent(with: .mapModeGalleryRemoveFavoritePlace)
            self.setupBookmarkButton(isBookmarked: false)
        }
    }
    
}
