//
//  HomeMapViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

class HomeMapViewController: UIViewController, PhotosViewDelegate, SearchDelegate {

    @IBOutlet weak var photosView: UIView?
    @IBOutlet weak var mapView: GMSMapView?
    @IBOutlet weak var blurPhotosHeaderView: UIView?
    @IBOutlet weak var blurPhotosView: UIView?
    @IBOutlet weak var photosViewBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var placemarkLabel: UILabel?
    @IBOutlet weak var bookmarkButton: UIButton?
    @IBOutlet weak var photosViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var photosViewFullHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var navigationBarBlurHeightConstraint: NSLayoutConstraint?
    
    var defaultDistance = 2.0
    var locationManager: CLLocationManager?
    var currentLocation: CLLocationCoordinate2D?
    var selectedLocation: CLLocationCoordinate2D?
    var currentPlacemark: CLPlacemark?
    var selectedPlacemark: CLPlacemark?
    
    var photosArray: [Photo] = []
    var photosAnnotation: [PhotoAnnotation] = []
    var photosViewReference: PhotosView?

    var place: Place?
    var searchController: AppSearchController?
    var localSearchRequest: MKLocalSearchRequest?
    var localSearch: MKLocalSearch?
    var dragGesture: UIPanGestureRecognizer?
    var photosViewIsHidden = false
    var isOpenFromHome = true
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent(with: .mapModeOpened)
        setupNavBar()
        loadLocationManager()
        setupPhotosView()
        setupDragGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        definesPresentationContext = true
        setupStatusBar(with: .clear)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isOpenFromHome {
            searchController?.isActive = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)  , execute: {
                self.activeSearchController()
            })
            isOpenFromHome = false
        }
    }
    
    private func setupPhotosView() {
        view.layoutIfNeeded()
        guard let photos = Bundle.main.loadNibNamed("PhotosView", owner: self, options: nil)?[0] as? PhotosView else { return }
        guard let bounds = blurPhotosView?.bounds else { return }
        photos.frame = bounds
        photos.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        photos.delegate = self
        photos.photosArray = photosArray
        photos.setupCollectionView()
        photosViewReference = photos
        blurPhotosView?.addSubview(photos)
        photosViewBottomConstraint?.constant = -(photosViewHeightConstraint?.constant)!
    }
    
    private func setupNavBar() {
        guard let searchNavController = UIStoryboard.navigationController(withIdentifier: "SearchNavController", storyBoardIdentifier: "SearchViewController") else { return }
        guard let searchViewController = searchNavController.viewControllers.first as? SearchViewController else { return }
        
        searchViewController.searchDelegate = self
        searchController = AppSearchController(searchResultsController: searchViewController)
        searchController?.searchResultsUpdater = searchViewController
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
 
        searchController?.appSearchBar.placeholder = "Search for places"
        searchController?.appSearchBar.tintColor = .black
        searchController?.appSearchBar.barStyle = .default
        searchController?.appSearchBar.backgroundColor = .clear
        searchController?.appSearchBar.returnKeyType = .done
        
        navigationItem.titleView = searchController?.appSearchBar
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        navigationBarBlurHeightConstraint?.constant = UIDevice.current.safeAreaHasTopInsetValue ? 108 : 88
    }
    
    private func activeSearchController() {
        guard let searchBar = searchController?.searchBar else { return }
        if searchBar.canBecomeFirstResponder {
            searchController?.searchBar.becomeFirstResponder()
        }
    }
    
    private func setupDragGesture() {
        dragGesture = UIPanGestureRecognizer(target: self, action: #selector(HomeMapViewController.handleDragGesture))
        guard let dragGesture = dragGesture else { return }
        blurPhotosHeaderView?.addGestureRecognizer(dragGesture)
    }
    
    //MARK: - IBActions
    
    @IBAction func closeAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func bookmarkPlaceAction(_ sender: UIButton) {
        bookmarkButton?.image(for: .normal) == #imageLiteral(resourceName: "btn_bookmark_place_fill") ? deleteBookmarkedPlace() : bookmarkPlace()
    }
    
    @IBAction func uploadImageButtonTapped(_ sender: UIButton) {
        showImagePicker()
    }
    
    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        Analytics.logEvent(with: .mapModeRefresh)
        let coordinates = selectedLocation != nil ? selectedLocation! : currentLocation!
        loadPinSelectedPhotos(coordinates)
    }
    
    @IBAction func currentLocationButtonTapped(_ sender: UIButton) {
        clearMap()
        keepCurrentLocationMarker()
        locationManager?.startUpdatingLocation()
        guard let currentLocation = currentLocation else { return }
        mapView?.camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 10.0)
    }
    
    //MARK: - Methods
    
    @objc func handleDragGesture() {
        guard let direction = dragGesture?.direction else { return}
        switch direction {
        case .up:
            if dragGesture?.state == .ended {
                dragGesture?.isEnabled = false
                photosViewIsHidden ? hidePhotoView() : expandPhotosView()
            }
        case .down:
            if dragGesture?.state == .ended {
                dragGesture?.isEnabled = false
                guard let isPhotosViewExpanded = photosViewReference?.isPhotosViewExpanded else { return }
                isPhotosViewExpanded ? shrinkPhotosView() : hidePhotoView()
            }
        default:
            break
        }
    }
    
    private func expandPhotosView() {
        Analytics.logEvent(with: .mapModeGalleryOpened)
        photosViewReference?.expandPhotosCollectionView()
        photosViewHeightConstraint?.isActive = false
        photosViewFullHeightConstraint?.isActive = true
        animatePhotoView()
    }
    
    private func shrinkPhotosView() {
        photosViewReference?.expandPhotosCollectionView()
        photosViewFullHeightConstraint?.isActive = false
        photosViewHeightConstraint?.isActive = true
        animatePhotoView()
    }
    
    private func hidePhotoView() {
        guard let photosViewBounds = photosView?.bounds else { return }
        photosViewIsHidden = photosViewBottomConstraint?.constant == 0
        photosViewBottomConstraint?.constant = photosViewBottomConstraint?.constant == 0 ? -(photosViewBounds.height - 40) : 0
        animatePhotoView()
    }

    private func animatePhotoView() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in self.dragGesture?.isEnabled = true })
    }
    
    private func showImagePicker() {
        guard let imagePickerNavController = UIStoryboard.navigationController(withIdentifier: "ImagePickerNavController", storyBoardIdentifier: "ImagePickerViewController") else { return }
        guard let imagePickerViewController = imagePickerNavController.viewControllers.first as? ImagePickerViewController else { return }
        imagePickerViewController.locationManager = locationManager
        definesPresentationContext = false
        Analytics.logEvent(with: .mapModeImagePickerOpened)
        present(imagePickerNavController, animated: true) { self.definesPresentationContext = true }
    }
    
    //MARK: - PhotosViewDelegate
    
    func openViewController(_ viewController: UIViewController) {
        definesPresentationContext = false
        present(viewController, animated: true, completion: { self.definesPresentationContext = true })
    }
    
    //MARK: - SearchDelegate
    
    func mapItemSelected(mapItem: MKMapItem) {
        clearMap()
        let camera = GMSCameraPosition.camera(withLatitude: mapItem.placemark.coordinate.latitude, longitude: mapItem.placemark.coordinate.longitude, zoom: 10.0)
        let location = CLLocation(latitude: mapItem.placemark.coordinate.latitude, longitude: mapItem.placemark.coordinate.longitude)
        stablishSelectedLocation(mapItem.placemark.coordinate, getPhotos: true, location: location)
        photosViewBottomConstraint?.constant = 0
        mapView?.camera = camera
    }
    
}
