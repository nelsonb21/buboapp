//
//  SearchView.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/18/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import MapKit

protocol SearchDelegate {
    func mapItemSelected(mapItem: MKMapItem)
}

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {

    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var tableView: UITableView?
    var placesResults: [MKMapItem] = []
    var searchDelegate: SearchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            viewTopConstraint?.constant = -12
        } else {
            viewTopConstraint?.constant = 44
        }
        
        tableView?.register(withIdentifier: "SearchTableViewCell")
        tableView?.dataSource = self
        tableView?.delegate = self
        tableView?.tableFooterView = UIView()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    //MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell
        let mapItem = placesResults[indexPath.row]
        cell.setupCell(mapItem: mapItem)
        return cell
    }
    
    //MARK: - UITabBarDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapItem = placesResults[indexPath.row]
        searchDelegate?.mapItemSelected(mapItem: mapItem)
        Analytics.logEvent(with: .mapModeSearchSelected)
        dismiss(animated: true, completion: nil)
    }
    

    //MARK: - UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchBarText = searchController.searchBar.text else { return }
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchBarText
        let search = MKLocalSearch(request: request)
        
        search.start { (response, error) in
            guard let response = response else { return }
            self.placesResults = response.mapItems
            self.tableView?.reloadData()
        }
    }
    
}
