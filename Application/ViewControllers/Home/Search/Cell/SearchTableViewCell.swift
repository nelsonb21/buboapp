//
//  SearchTableViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/20/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import MapKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var searchLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(mapItem: MKMapItem) {
        var place: String = ""
        
        if let name = mapItem.name {
            place = name
        }
        
        if let city = mapItem.placemark.locality {
            place = place.appending(", "+city)
        }
        
        if let country = mapItem.placemark.country {
            place = place.appending(", "+country)
        }
        
        searchLabel?.text = place
    }
    
}
