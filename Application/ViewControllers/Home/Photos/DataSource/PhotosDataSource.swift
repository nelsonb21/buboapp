//
//  PhotosDataSource.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias PhotosDataSource = PhotosView

extension PhotosDataSource: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return HomePhotosSection.allValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let section = HomePhotosSection(rawValue: section) else { return 0 }
        switch section {
        case .photo:
            return photosArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let section = HomePhotosSection(rawValue: indexPath.section) else { return UICollectionViewCell() }
        switch section {
        case .photo:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotosCollectionViewCell", for: indexPath) as! PhotosCollectionViewCell
            cell.backgroundColor = UIColor.clear
            cell.setupPhotoCell(photosArray[indexPath.row])
            return cell
        }
    }

}
