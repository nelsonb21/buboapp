//
//  PhotosCollectionDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias PhotosCollectionDelegate = PhotosView

extension PhotosCollectionDelegate: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if !isPhotosViewExpanded {
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
        }
        
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = (collectionViewSize.width - 2)/3.0
        collectionViewSize.height = collectionViewSize.width
        collectionViewSize.height = collectionViewSize.width
        return collectionViewSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let section = HomePhotosSection(rawValue: indexPath.section) else { return }
        switch section {
        case .photo:
            let photo = photosArray[indexPath.row]
            openPhotoDetail(photo, index: indexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return !isPhotosViewExpanded ? CGFloat(integerLiteral: 10) : CGFloat(integerLiteral: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return !isPhotosViewExpanded ? CGFloat(integerLiteral: 10) : CGFloat(integerLiteral: 0)
    }
    
}
