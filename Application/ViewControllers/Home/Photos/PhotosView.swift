//
//  PhotosView.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/12/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

protocol PhotosViewDelegate: class {
    func openViewController(_ viewController: UIViewController)
}

class PhotosView: UIView, PhotoDetailDelegate {

    @IBOutlet weak var activityIndicatorView: UIView?
    @IBOutlet weak var emptyStateLabel: UILabel?
    @IBOutlet weak var collectionView: UICollectionView?
    var photosArray: [Photo] = []
    weak var delegate: PhotosViewDelegate?
    var locationCoordinates: CLLocationCoordinate2D?
    var isPhotosViewExpanded = false
    private var loadingImage: UIImageView?
    
    func setupCollectionView() {
        collectionView?.register(withIdentifier: "PhotosCollectionViewCell")
        collectionView?.register(withIdentifier: "PhotosOptionCollectionViewCell")
        collectionView?.dataSource = self
        collectionView?.delegate = self

        emptyStateLabel?.isHidden = !photosArray.isEmpty
        setupLoadingAnimation()
    }
    
    private func setupLoadingAnimation() {
        guard let frame = activityIndicatorView?.frame else { return }
        loadingImage = UIImageView(frame: CGRect(x: -frame.width/2, y: -frame.height/2, width: frame.width*2, height: frame.height*2))
        loadingImage?.backgroundColor = .clear
        loadingImage?.contentMode = .scaleToFill
        loadingImage?.isHidden = true
        loadingImage?.setupGif()
        guard let loadingImage = loadingImage else { return }
        activityIndicatorView?.addSubview(loadingImage)
    }
    
    func reloadCollectionView() {
        collectionView?.reloadData()
        emptyStateLabel?.isHidden = !photosArray.isEmpty
    }
    
    func openPhotoDetail(_ photo: Photo, index: Int) {
        guard let photoDetailNavController = UIStoryboard.navigationController(withIdentifier: "PhotoDetailNavController", storyBoardIdentifier: "PhotoDetailViewController") else { return }
        guard let photoDetailViewController = photoDetailNavController.viewControllers.first as? PhotoDetailViewController else { return }
        photoDetailViewController.photo = photo
        photoDetailViewController.photos = photosArray
        photoDetailViewController.index = index
        photoDetailViewController.delegate = self
        photoDetailViewController.modalTransitionStyle = .crossDissolve
        delegate?.openViewController(photoDetailNavController)
    }
    
    func expandPhotosCollectionView() {
        isPhotosViewExpanded = !isPhotosViewExpanded
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = isPhotosViewExpanded ? .vertical : .horizontal
        }
        collectionView?.alwaysBounceVertical = isPhotosViewExpanded
        collectionView?.alwaysBounceHorizontal = !isPhotosViewExpanded
        collectionView?.reloadData()
    }
    
    func showActivityIndicator() {
        emptyStateLabel?.isHidden = true
        loadingImage?.startLoadingAnimation()
    }
    
    func stopActivityIndicator () {
        loadingImage?.stopLoadingAnimation()
    }
    
    //MARK: - PhotoDetailDelegate
    
    func deletePhoto(_ photo: Photo) {
        guard let index = photosArray.index(of: photo) else { return }
        photosArray.remove(at: index)
        collectionView?.reloadData()
    }

}
