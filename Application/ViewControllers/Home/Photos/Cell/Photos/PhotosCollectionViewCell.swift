//
//  PhotosCollectionViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import Kingfisher

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photosImageView: UIImageView?
    @IBOutlet weak var videoIconImageView: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupPhotoCell(_ photo: Photo) {
        photosImageView?.image = #imageLiteral(resourceName: "icon_user_placeholder")
        let isVideoNil = photo.isVideo == nil ? false : true
        let isVideo = isVideoNil && photo.isVideo! == AssetType.isVideo.rawValue ? true : false
        
        videoIconImageView?.isHidden = !isVideo
        
        if !isVideo {
            let existVoiceNote = photo.voiceNoteUrl == "" || photo.voiceNoteUrl == nil
            videoIconImageView?.isHidden = existVoiceNote
            videoIconImageView?.image = #imageLiteral(resourceName: "icon_voiceNote_available")
        } else {
            videoIconImageView?.image = #imageLiteral(resourceName: "icon_video")
        }
        
        if isVideo {
            guard let videoThumbnail = photo.videoThumbnail, let url = URL(string: videoThumbnail) else { return }
            photosImageView?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        } else {
            guard let imageUrl = photo.imageUrl, let url = URL(string: imageUrl) else { return }
            photosImageView?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        }
    }
    
}
