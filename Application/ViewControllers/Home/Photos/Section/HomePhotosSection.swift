//
//  HomePhotosSection.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

enum HomePhotosSection: Int {

    case photo
    
    static let allValues: [HomePhotosSection] = [.photo]
    
}
