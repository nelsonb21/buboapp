//
//  ImagePickerCollectionDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 9/2/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import Photos

extension ImagePickerViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size.height
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let asset = LibraryController.shared.images?[(indexPath as NSIndexPath).item] else { return }
        guard let size = previewImageView?.frame.height else { return }
        
        nextButton?.isHidden = true
        loadingImage?.startLoadingAnimation()
        let options = PHImageRequestOptions()
        options.version = .current
        options.isSynchronous = false
        options.isNetworkAccessAllowed = true
        options.progressHandler = { (progress, error, stop, info) in
            print(progress)
        }
        
        LibraryController.shared.imageManager?.requestImage(for: asset, targetSize: CGSize(width: size, height: size), contentMode: .aspectFill, options: options) { result, info in
            if let information = info, let _ = information["PHImageFileURLKey"] {
                self.nextButton?.isHidden = false
                self.loadingImage?.stopLoadingAnimation()
            }
            
            self.previewImageView?.image = result
            self.asset = asset
            self.previewImageView?.isHidden = false
            self.backButton?.isHidden = false
        }
    }
    
}
