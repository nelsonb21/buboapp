//
//  ImagePickerCollectionViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 9/2/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class ImagePickerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView?
    
    var image: UIImage? {
        didSet {
            imageView?.image = image
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isSelected = false
    }
    
    override var isSelected : Bool {
        didSet {
            self.layer.borderColor = isSelected ? #colorLiteral(red: 0.6196078431, green: 0.5294117647, blue: 0.7568627451, alpha: 1).cgColor : UIColor.clear.cgColor
            self.layer.borderWidth = isSelected ? 2 : 0
        }
    }
    
}
