//
//  ImagePickerDataSource.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 9/2/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension ImagePickerViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return LibraryController.shared.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagePickerCollectionViewCell", for: indexPath) as? ImagePickerCollectionViewCell else { return UICollectionViewCell() }
        guard let asset = LibraryController.shared.images?[(indexPath as NSIndexPath).item] else { return UICollectionViewCell() }
        let currentTag = cell.tag + 1
        cell.tag = currentTag
        
        LibraryController.shared.imageManager?.requestImage(for: asset, targetSize: CGSize(width: collectionView.contentSize.height, height: collectionView.contentSize.height), contentMode: .aspectFill, options: nil) { result, info in
            if cell.tag == currentTag {
                cell.image = result
            }
        }
        return cell
    }
    
}
