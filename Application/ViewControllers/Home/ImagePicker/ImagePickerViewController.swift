//
//  ImagePickerViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 7/16/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import Photos

class ImagePickerViewController: AppViewController, CameraControllerDelegate, LibraryControllerDelegate {
    
    @IBOutlet weak var previewViewContainer: UIView?
    @IBOutlet weak var shotButton: UIButton?
    @IBOutlet weak var flashButton: UIButton?
    @IBOutlet weak var flipButton: UIButton?
    @IBOutlet weak var previewImageView: UIImageView?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var backButton: UIButton?
    @IBOutlet weak var nextButton: UIButton?
    @IBOutlet weak var videoProgressView: UIProgressView?
    
    var isOpenFromHome = true
    var locationManager: CLLocationManager?
    var asset: PHAsset?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(recordVideo))
        shotButton?.addGestureRecognizer(longPress)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isOpenFromHome {
            clearVideoLayer()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addLoadingAnimation(toView: view)
        if isOpenFromHome {
            guard let frame = previewViewContainer?.bounds else { return }
            CameraController.shared.initialize(frame)
            CameraController.shared.delegate = self
            guard let videoLayer = CameraController.shared.videoLayer else { return }
            previewViewContainer?.layer.addSublayer(videoLayer)
            isOpenFromHome = false
            
            if let hasVideo = CameraController.shared.device?.hasFlash {
                flashButton?.isHidden = !hasVideo
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        loadingImage?.removeFromSuperview()
        guard let sublayers = previewViewContainer?.layer.sublayers else { return }
        for sublayer in sublayers {
            sublayer.removeFromSuperlayer()
        }
    }
    
    func setupView() {
        LibraryController.shared.initialize()
        LibraryController.shared.delegate = self
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    //MARK: - IBActions
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        close()
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        if let photo = previewImageView?.image {
            openPhotoCreate(with: photo, location: asset?.location, videoURL: nil)
        }
    }
    
    @IBAction func shotAction(_ sender: UIButton) {
        self.shotButton?.isEnabled = false
        CameraController.shared.shotButtonPressed { (data, _) in
            guard let image = data as? UIImage else { self.shotButton?.isEnabled = true; return }
            self.openPhotoCreate(with: image, location: self.locationManager?.location, videoURL: nil)
        }
    }
    
    @IBAction func flashAction(_ sender: UIButton) {
        CameraController.shared.flashButtonPressed()
        flashButton?.setImage(CameraController.shared.device?.flashMode == .on ? #imageLiteral(resourceName: "btn_flash_selected") : #imageLiteral(resourceName: "btn_flash"), for: .normal)
    }
    
    @IBAction func flipAction(_ sender: UIButton) {
        CameraController.shared.flipButtonPressed()
        flashButton?.isHidden = CameraController.shared.device?.position == .front
    }
    
    @IBAction func showCameraAction(_ sender: UIButton) {
        collectionView?.deselectAllItems(animated: true)
        previewImageView?.image = nil
        previewImageView?.isHidden = true
        backButton?.isHidden = true
        nextButton?.isHidden = true
        asset = nil
    }

    //MARK: - Methods
    
    private func openPhotoCreate(with photo: UIImage?, location: CLLocation?, videoURL: URL?) {
        guard let photoNavigationController = UIStoryboard.navigationController(withIdentifier: "PhotoCreateNavigationController", storyBoardIdentifier: "PhotoCreateViewController") else { return }
        guard let photoViewController = photoNavigationController.viewControllers.first as? PhotoCreateViewController else { return }
        photoViewController.photo = photo
        photoViewController.videoURL = videoURL
        photoViewController.assetIsVideo = photo == nil
        photoViewController.location = location == nil ? locationManager?.location : location
        navigationController?.pushViewController(photoViewController, animated: true)
    }
    
    @objc func recordVideo(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            videoProgressView?.trackTintColor = .lightGray
            CameraController.shared.startRecording()
        case .ended:
            CameraController.shared.stopRecording()
        default:
            break
        }
    }
    
    private func clearVideoLayer() {
        guard let frame = previewViewContainer?.bounds else { return }
        CameraController.shared.setupVideoLayer(frame)
        guard let videoLayer = CameraController.shared.videoLayer else { return }
        previewViewContainer?.layer.addSublayer(videoLayer)
        CameraController.shared.startCamera()
        videoProgressView?.progress = 0.0
        videoProgressView?.trackTintColor = .clear
        shotButton?.isEnabled = true
    }
    
    //MARK: - CameraControllerDelegate
    
    func videoRecordFinished(with urlFile: URL) {
        openPhotoCreate(with: nil, location: locationManager?.location, videoURL: urlFile)
    }
    
    func updateVideoProgressView(with value: Float) {
        videoProgressView?.setProgress(value, animated: true)
    }
    
    //MARK: - LibraryControllerDelegate
    
    func reloadAssets() {
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }

}
