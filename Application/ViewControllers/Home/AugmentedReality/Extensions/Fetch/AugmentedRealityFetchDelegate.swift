//
//  AugmentedRealityFetchDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 4/14/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias AugmentedRealityFetchDelegate = HomeAugmentedRealityViewController

extension AugmentedRealityFetchDelegate {
    
    func getPhotos(with coordinates: CLLocationCoordinate2D) {
        indicator?.startAnimating()
        print("Get Photos with Coordinates")
        PhotoController.getPhotosByCoordinates(coordinates, distance: 2.0, completion: { response, error in
            ReportPostController.getReportedPosts(with: User.current) { (result, error) in
                self.indicator?.stopAnimating()
                self.refreshButton?.isEnabled = true
                self.refreshButton?.isHidden = false
                guard let photos = response as? [Photo] else { return }
                
                self.photos.removeAll()
                self.photos = photos
                
                print("Photos in location count: \(photos.count)")
                self.setupNoPhotosView(with: .noPhotosAround, shouldHide: photos.count == 0)
                
                self.photosAnnotations.removeAll()
                photos.forEach({ (photo) in
                    guard let photoAnnotation = PhotoAnnotation(photo: photo) else { return }
                    self.photosAnnotations.append(photoAnnotation)
                })
                
                self.setAnnotations(self.photosAnnotations)
                self.reload(reloadType: .annotationsChanged)
                self.photosViewReference?.photosArray = self.photos
                self.photosViewReference?.reloadCollectionView()
            }
        })
    }
    
    //MARK: - Request User Likes
    
    func requestUserLikesPhotos() {
        LikePhotoController.getLikes(with: User.current) { (result, error) in
            print(error?.description ?? "No error")
        }
    }
    
    func requestUserLikesPeople() {
        LikePeopleController.getLikes(with: User.current) { (result, error) in
            print(error?.description ?? "No error")
        }
    }
    
    func requestUserLikesPlaces() {
        PlaceController.getPlaces(with: User.current) { (result, error) in
            print(error?.description ?? "No error")
        }
    }

}
