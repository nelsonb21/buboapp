//
//  HomeAugmentedRealitySetup.swift
//  Bubo
//
//  Created by Nelson Bolivar on 10/5/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension HomeAugmentedRealityViewController {
    
    func setupView() {
        setupLoader()
        setupAugmentedReality()
        getCurrentLocation()
        setupOptions()
        setupSearchBar()
        setupMapView()
        requestUserLikesPhotos()
        requestUserLikesPeople()
        requestUserLikesPlaces()
        setupPhotosView()
        setupDragGesture()
        setupBanner()
    }

     func setupOptions() {
        profileOptionViewTopConstraint?.constant = K.optionsSetupTopConstraint
        galleryOptionViewTopConstraint?.constant = K.optionsSetupTopConstraint
        galleryView?.alpha = 0
        profileView?.alpha = 0
    }
    
    func setupMapView() {
        guard let homeMapViewController = UIStoryboard.navigationController(withIdentifier: "HomeMapNavController", storyBoardIdentifier: "HomeMapViewController") else { return }
        homeMapViewController.modalTransitionStyle = .crossDissolve
        self.homeMapViewController = homeMapViewController
    }
    
    func setupPhotosView() {
        view.layoutIfNeeded()
        guard let photosView = Bundle.main.loadNibNamed("PhotosView", owner: self, options: nil)?[0] as? PhotosView else { return }
        guard let bounds = blurPhotosView?.bounds else { return }
        photosView.frame = bounds
        photosView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        photosView.delegate = self
        photosView.photosArray = photos
        photosView.setupCollectionView()
        photosViewReference = photosView
        blurPhotosView?.addSubview(photosView)
        guard let height = self.photosView?.bounds.height else { return }
        photosViewBottomConstraint?.constant =  -height - UIDevice.current.safeAreaBottomInsetValue
        photosViewHeight = photosViewHeightConstraint?.constant
    }
    
    func setupDragGesture() {
        dragGesture = UIPanGestureRecognizer(target: self, action: #selector(handleDragGesture))
        guard let dragGesture = dragGesture else { return }
        blurPhotosHeaderView?.addGestureRecognizer(dragGesture)
    }
    
    func setupSearchBar() {
        let searchBar = UISearchBar()
        searchBar.tintColor = .black
        searchBar.barStyle = .default
        searchBar.backgroundColor = .clear
        searchBar.returnKeyType = .done
        searchBar.placeholder = "Search for places"
        searchBar.isUserInteractionEnabled = false
        
        navigationItem.titleView = searchBar
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    func setupLoader() {
        indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 34, height: 34), type: .ballSpinFadeLoader, color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), padding: nil)
        refreshView?.addSubview(indicator!)
    }
    
    func setupBanner() {
        UIView.animate(withDuration: 0.5, delay: 8, options: [.curveEaseOut], animations: {
            self.bannerView?.alpha = 0
        }) { _ in
            self.bannerView?.isHidden = true
        }
    }
    
    func setupNoPhotosView(with instruction: InstructionsMessages, shouldHide: Bool) {
        messageLabel?.attributedText = instruction.message
        setupNoPhotosView(shouldHide)
    }
    
    func setupNoPhotosView(_ shouldHide: Bool) {
        noPhotosView?.isHidden = !shouldHide
        noPhotosView?.alpha = shouldHide ? 0 : 1
        UIView.animate(withDuration: 0.5, delay: 0.1, options: [.curveEaseOut], animations: {
            self.noPhotosView?.alpha = shouldHide ? 1 : 0
        }, completion: nil )
    }
    
    func setupTapableView() {
        guard let titleView = navigationItem.titleView else { return }
        let tapableView = UIView(frame: CGRect(x: 0, y: K.tapableViewYPosition, width: titleView.bounds.width - K.rightNavigationItemWidth, height: K.navigationBarHeight))
        let tap = UITapGestureRecognizer(target: self, action: #selector(openSearchMode))
        tapableView.backgroundColor = .clear
        tapableView.addGestureRecognizer(tap)
        navigationController?.view.addSubview(tapableView)
    }
    
}
