//
//  AugmentedRealityDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 4/14/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias AugmentedRealityDelegate = HomeAugmentedRealityViewController

extension AugmentedRealityDelegate: ARDataSource, AugmentedRealityAnnotationViewDelegate, ARViewControllerDelegate {
    
    func setupAugmentedReality() {
        dataSource = self
        delegate = self
        presenter.maxDistance = 2000
        presenter.maxVisibleAnnotations = 25
        presenter.distanceOffsetMode = .automaticOffsetMinDistance
        presenter.distanceOffsetMultiplier = 0.25
        presenter.distanceOffsetMinThreshold = 0.5
        trackingManager.userDistanceFilter = 20
        trackingManager.reloadDistanceFilter = 75
        setAnnotations(photosAnnotations)
        uiOptions.debugMap = false
        uiOptions.closeButtonEnabled = false
    }
    
    //MARK: - ARDataSource
    
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AugmentedRealityAnnotationView()
        annotationView.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        return annotationView
    }
    
    //MARK: - AugmentedRealityAnnotationViewDelegate
    
    func didTouch(annotationView: AugmentedRealityAnnotationView) {
        if let annotation = annotationView.annotation as? PhotoAnnotation {
            guard let index = photosAnnotations.index(where: { (photoAnnotation) -> Bool in
                annotation == photoAnnotation
            }) else { return }
            openPhotoDetail(photos[index], index: index)
        }
    }
    
    //MARK: - ARViewControllerDelegate
    
    func getLocationAfterUserAllowAccess() {
        getCurrentLocation()
    }
    
}
