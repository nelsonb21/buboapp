//
//  HomeAugmentedRealityViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/26/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class HomeAugmentedRealityViewController: ARViewController, PhotosViewDelegate, PhotoDetailDelegate {
    
    var photos: [Photo] = []
    var photosAnnotations: [PhotoAnnotation] = []
    var optionsAreHidden = true
    var homeMapViewController: UINavigationController?
    
    var dragGesture: UIPanGestureRecognizer?
    var currentPlacemark: CLPlacemark?
    var photosViewReference: PhotosView?
    var indicator: NVActivityIndicatorView?
    
    //Constants
    struct K {
        static let navigationBarHeight: CGFloat = 44
        static let rightNavigationItemWidth: CGFloat = 60.0
        static let tapableViewYPosition: CGFloat = UIDevice.current.safeAreaHasTopInsetValue ? 49 : 25
        static let optionsSetupTopConstraint: CGFloat = UIDevice.current.safeAreaHasTopInsetValue ? -56 : -34
        static let optionsHiddenTopConstraint: CGFloat = 12
        static let optionsNotHiddenTopConstraint: CGFloat = -34
    }
    
    //IBOutlets
    @IBOutlet weak var refreshButton: UIButton?
    @IBOutlet weak var refreshView: UIView?
    @IBOutlet weak var bannerView: UIView?
    @IBOutlet weak var noPhotosView: UIView?
    @IBOutlet weak var messageLabel: UILabel?
    
    //OptionsView
    @IBOutlet weak var optionsView: UIView?
    @IBOutlet weak var galleryView: UIView?
    @IBOutlet weak var profileView: UIView?
    
    //Constraints
    @IBOutlet weak var profileOptionViewTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var galleryOptionViewTopConstraint: NSLayoutConstraint?
    
    //Photos View
    @IBOutlet weak var photosView: UIView?
    @IBOutlet weak var blurPhotosHeaderView: UIView?
    @IBOutlet weak var blurPhotosView: UIView?
    @IBOutlet weak var photosViewBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var placemarkLabel: UILabel?
    @IBOutlet weak var photosViewHeightConstraint: NSLayoutConstraint?
    var photosViewHeight: CGFloat?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent(with: .augmentedRealityOpened)
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTapableView()
        if photosViewBottomConstraint?.constant == 0 { stopCameraAndTracking() }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        NotificationCenter.default.addObserver(self, selector: #selector(startCameraAfterBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        setupStatusBar(with: .clear)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    // MARK: - IBActions
    
    @IBAction func openProfileButtonTapped(_ sender: UIButton) {
        stopCameraAndTracking()
        openProfile()
    }
    
    @IBAction func reloadButtonAction(_ sender: UIButton) {
        Analytics.logEvent(with: .augmentedRealityRefresh)
        getCurrentLocation()
    }
    
    @IBAction func rightBarButtonAction(_ sender: UIButton) {
        animateOptions()
    }
    
    @IBAction func optionsButtonAction(_ sender: UIButton) {}
    
    @IBAction func uploadImageButtonTapped(_ sender: UIButton) {
        stopCameraAndTracking()
        definesPresentationContext = false
        showImagePicker()
    }
    
    @IBAction func galleryButtonAction(_ sender: UIButton) {
        Analytics.logEvent(with: .augmentedRealityGalleryOpened)
        stopCameraAndTracking()
        expandPhotosView()
    }
    
    //MARK: - Methods
    
    @objc func handleDragGesture() {
        guard let direction = dragGesture?.direction else { return}
        switch direction {
        case .down:
            if dragGesture?.state == .ended {
                startCameraAndTracking(notifyLocationFailure: true)
                dragGesture?.isEnabled = false
                expandPhotosView()
            }
        default:
            break
        }
    }
    
    func openPhotoDetail(_ photo: Photo, index: Int) {
        guard let photoDetailNavController = UIStoryboard.navigationController(withIdentifier: "PhotoDetailNavController", storyBoardIdentifier: "PhotoDetailViewController") else { return }
        guard let photoDetailViewController = photoDetailNavController.viewControllers.first as? PhotoDetailViewController else { return }
        photoDetailViewController.photo = photo
        photoDetailViewController.photos = [photo]
        photoDetailViewController.delegate = self
        photoDetailViewController.modalPresentationStyle = .overCurrentContext
        photoDetailViewController.modalTransitionStyle = .crossDissolve
        photoDetailViewController.shouldDisablePageScroll = true
        present(photoDetailNavController, animated: true, completion: nil)
    }
    
    private func openProfile() {
        guard let profileNavController = UIStoryboard.navigationController(withIdentifier: "ProfileNavController", storyBoardIdentifier: "ProfileViewController") else { return }
        definesPresentationContext = false
        present(profileNavController, animated: true, completion: { self.definesPresentationContext = true })
    }
    
    @objc func openSearchMode() {
        guard let homeMapViewController = homeMapViewController else { return }
        guard let homeMap = homeMapViewController.viewControllers.first as? HomeMapViewController else { return }
        homeMap.isOpenFromHome = true
        definesPresentationContext = false
        present(homeMapViewController, animated: true, completion: {
            self.definesPresentationContext = true
        })
    }
    
    func getCurrentLocation() {
        guard CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse else {
            setupNoPhotosView(with: .noLocationAllowed, shouldHide: true)
            return
        }
        
        refreshButton?.isEnabled = false
        refreshButton?.isHidden = true
        guard let location = trackingManager.locationManager.location else { refreshButton?.isEnabled = true; refreshButton?.isHidden = false; return }
        getPhotos(with: location.coordinate)
        
        PlacemarkController.getReverseGeocodeLocation(with: location) { (placemark, _) in
            guard let placemark = placemark as? CLPlacemark else { return }
            self.currentPlacemark = placemark
            self.setupPlaceInformation(placemark: self.currentPlacemark)
        }
    }
    
    private func animateOptions() {
        self.profileOptionViewTopConstraint?.constant = optionsAreHidden ? K.optionsHiddenTopConstraint : K.optionsNotHiddenTopConstraint
        self.galleryOptionViewTopConstraint?.constant = optionsAreHidden ? K.optionsHiddenTopConstraint : K.optionsNotHiddenTopConstraint
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [.curveEaseOut], animations: {
            self.optionsView?.transform = self.optionsView!.transform.rotated(by: CGFloat(Double.pi/2))
            self.profileView?.alpha = self.optionsAreHidden ? 1 : 0
            self.galleryView?.alpha = self.optionsAreHidden ? 1 : 0
            self.view.layoutIfNeeded()
        }, completion: {_ in
            self.optionsAreHidden = !self.optionsAreHidden
        })
    }
    
    private func expandPhotosView() {
        photosViewReference?.expandPhotosCollectionView()
        guard let height = photosView?.bounds.height, let photosViewHeight = photosViewHeight else {
            return
        }
        
        if UIDevice.current.safeAreaHasTopInsetValue {
            photosViewHeightConstraint?.constant = CGFloat(photosViewHeight - K.navigationBarHeight)
        }
        
        photosViewBottomConstraint?.constant = photosViewBottomConstraint?.constant == 0 ? -height - UIDevice.current.safeAreaBottomInsetValue : 0
        animatePhotoView()
    }
    
    private func animatePhotoView() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in self.dragGesture?.isEnabled = true })
    }
    
    private func setupPlaceInformation(placemark: CLPlacemark?) {
        guard let placemark = placemark else { return }
        placemarkLabel?.text = PlacemarkController.getAddress(of: placemark)
    }
    
    private func showImagePicker() {
        guard let imagePickerNavController = UIStoryboard.navigationController(withIdentifier: "ImagePickerNavController", storyBoardIdentifier: "ImagePickerViewController") else { return }
        guard let imagePickerViewController = imagePickerNavController.viewControllers.first as? ImagePickerViewController else { return }
        imagePickerViewController.locationManager = trackingManager.locationManager
        imagePickerViewController.isOpenFromHome = true
        definesPresentationContext = false
        Analytics.logEvent(with: .augmentedRealityImagePickerOpened)
        present(imagePickerNavController, animated: true, completion: {
            self.definesPresentationContext = true
        })
    }
    
    @objc func startCameraAfterBecomeActive() {
        startCameraAndTracking(notifyLocationFailure: true)
    }

    //MARK: - PhotosViewDelegate
    
    func openViewController(_ viewController: UIViewController) {
        definesPresentationContext = false
        present(viewController, animated: true, completion: { self.definesPresentationContext = true })
    }
    
    //MARK: - PhotoDetailDelegate
    
    func deletePhoto(_ photo: Photo) {
        refreshButton?.isEnabled = false
        refreshButton?.isHidden = true
        guard let location = trackingManager.locationManager.location else { refreshButton?.isEnabled = true; return }
        getPhotos(with: location.coordinate)
    }
    
}
