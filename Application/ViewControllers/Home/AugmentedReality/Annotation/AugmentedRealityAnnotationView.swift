//
//  AugmentedRealityAnnotationView.swift.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/24/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

protocol AugmentedRealityAnnotationViewDelegate {
    func didTouch(annotationView: AugmentedRealityAnnotationView)
}

class AugmentedRealityAnnotationView: ARAnnotationView {
    var titleLabel: UILabel?
    var distanceLabel: UILabel?
    var imageView: UIImageView?
    var delegate: AugmentedRealityAnnotationViewDelegate?
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        loadUI()
    }
    
    func loadUI() {
        removeAllViews()
        titleLabel?.removeFromSuperview()
        distanceLabel?.removeFromSuperview()
        imageView?.removeFromSuperview()
        clipsToBounds = true
    
        imageView = UIImageView(frame: CGRect(x: frame.size.width/10, y: 0, width: 94, height: 94))
        imageView?.clipsToBounds = true
        imageView?.layer.cornerRadius = 23
        imageView?.contentMode = .scaleAspectFill
        imageView?.alpha = 0.7
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: (imageView?.frame.size.width)!/2, y: (imageView?.frame.size.height)!))
        path.addLine(to: CGPoint(x: (imageView?.frame.size.width)!/6.9, y:  (imageView?.frame.size.height)! - (imageView?.frame.size.height)!/2.5))
        path.addArc(withCenter: CGPoint(x: (imageView?.frame.size.width)!/2, y: 38), radius: 38, startAngle: -CGFloat((7*Double.pi)/6), endAngle: -(CGFloat)((11*Double.pi)/6), clockwise: true)
        path.addLine(to: CGPoint(x: (imageView?.frame.size.width)! - (imageView?.frame.size.width)!/6.9, y: (imageView?.frame.size.height)! - (imageView?.frame.size.height)!/2.5))
        path.close()
        UIColor.red.setFill()
        path.stroke()
        path.reversing()
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = (imageView?.bounds)!
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = UIColor.red.cgColor
        imageView?.layer.mask = shapeLayer;
        imageView?.layer.masksToBounds = true
        addSubview(imageView!)
        
        let footerView = UIView(frame: CGRect(x: 0, y: 94, width: frame.size.width, height: 26))
        footerView.backgroundColor = UIColor(white: 0.9, alpha: 0.8)
        footerView.clipsToBounds = true
        footerView.layer.cornerRadius = 4
        addSubview(footerView)
        
        let label = UILabel(frame: CGRect(x: 2, y: 94, width: frame.size.width - 4, height: 14))
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 1
        label.textColor = .darkGray
        label.textAlignment = .center
        addSubview(label)
        titleLabel = label
        
        distanceLabel = UILabel(frame: CGRect(x: 2, y: 108, width: frame.size.width - 4, height: 12))
        distanceLabel?.font = UIFont.systemFont(ofSize: 10)
        distanceLabel?.numberOfLines = 1
        distanceLabel?.textColor = #colorLiteral(red: 0.5019607843, green: 0.39608, blue: 1, alpha: 1)
        distanceLabel?.textAlignment = .center
        addSubview(distanceLabel!)
        
        if let annotation = annotation as? PhotoAnnotation {
            titleLabel?.text = annotation.title
            distanceLabel?.text = String(format: "%.2f km", annotation.distanceFromUser / 1000)
            guard let stringURL = annotation.imageUrl, let imageUrl = URL(string: stringURL) else { return }
            imageView?.setImage(with: imageUrl)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel?.frame = CGRect(x: 2, y: 94, width: frame.size.width - 4, height: 14)
        distanceLabel?.frame = CGRect(x: 2, y: 108, width: frame.size.width - 4, height: 12)
        imageView?.frame = CGRect(x: frame.size.width/10, y: 0, width: 94, height: 94)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.didTouch(annotationView: self)
    }
    
    func removeAllViews() {
        subviews.forEach { (view) in
            view.removeFromSuperview()
        }
    }
    
}
