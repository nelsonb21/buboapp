//
//  UserSignUpViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/14/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

fileprivate enum resultMessage: String {
    case fail = "Something when wrong!. Please try again later."
    case userExist = "User with the same email already exists. Please try again."
    
    var color: UIColor {
        return #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
    }
}

class UserSignUpViewController: AppViewController {

    @IBOutlet weak var nameTextField: UserTextField?
    @IBOutlet weak var emailTextField: UserTextField?
    @IBOutlet weak var passwordTextField: UserTextField?
    @IBOutlet weak var signUpButton: UIButton?
    @IBOutlet weak var resultLabel: UILabel?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addLoadingAnimation(toView: view)
    }
    
    //MARK: - Setup
    
    func setupView() {
        Analytics.logEvent(with: .signUpOpened)
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationBarBackgroundImageView?.backgroundColor = .clear
    }
    
    // MARK: - IBActions
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        if validateFields() {
            signUserUp()
        }
    }
    
    @IBAction func closeViewButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openLegal(_ sender: UIButton) {
        openLegal()
    }
    
    //MARK: - Methods
    
    private func goToHomeView() {
        let window = UIApplication.shared.delegate?.window
        window??.rootViewController?.dismiss(animated: true, completion: {
            self.instantiateHome(window)
        })
    }
    
    private func instantiateHome(_ window: UIWindow??) {
        guard let homeNavController = UIStoryboard.navigationController(withIdentifier: "HomeAugmentedRealityNavController", storyBoardIdentifier: "HomeAugmentedRealityViewController") else { return }
        window??.rootViewController = homeNavController
    }

    private func signUserUp() {
        let user = BackendlessUser()
        guard let name = nameTextField?.text else { return }
        guard let email = emailTextField?.text else { return }
        guard let password = passwordTextField?.text else { return }
        
        user.name = name as NSString!
        user.email = email as NSString!
        user.password = password as NSString!
        
        let properties = [User.Attributes.bio.rawValue: "",
                          User.Attributes.website.rawValue: "",
                          User.Attributes.profileImageUrl.rawValue: "",
                          User.Attributes.osType.rawValue: "iOS",
                          User.Attributes.osVersion.rawValue: UIDevice.current.systemVersion,
                          User.Attributes.phoneModel.rawValue: UIDevice().modelName,
                          User.Attributes.appVersion.rawValue: Bundle.main.fullAppVersion]
        
        user.updateProperties(properties)
        
        loadingImage?.startLoadingAnimation()
        signUpButton?.isEnabled = false
        UserController.signUserUp(with: user) { (user, error) in
            self.signUpButton?.isEnabled = true
            self.loadingImage?.stopLoadingAnimation()
            if error == nil {
                guard let user = user as? BackendlessUser else { return }
                let userID = user.objectId.abbreviatingWithTildeInPath
                KeychainWrapper.save(string: userID, key: .CustomerTokenData)
                Analytics.logEvent(with: .signUpCompleted)
                self.goToHomeView()
            } else if let error = error {
                self.resultLabel?.text = error.faultCode == "3033" ? resultMessage.userExist.rawValue : resultMessage.fail.rawValue
                self.resultLabel?.textColor = resultMessage.userExist.color
                return
            }
        }
    }
    
    private func validateFields() -> Bool {
        return (nameTextField?.validate(isOptional: false))! && (emailTextField?.isValidEmail())! && (passwordTextField?.isValidPassword())!
    }
    
    private func openLegal() {
        guard let settingsWebViewController = UIStoryboard.viewController(withIdentifier: String(describing: SettingsWebViewController.self)) as? SettingsWebViewController else { return }
        settingsWebViewController.link = .legal
        Analytics.logEvent(with: .signUpLegal)
        navigationController?.pushViewController(settingsWebViewController, animated: true)
    }
    
}
