//
//  ForgotPasswordViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 8/8/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

fileprivate enum resultMessage: String {
    
    case success = "Check your email"
    case fail = "Something when wrong!. Please try again later"
    case wrongUser = "The email you entered is not register"
    
    var color: UIColor {
        switch self {
        case .success:
            return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        default:
            return #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        }
    }
}

class ForgotPasswordViewController: AppViewController {

    @IBOutlet weak var emailTextfield: UserTextField?
    @IBOutlet weak var resetPasswordButton: UIButton?
    @IBOutlet weak var resultLabel: UILabel?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addLoadingAnimation(toView: view)
    }
    
    //MARK: - Setup
    
    func setupView() {
        Analytics.logEvent(with: .forgotPasswordOpened)
        if let font = UIFont(name: "CaviarDreams-Bold", size: 16.0) {
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: font]
        }
    }

    //MARK: - IBActions
    
    @IBAction func resetPasswordAction(_ sender: UIButton) {
        if validateFields() {
            resetPassword()
        }
    }
    
    //MARK: - Methods
    
    private func resetPassword() {
        guard let email = emailTextfield?.text else { return }
        
        loadingImage?.startLoadingAnimation()
        resetPasswordButton?.isEnabled = false
        UserController.resetPassword(with: email) { (result, error) in
            Analytics.logEvent(with: .forgotPasswordCompleted)
            self.resetPasswordButton?.isEnabled = true
            self.emailTextfield?.resignFirstResponder()
            self.loadingImage?.stopLoadingAnimation()
            if error == nil {
                self.resultLabel?.text = resultMessage.success.rawValue
                self.resultLabel?.textColor = resultMessage.success.color
                return
            }
            
            if let error = error {
                self.resultLabel?.text = error.faultCode == "3020" ? resultMessage.wrongUser.rawValue : resultMessage.fail.rawValue
                self.resultLabel?.textColor = resultMessage.wrongUser.color
                return
            }
        }
    }
    
    private func validateFields() -> Bool {
        return (emailTextfield?.isValidEmail())!
    }

}
