//
//  UserSignInViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/14/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

fileprivate enum resultMessage: String {
    case fail = "Something when wrong!. Please try again later."
    case wrongUser = "Invalid email or password. Please try again."
    case userExist = "User with the same email already exists. Click on Forgot Password to recover it."
    
    var color: UIColor {
        return #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
    }
}

class UserSignInViewController: AppViewController {
    
    @IBOutlet weak var emailTextField: UserTextField?
    @IBOutlet weak var passwordTextField: UserTextField?
    @IBOutlet weak var signInButton: UIButton?
    @IBOutlet weak var resultLabel: UILabel?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addLoadingAnimation(toView: view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //MARK: - Setup
    
    func setupView() {
        Analytics.logEvent(with: .signInOpened)
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationBarBackgroundImageView?.backgroundColor = .clear
    }

    // MARK: - IBActions
    
    @IBAction func signInButtonTapped(_ sender: UIButton) {
        if validateFields() {
            loginUserAsync()
        }
    }
    
    @IBAction func signInWithFacebookButtonTapped(_ sender: UIButton) {
        loginWithFacebook()
    }
    
    @IBAction func closeViewButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func passwordButtonTapped(_ sender: UIButton) {
        openForgotPassword()
    }
    
    //MARK: - Methods
    
    private func openSignUpView() {
        guard let userSignUpViewController = UIStoryboard.viewController(withIdentifier: String(describing: UserSignUpViewController.self)) as? UserSignUpViewController else { return }
        navigationController?.pushViewController(userSignUpViewController, animated: true)
    }
    
    private func goToHomeView() {
        let window = UIApplication.shared.delegate?.window
        window??.rootViewController?.dismiss(animated: true, completion: { self.instantiateHome(window) })
    }
    
    private func instantiateHome(_ window: UIWindow??) {
        guard let homeNavController = UIStoryboard.navigationController(withIdentifier: "HomeAugmentedRealityNavController", storyBoardIdentifier: "HomeAugmentedRealityViewController") else { return }
        window??.rootViewController = homeNavController
    }
    
    private func openForgotPassword() {
        guard let forgotPasswordViewController = UIStoryboard.viewController(withIdentifier: String(describing: ForgotPasswordViewController.self)) as? ForgotPasswordViewController else { return }
        navigationController?.pushViewController(forgotPasswordViewController, animated: true)
    }
    
    private func loginUserAsync() {
        guard let email = emailTextField?.text else { return }
        guard let password = passwordTextField?.text else { return }
        
        signInButton?.isEnabled = false
        loadingImage?.startLoadingAnimation()
        UserController.logUserIn(with: email, password: password) { (user, error) in
            self.signInButton?.isEnabled = true
            self.loadingImage?.stopLoadingAnimation()
            if error == nil {
                guard let user = user as? BackendlessUser else { return }
                let userID = user.objectId.abbreviatingWithTildeInPath
                KeychainWrapper.save(string: userID, key: .CustomerTokenData)
                Analytics.logEvent(with: .signInCompleted)
                self.goToHomeView()
            } else if let error = error {
                self.resultLabel?.text = error.faultCode == "3003" ? resultMessage.wrongUser.rawValue : resultMessage.fail.rawValue
                self.resultLabel?.textColor = resultMessage.wrongUser.color
                return
            }
        }
    }
    
    private func loginWithFacebook() {
        guard let viewController = UIViewController.topViewController else { return }

        loadingImage?.startLoadingAnimation()
        FacebookController.logIn(with: viewController) { (user, error) in
            self.loadingImage?.stopLoadingAnimation()
            if error == nil && user != nil {
                guard let user = user as? BackendlessUser else { return }
                let userID = user.objectId.abbreviatingWithTildeInPath
                KeychainWrapper.save(string: userID, key: .CustomerTokenData)
                Analytics.logEvent(with: .signInWithFacebookCompleted)
                self.goToHomeView()
            } else if let error = error {
                
                if error.faultCode == "3003" {
                    self.resultLabel?.text = resultMessage.wrongUser.rawValue
                } else if error.faultCode == "3033" {
                    self.resultLabel?.text = resultMessage.userExist.rawValue
                } else {
                    self.resultLabel?.text = resultMessage.fail.rawValue
                }
                
                self.resultLabel?.textColor = resultMessage.wrongUser.color
                return
            }
        }
    }
    
    private func validateFields() -> Bool {
        return (emailTextField?.isValidEmail())! && (passwordTextField?.isValidPassword())!
    }

}
