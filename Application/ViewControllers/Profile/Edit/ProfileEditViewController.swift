//
//  ProfileEditViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/27/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class ProfileEditViewController: AppViewController {
    
    let placeholderText = "About you"
    
    @IBOutlet weak var backgroundUserImage: UIImageView?
    @IBOutlet weak var userImage: UIImageView?
    @IBOutlet weak var imageButton: UIButton?
    
    @IBOutlet weak var userNameTextField: UITextField?
    @IBOutlet weak var bioTextView: UITextView?
    @IBOutlet weak var websiteTextField: UITextField?
    
    var isANewPhoto = false
    var newPhotoSelected = false
    var isInfoEdited = false
    
    //MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addLoadingAnimation(toView: view)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setupView() {
        setupBarButtonStyles()
        let user = User.current
        userNameTextField?.text = user.name.capitalized
        
        if let website = User.getValue(key: .website) as? String { websiteTextField?.text = website }
        
        if let bio = User.getValue(key: .bio) as? String {
            bioTextView?.text = bio == "" ? placeholderText : bio
            bioTextView?.textColor = bio == "" ? .lightGray : .black
        }
        bioTextView?.delegate = self
    
        if let profileImageUrl = User.getValue(key: .profileImageUrl) as? String {
            guard let imageUrl = URL(string: profileImageUrl) else {
                backgroundUserImage?.image = #imageLiteral(resourceName: "icon_user_placeholder")
                userImage?.image = #imageLiteral(resourceName: "icon_user_placeholder")
                return
            }
            backgroundUserImage?.setForceRefreshImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
            userImage?.setForceRefreshImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        }
    }
    
    private func setupBarButtonStyles() {
        navigationBarBackgroundImageView?.backgroundColor = .clear
    }
    
    //MARK: - IBActions
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeUserPhotoAction(_ sender: UIButton) {
        showImageSourceMenu()
    }
    
    @IBAction func saveUserAction(_ sender: UIButton) {
        validateInfo() ? updateUserProperties() : showErrorAlert()
    }
    
    //MARK: - Methods
    
    private func updateUserProperties() {
        
        guard let name = userNameTextField?.text else { return }
        guard var bio = bioTextView?.text else { return }
        guard let website = websiteTextField?.text else { return }
        
        bio = bio == placeholderText ? "" : bio
        
        Analytics.logEvent(with: .profileEditCompleted)
        var properties = [User.Attributes.bio.rawValue: bio,
                          User.Attributes.website.rawValue: website,
                          User.Attributes.name.rawValue: name,
                          User.Attributes.osType.rawValue: "iOS",
                          User.Attributes.osVersion.rawValue: UIDevice.current.systemVersion,
                          User.Attributes.phoneModel.rawValue: UIDevice().modelName,
                          User.Attributes.appVersion.rawValue: Bundle.main.fullAppVersion]

        if isANewPhoto {
            updateUserImage(completion: { (url) in
                guard let url = url else { return }
                properties[User.Attributes.profileImageUrl.rawValue] = url
                self.updateUser(properties: properties)
                return
            })
        } else {
            self.updateUser(properties: properties)
        }
    }
    
    private func updateUserImage(completion: @escaping (_ url: String?) -> Void) {
        guard let image = userImage?.image else { return }
        guard let imageData = UIImageJPEGRepresentation(image, 0.5) else { return }
        
        guard var photoName = User.current.objectId else { return }
        photoName = photoName.replacingOccurrences(of: "-", with: "") as NSString
        
        loadingImage?.startLoadingAnimation()
        UserController.updateUserPhoto(with: imageData, name: photoName as String) { response, error in
            if error == nil {
                guard let fileURL = response as? String else { completion(nil); return }
                completion(fileURL)
                return
            } else {
                self.loadingImage?.stopLoadingAnimation()
                completion(nil)
                return
            }
        }
    }
    
    private func updateUser(properties: [String: Any]) {
        let user = User.current
        user.updateProperties(properties)
        
        loadingImage?.startLoadingAnimation()
        UserController.updateUser(with: user) { (user, error) in
            self.loadingImage?.stopLoadingAnimation()
            if error == nil {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func showImageSourceMenu() {
        let menu = UIAlertController(title: "", message: "Upload profile photo from?", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera() })
        let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: { _ in self.openCameraRoll() })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        menu.addAction(cameraAction)
        menu.addAction(photoLibraryAction)
        menu.addAction(cancelAction)
        
        if let menu = menu.popoverPresentationController {
            menu.sourceView = imageButton
            menu.sourceRect = imageButton?.bounds ?? CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0)
        }
        
        present(menu, animated: true, completion: nil)
    }
    
    func showErrorAlert() {
        let alert = UIAlertController(title: "", message: "Some of the information is wrong!!!", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Validate
    func validateInfo() -> Bool {
        return (userNameTextField?.validate(isOptional: false))! && (websiteTextField?.isValidUrl())! && (bioTextView?.validate(isOptional: true))!
    }

}
