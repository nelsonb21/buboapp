//
//  ProfileViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 12/11/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

class ProfileViewController: AppViewController, ProfileHeaderDelegate, PhotoDetailDelegate, ProfilePlaceCollectionViewCellDelegate {
    
    var currentOption: ProfileOption = .myPhotos

    @IBOutlet weak var closeBarButtonItem: UIBarButtonItem?
    @IBOutlet weak var settingsBarButtonItem: UIBarButtonItem?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var imageBackgroundViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var imageBackgroundView: UIImageView?
    @IBOutlet weak var reportedUserView: UIView?
    
    var photosArray: [Photo] = []
    var placesArray: [Place] = []
    var likedPeopleArray: [LikePeople] = []
    var likedPhotosArray: [LikePhoto] = []
    
    var user = User.current
    
    var isOtherUserProfile = false
    var shouldHideEmptyState = true
    
    var headerHeight: CGSize?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Profile"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isOtherUserProfile ? getUserPhotos() : getMyPhotos()
        user = isOtherUserProfile ? user : User.current
        if isOtherUserProfile {
            settingsBarButtonItem?.image = #imageLiteral(resourceName: "icon_options")
        }
        setupBackground()
        collectionView?.reloadData()
    }
    
    func setupView() {
        Analytics.logEvent(with: .profileOpened)
        if let font = UIFont(name: "CaviarDreams-Bold", size: 16.0) {
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: font]
        }
        collectionView?.register(withIdentifier: "ProfilePhotoCollectionViewCell")
        collectionView?.register(withIdentifier: "ProfilePlaceCollectionViewCell")
        collectionView?.register(withIdentifier: "ProfilePeopleCollectionViewCell")
        collectionView?.register(withIdentifier: "ProfileEmptyCollectionViewCell")
        collectionView?.delegate = self
        collectionView?.dataSource = self
        setupReportedUser(user)
    }
    
    private func setupBackground() {
        guard let profileImageUrl = user.getValue(key: .profileImageUrl) as? String, let imageUrl = URL(string: profileImageUrl) else {
            imageBackgroundView?.image = #imageLiteral(resourceName: "icon_user_placeholder"); return
        }
        imageBackgroundView?.setImage(with: imageUrl)
    }
    
    //MARK: - IBActions
    
    @IBAction func closeButtonTapped(_ sender: UIBarButtonItem) {
        if isOtherUserProfile {
            _ = navigationController?.popViewController(animated: true)
        } else {
            guard let homeNav = presentingViewController as? UINavigationController else { return }
            guard let homeAR = homeNav.viewControllers.first as? HomeAugmentedRealityViewController else { return }
            dismiss(animated: true, completion: { homeAR.startCameraAndTracking(notifyLocationFailure: true) })
        }
    }
    
    @IBAction func editProfileTapped(_ sender: UIButton) {
        Analytics.logEvent(with: .profileEditOpened)
        openEditProfile()
    }
    
    @IBAction func settingsButtonTapped(_ sender: UIBarButtonItem) {
        if isOtherUserProfile{
            openOptions()
        } else {
            openSettings()
        }
    }
    
    //MARK: - Methods
    
    func openPlaceDetail(place: Place) {
        guard let photoCollectionViewController = UIStoryboard.viewController(withIdentifier: "PhotoCollectionViewController") as? PhotoCollectionViewController else { return }
        photoCollectionViewController.place = place
        navigationController?.pushViewController(photoCollectionViewController, animated: true)
    }
    
    func openEditProfile() {
        guard let profileEditNavController = UIStoryboard.navigationController(withIdentifier: "ProfileEditNavController", storyBoardIdentifier: "ProfileEditViewController") else { return }
        present(profileEditNavController, animated: true, completion: nil)
    }
    
    func openDetail(with photo: Photo, index: Int, option: ProfileOption) {
        guard let photoDetailNavController = UIStoryboard.navigationController(withIdentifier: "PhotoDetailNavController", storyBoardIdentifier: "PhotoDetailViewController") else { return }
        guard let photoDetailViewController = photoDetailNavController.viewControllers.first as? PhotoDetailViewController else { return }
        photoDetailViewController.index = index
        photoDetailViewController.option = option
        photoDetailViewController.photo = photo
        photoDetailViewController.delegate = self
        if option == .photosLiked { photoDetailViewController.likedPhotos = likedPhotosArray } else { photoDetailViewController.photos = photosArray }
        present(photoDetailNavController, animated: true, completion: nil)
    }
    
    func openOtherUserProfile(user: BackendlessUser) {
        guard let profileViewController = UIStoryboard.viewController(withIdentifier: "ProfileViewController") as? ProfileViewController else { return }
        profileViewController.isOtherUserProfile = true
        profileViewController.user = user
        navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    private func openSettings() {
        guard let settingsDetailViewController = UIStoryboard.viewController(withIdentifier: "SettingsDetailViewController") as? SettingsDetailViewController else { return }
        navigationController?.pushViewController(settingsDetailViewController, animated: true)
    }
    
    private func openOptions() {
        let alertView = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let reportActionInappropriate = UIAlertAction(title: "Report user as inappropriate", style: .default) { _ in
            Analytics.logEvent(with: .profileReportUserInappropriate)
            self.reportUser(with: "inappropriate")
        }
        alertView.addAction(reportActionInappropriate)
        
        let reportActionSpam = UIAlertAction(title: "Report user as spam", style: .default) { _ in
            Analytics.logEvent(with: .profileReportUserSpam)
            self.reportUser(with: "spam")
        }
        alertView.addAction(reportActionSpam)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertView.addAction(cancelAction)
        
        if let alertView = alertView.popoverPresentationController {
            alertView.barButtonItem = settingsBarButtonItem
        }
        
        present(alertView, animated: true, completion: nil)
    }
    
    private func reportUser(with reason: String) {
        reportedUserView?.isHidden = false
        let reportUser = ReportUser(with: user, user: User.current, reason: reason)
        ReportUserController.reportUser(user: reportUser) { (result, error) in
            if error == nil {
                self.setupReportedUser(self.user)
            }
        }
    }
    
    //MARK: - ProfileHeaderDelegate
    
    func setupMenuOption(option: ProfileOption) {
        currentOption = option
        getData(option: option)
    }
    
    private func getData(option: ProfileOption) {
        switch option {
        case .myPhotos:
            Analytics.logEvent(with: .profileMyPhotosOpened)
            getMyPhotos()
        case .places:
            Analytics.logEvent(with: .profileFavoritePlacesOpened)
            getPlaces()
        case .people:
            Analytics.logEvent(with: .profileFollowedPeopleOpened)
            getLikedPeople()
        case .photosLiked:
            Analytics.logEvent(with: .profileFavoritePhotosOpened)
            getLikedPhotos()
        }
    }
    
    //MARK: - PhotoDetailDelegate
    
    func deletePhoto(_ photo: Photo) {
        if let indexMyPhotos = photosArray.index(of: photo) { photosArray.remove(at: indexMyPhotos) }
        
        if let indexLikedPhotos = likedPhotosArray.index(where: { (likedPhoto) -> Bool in
            likedPhoto.photo == photo
        }) {
            likedPhotosArray.remove(at: indexLikedPhotos)
        }
    }
    
    //MARK: - ProfilePlaceCollectionViewCellDelegate
    
    func reload() {
        placesArray = PlaceController.likePlaces
        collectionView?.reloadData()
    }
    
}
