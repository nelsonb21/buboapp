//
//  ProfileDataSource.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 12/11/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias ProfileDataSource = ProfileViewController

extension ProfileDataSource: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        switch currentOption {
        case .myPhotos:
            return 1
        case .places:
            return 1
        case .people:
            return 1
        case .photosLiked:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch currentOption {
        case .myPhotos:
            return photosArray.count > 0 ? photosArray.count : 1
        case .places:
            return placesArray.count > 0 ? placesArray.count : 1
        case .people:
            return likedPeopleArray.count > 0 ? likedPeopleArray.count : 1
        case .photosLiked:
            return likedPhotosArray.count > 0 ? likedPhotosArray.count : 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProfileHeaderView", for: indexPath) as! ProfileHeaderCollectionReusableView
        headerView.delegate = self
        headerView.setupCell(user: user, isOtherUserProfile: isOtherUserProfile)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch currentOption {
        case .myPhotos:
            if photosArray.count > 0 {
                let photo = photosArray[indexPath.row]
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePhotoCollectionViewCell", for: indexPath) as! ProfilePhotoCollectionViewCell
                cell.setupCell(with: photo)
                return cell
            } else {
                return setEmptyState(collectionView, indexPath: indexPath, option: currentOption)
            }
        case .places:
            if placesArray.count > 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePlaceCollectionViewCell", for: indexPath) as! ProfilePlaceCollectionViewCell
                cell.setupCell(with: placesArray[indexPath.row])
                cell.delegate = self
                return cell
            } else {
                return setEmptyState(collectionView, indexPath: indexPath, option: currentOption)
            }
        case .people:
            if likedPeopleArray.count > 0, let userLiked = likedPeopleArray[indexPath.row].userLiked {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePeopleCollectionViewCell", for: indexPath) as! ProfilePeopleCollectionViewCell
                cell.setupCell(with: userLiked)
                return cell
            } else {
                return setEmptyState(collectionView, indexPath: indexPath, option: currentOption)
            }
        case .photosLiked:
            if likedPhotosArray.count > 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePhotoCollectionViewCell", for: indexPath) as! ProfilePhotoCollectionViewCell
                let likedPhoto = likedPhotosArray[indexPath.row]
                cell.setupCell(with: likedPhoto.photo)
                return cell
            } else {
                return setEmptyState(collectionView, indexPath: indexPath, option: currentOption)
            }
        }
    }
    
    private func setEmptyState(_ collectionView: UICollectionView, indexPath: IndexPath, option: ProfileOption) -> ProfileEmptyCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileEmptyCollectionViewCell", for: indexPath) as! ProfileEmptyCollectionViewCell
        cell.setupCell(with: option, shouldHide: shouldHideEmptyState, isOtherUserProfile: isOtherUserProfile)
        return cell
    }
    
}
