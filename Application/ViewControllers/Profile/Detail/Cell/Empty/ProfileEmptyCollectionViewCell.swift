//
//  ProfileEmptyCollectionViewCell.swift
//  Bubo
//
//  Created by Nelson Bolivar on 9/10/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class ProfileEmptyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var emptyStateView: UIView?
    @IBOutlet weak var emptyStateLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(with option: ProfileOption, shouldHide: Bool, isOtherUserProfile: Bool)  {
        emptyStateView?.isHidden = shouldHide
        
        switch option {
        case .myPhotos:
            emptyStateLabel?.text = isOtherUserProfile ? "Nothing to see here :(" : "What are you waiting for to leave your mark?"
        case .places:
            emptyStateLabel?.text = "Save all the places you like and find them here"
        case .people:
            emptyStateLabel?.text = "Follow people all over the world and discover new amazing places"
        case .photosLiked:
            emptyStateLabel?.text = "You have no favorites. Go explore!"
        }
    }

}
