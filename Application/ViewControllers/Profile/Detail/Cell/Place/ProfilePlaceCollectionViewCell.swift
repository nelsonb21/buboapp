//
//  ProfilePlaceCollectionViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/19/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

protocol ProfilePlaceCollectionViewCellDelegate {
    func reload()
}

class ProfilePlaceCollectionViewCell: UICollectionViewCell {
    
    var place: Place?
    var delegate: ProfilePlaceCollectionViewCellDelegate?

    @IBOutlet weak var placeImageView: UIImageView?
    @IBOutlet weak var placeNameLabel: UILabel?
    @IBOutlet weak var placeCityLabel: UILabel?
    @IBOutlet weak var bookmarkPlaceButton: UIButton?
    @IBOutlet weak var bookmarkPlaceIcon: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(with place: Place) {
        
        self.place = place
        
        if let imageUrl = place.imageUrl {
            placeImageView?.setImageKF(with: imageUrl)
        } else {
            placeImageView?.image = #imageLiteral(resourceName: "icon_user_placeholder")
        }
        
        if let city = place.city {
            placeCityLabel?.text = city
        } else {
            placeCityLabel?.text = ""
        }
        
        if let name = place.name {
            placeNameLabel?.text = name
        } else {
            placeNameLabel?.text = ""
        }
        
        bookmarkPlaceButton?.removeTarget(self, action: nil, for: .allEvents)
        bookmarkPlaceButton?.addTarget(self, action: #selector(ProfilePlaceCollectionViewCell.bookmarkPlaceAction), for: .touchUpInside)
        
        bookmarkPlaceIcon?.image = #imageLiteral(resourceName: "btn_bookmark_place_fill")
        
    }
    
    @objc func bookmarkPlaceAction() {
        if self.bookmarkPlaceIcon?.image == #imageLiteral(resourceName: "btn_bookmark_place_fill") {
            unBookmarkPlace()
        } else {
            bookmarkPlace()
        }
    }
    
    private func unBookmarkPlace() {
        guard let place = place else { return }
        PlaceController.deletePlace(with: place) { (result, error) in
            Analytics.logEvent(with: .profileDislikePlace)
            if error == nil {
                self.bookmarkPlaceIcon?.image = #imageLiteral(resourceName: "btn_bookmark_place_empty")
            } else {
                self.bookmarkPlaceIcon?.image = #imageLiteral(resourceName: "btn_bookmark_place_fill")
            }
            self.delegate?.reload()
        }
    }
    
    private func bookmarkPlace() {
        place?.objectId = nil
        place?.user = nil
        guard let place = place else { return }
        PlaceController.bookmarkPlace(place: place) { (result, error) in
            Analytics.logEvent(with: .profileLikePlace)
            if error == nil {
                self.bookmarkPlaceIcon?.image = #imageLiteral(resourceName: "btn_bookmark_place_fill")
            } else {
                self.bookmarkPlaceIcon?.image = #imageLiteral(resourceName: "btn_bookmark_place_empty")
            }
            self.delegate?.reload()
        }
    }

}
