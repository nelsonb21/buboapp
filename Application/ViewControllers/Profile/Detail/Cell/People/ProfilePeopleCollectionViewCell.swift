//
//  ProfilePeopleCollectionViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class ProfilePeopleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImage: UIImageView?
    @IBOutlet weak var userNameLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(with user: BackendlessUser) {
        userNameLabel?.text = user.name.capitalized
        if let urlString = user.getValue(key: .profileImageUrl) as? String, let url = URL(string: urlString) {
            userImage?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        } else {
            userImage?.image = #imageLiteral(resourceName: "icon_user_placeholder")
        }
    }

}
