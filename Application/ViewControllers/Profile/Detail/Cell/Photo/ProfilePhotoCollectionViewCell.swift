//
//  ProfilePhotoCollectionViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 12/11/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

class ProfilePhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var videoIconImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(with photo: Photo?)  {
        imageView?.image = #imageLiteral(resourceName: "icon_user_placeholder")
        guard photo != nil else { return }
        let isVideoNil = photo?.isVideo == nil ? false : true
        let isVideo = isVideoNil && photo?.isVideo! == AssetType.isVideo.rawValue ? true : false
        
        videoIconImageView?.isHidden = !isVideo
        
        if !isVideo {
            let existVoiceNote = photo?.voiceNoteUrl == "" || photo?.voiceNoteUrl == nil
            videoIconImageView?.isHidden = existVoiceNote
            videoIconImageView?.image = #imageLiteral(resourceName: "icon_voiceNote_available")
        } else {
            videoIconImageView?.image = #imageLiteral(resourceName: "icon_video")
        }
        
        if isVideo {
            guard let videoThumbnail = photo?.videoThumbnail, let url = URL(string: videoThumbnail) else { return }
            imageView?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        } else {
            guard let imageUrl = photo?.imageUrl, let url = URL(string: imageUrl) else { return }
            imageView?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        }
    }
    
}
