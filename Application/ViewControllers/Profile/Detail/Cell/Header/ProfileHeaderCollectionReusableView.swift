//
//  ProfileHeaderCollectionReusableView.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/15/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

protocol ProfileHeaderDelegate {
    func setupMenuOption(option: ProfileOption)
}

fileprivate enum TabOptions: Int {
    case myPhotos
    case places
    case people
    case photosLiked
    
    var iconEmpty: UIImage {
        switch self {
        case .myPhotos:
            return #imageLiteral(resourceName: "icon_myPhotos")
        case .places:
            return #imageLiteral(resourceName: "icon_places")
        case .people:
            return #imageLiteral(resourceName: "icon_people")
        case .photosLiked:
            return #imageLiteral(resourceName: "icon_photosLiked")
        }
    }
    
    var iconFill: UIImage {
        switch self {
        case .myPhotos:
            return #imageLiteral(resourceName: "icon_myPhotosFill")
        case .places:
            return #imageLiteral(resourceName: "icon_placesFill")
        case .people:
            return #imageLiteral(resourceName: "icon_peopleFill")
        case .photosLiked:
            return #imageLiteral(resourceName: "icon_photosLikedFill")
        }
    }
}

class ProfileHeaderCollectionReusableView: UICollectionReusableView {
    
    var delegate: ProfileHeaderDelegate?
    var user: BackendlessUser?
    
    @IBOutlet var profileTabViews: [UIView]?
    @IBOutlet var profileTabButtons: [UIButton]?
    @IBOutlet var profileTabImages: [UIImageView]?
    
    @IBOutlet weak var usernameLabel: UILabel?
    @IBOutlet weak var bioLabel: UILabel?
    @IBOutlet weak var websiteButton: UIButton?
    @IBOutlet weak var userImage: UIImageView?
    
    @IBOutlet weak var editButton: UIButton?
    @IBOutlet weak var followButton: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.9725490196, blue: 1, alpha: 1)
        websiteButton?.contentHorizontalAlignment = .left
        setupProfileImage()
    }
    
    func setupCell(user: BackendlessUser, isOtherUserProfile: Bool) {
        self.user = user
        isOtherUserProfile ? setupCellOtherUser(user: user) : setupCellCurrentUser(user: user)
        layoutIfNeeded()
    }
    
    private func setupCellCurrentUser(user: BackendlessUser) {
        followButton?.isHidden = true
        usernameLabel?.text = user.name.capitalized
        bioLabel?.text = user.getValue(key: .bio) as? String ?? ""
        
        websiteButton?.removeTarget(self, action: nil, for: .allEvents)
        if let link = user.getValue(key: .website) as? String {
            websiteButton?.setTitle(link, for: .normal)
            websiteButton?.addTarget(self, action: #selector(openWebsite), for: .touchUpInside)
        }
        
        if let profileImageUrl = user.getValue(key: .profileImageUrl) as? String {
            guard let imageUrl = URL(string: profileImageUrl) else {
                userImage?.image = #imageLiteral(resourceName: "icon_user_placeholder")
                return
            }
            
            userImage?.setForceRefreshImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        }
    }
    
    private func setupCellOtherUser(user: BackendlessUser) {
        setupCellCurrentUser(user: user)
        
        editButton?.isHidden = true
        followButton?.isHidden = false
        profileTabViews?.forEach({ (view) in
            if view.tag != 1 {
                view.removeFromSuperview()
            }
        })
        
        profileTabButtons?.forEach({ (button) in
            if button.tag == 1 {
                button.setTitle("", for: .normal)
                button.isEnabled = false
            }
        })
        
        setupFollowPeopleButton()
    }
    
    //MARK: - IBActions
    
    @IBAction func profileTabAction(_ sender: UIButton) {
        guard let index = profileTabButtons?.index(where: { (button) -> Bool in
            button == sender
        }) else { return }
        
        var i = 0
        profileTabImages?.forEach({ (imageView) in
            imageView.image = TabOptions(rawValue: i)?.iconEmpty
            i += 1
        })
        
        profileTabImages?[index].image = TabOptions(rawValue: index)?.iconFill
        
        guard let optionSelected = getProfileOption(optionSelected: sender) else { return }
        delegate?.setupMenuOption(option: optionSelected)
    }
    
    //MARK: - Methods
    
    func getProfileOption(optionSelected: UIButton) -> ProfileOption? {
        switch optionSelected.tag {
        case 1:
            return .myPhotos
        case 2:
            return .places
        case 3:
            return .people
        case 4:
            return .photosLiked
        default:
            return nil
        }
    }
    
    @objc func openWebsite() {
        guard let urlString = websiteButton?.title(for: .normal) else { return }
        guard let url = URL(string: "http://"+urlString) else { return }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    private func setupProfileImage() {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 47, y: 94))
        path.addLine(to: CGPoint(x: 14, y:  56))
        path.addArc(withCenter: CGPoint(x: 47, y: 37.7), radius: 37.7, startAngle: -CGFloat((7*Double.pi)/6), endAngle: -(CGFloat)((11*Double.pi)/6), clockwise: true)
        path.addLine(to: CGPoint(x: 80, y:  56))
        path.close()
        path.stroke()
        
        let shapeLayer = CAShapeLayer()
        guard let bounds = userImage?.bounds else { return }
        shapeLayer.frame = bounds
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = UIColor.red.cgColor
        userImage?.layer.mask = shapeLayer
        userImage?.layer.masksToBounds = true
        
        shapeLayer.setNeedsDisplay()
        userImage?.setNeedsDisplay()
        setNeedsDisplay()
        layoutIfNeeded()
    }
    
    private func setupFollowPeopleButton() {
        let isLiked = LikePeopleController.likePeople.contains { (likesPeople) -> Bool in
            likesPeople.userLiked?.objectId == self.user?.objectId
        }
        followButton?.setTitle(isLiked ? "Unfollow" : "Follow", for: .normal)
        followButton?.removeTarget(self, action: nil, for: .allEvents)
        
        if isLiked {
            followButton?.addTarget(self, action: #selector(dislikePeople), for: .touchUpInside)
        } else {
            followButton?.addTarget(self, action: #selector(likePeople), for: .touchUpInside)
        }
    }
    
    @objc func likePeople() {
        followButton?.setTitle("Unfollow", for: .normal)
        let like = LikePeople()
        like.userLiked = user
        like.datetime = Date()
        
        LikePeopleController.likePeople(like: like) { (result, error) in
            self.followButton?.setTitle(error == nil ? "Unfollow" : "Follow", for: .normal)
            Analytics.logEvent(with: .profileFollowUser)
        }
    }
    
    @objc func dislikePeople() {
        followButton?.setTitle("Follow", for: .normal)
        
        let likePeople = LikePeopleController.likePeople.first { (likePeople) -> Bool in
            likePeople.userLiked?.objectId == self.user?.objectId
        }
        
        if likePeople != nil {
            LikePeopleController.deleteLike(with: likePeople!) { (result, error) in
                self.followButton?.setTitle(error == nil ? "Follow" : "Unfollow", for: .normal)
                Analytics.logEvent(with: .profileUnfolloweUser)
            }
        }
    }
    
}
