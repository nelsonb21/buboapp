//
//  ProfileRequest.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias ProfileRequest = ProfileViewController

extension ProfileRequest {
    
    //MARK: - Requests
    
    func getMyPhotos() {
        collectionView?.reloadData()
        PhotoController.getPhotosByCurrentUser { (photos, error) in
            if error == nil {
                guard let photos = photos as? [Photo] else { return }
                self.photosArray = photos
                self.shouldHideEmptyState = false
                self.collectionView?.reloadData()
            }
        }
    }
    
    func getUserPhotos() {
        PhotoController.getPhotos(with: user) { (photos, error) in
            if error == nil {
                guard let photos = photos as? [Photo] else { return }
                self.photosArray = photos                
                self.shouldHideEmptyState = false
                self.collectionView?.reloadData()
            }
        }
    }
    
    func getPlaces() {
        shouldHideEmptyState = false
        placesArray = PlaceController.likePlaces
        collectionView?.reloadData()
    }
    
    func getLikedPeople() {
        shouldHideEmptyState = false
        likedPeopleArray = LikePeopleController.likePeople
        collectionView?.reloadData()
    }
    
    func getLikedPhotos() {
        shouldHideEmptyState = false
        likedPhotosArray.removeAll()
        LikePhotoController.likePhoto.forEach({ (likedPhoto) in
            if likedPhoto.photo != nil {
                likedPhotosArray.append(likedPhoto)
            }
        })
        collectionView?.reloadData()
    }

}
