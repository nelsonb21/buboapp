//
//  UserDetailReport.swift
//  Bubo
//
//  Created by Nelson Bolivar on 1/10/18.
//  Copyright © 2018 Nelson Bolivar. All rights reserved.
//

import Foundation

extension ProfileViewController {
    
    func setupReportedUser(_ user: BackendlessUser) {
        reportedUserView?.isHidden = !isUserReported(user)
    }
    
    private func isUserReported(_ user: BackendlessUser) -> Bool {
        let isReported = ReportUserController.reported.contains { (reported) -> Bool in
            reported.userReported?.objectId == user.objectId
        }
        return isReported
    }
    
}
