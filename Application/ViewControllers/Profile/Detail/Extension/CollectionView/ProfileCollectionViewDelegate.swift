//
//  ProfileCollectionViewDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 1/22/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias ProfileCollectionViewDelegate = ProfileViewController

extension ProfileCollectionViewDelegate: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch currentOption {
        case .myPhotos:
            return getCellSize(collectionView, cellPerRow: photosArray.count > 0 ? 3.0 : 1.0)
        case .places:
            return getCellSize(collectionView, cellPerRow: placesArray.count > 0 ? 2.0 : 1.0)
        case .people:
            return getCellSize(collectionView, cellPerRow: likedPeopleArray.count > 0 ? 3.0 : 1.0)
        case .photosLiked:
            return getCellSize(collectionView, cellPerRow: likedPhotosArray.count > 0 ? 3.0 : 1.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch currentOption {
        case .myPhotos:
            if photosArray.count > 0 {
                let photo = photosArray[indexPath.row]
                Analytics.logEvent(with: .profilePhotoOpened)
                openDetail(with: photo, index: indexPath.row, option: .myPhotos)
            }
        case .places:
            if placesArray.count > 0 {
                Analytics.logEvent(with: .profilePlaceOpened)
                let place = placesArray[indexPath.row]
                openPlaceDetail(place: place)
            }
        case .people:
            if likedPeopleArray.count > 0 {
                guard let userLiked = likedPeopleArray[indexPath.row].userLiked else { return }
                Analytics.logEvent(with: .profilePeopleOpened)
                openOtherUserProfile(user: userLiked)
            }
        case .photosLiked:
            if likedPhotosArray.count > 0 {
                let likedPhoto = likedPhotosArray[indexPath.row]
                guard let photo = likedPhoto.photo else { return }
                Analytics.logEvent(with: .profilePhotoOpened)
                openDetail(with: photo, index: indexPath.row, option: .photosLiked)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.frame.width - 30, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        label.font = UIFont(name: "Helvetica", size: 12.0)
        
        label.text = (user.getValue(key: .bio) as? String) ?? ""
        
        label.sizeToFit()
        let headerHeight = CGSize(width: collectionView.frame.width, height: label.frame.height + 189)
        self.headerHeight = headerHeight
        imageBackgroundViewHeightConstraint?.constant = label.frame.height + 189
        return headerHeight
    }
    
    private func getCellSize(_ collectionView: UICollectionView, cellPerRow: CGFloat) -> CGSize {
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = (collectionViewSize.width - 2)/cellPerRow //Display n elements in a row.
        collectionViewSize.height = collectionViewSize.width
        return collectionViewSize
    }
    
    //MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //updateHeader(animated: false)
    }
    
    func updateHeader(animated: Bool) {
        guard let collectionView = collectionView else { return }
        
        guard let defaultHeight = headerHeight else { return }
        let height = defaultHeight.height + (collectionView.contentOffset.y * -1)
        if height >= 0 { imageBackgroundViewHeightConstraint?.constant = height }
    }
    
}
