//
//  ProfileOption.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/19/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

enum ProfileOption {

    case myPhotos
    case places
    case people
    case photosLiked
    
}
