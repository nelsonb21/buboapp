//
//  MainViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/16/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    var showOnBoarding: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let isFirstTime = UserDefaults().object(forKey: UserDefaultKey.isFirstTime.rawValue)
        if isFirstTime == nil {
            UserDefaultWrapper.setData(value: false, key: .isFirstTime)
            KeychainWrapper.removeAll()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let _ = KeychainWrapper.getString(key: .CustomerTokenData) {
            let window = UIApplication.shared.delegate?.window
            instantiateHome(window)
        } else {
            instantiateOnBoarding()
        }
    }
    
    fileprivate func instantiateHome(_ window: UIWindow??) {
        guard let homeNavController = UIStoryboard.navigationController(withIdentifier: "HomeAugmentedRealityNavController", storyBoardIdentifier: "HomeAugmentedRealityViewController") else { return }
        window??.rootViewController = homeNavController
    }

    fileprivate func instantiateOnBoarding() {
        guard let onBoardingViewController = UIStoryboard.viewController(withIdentifier: "OnBoardingViewController") as? OnBoardingViewController else { return }
        present(onBoardingViewController, animated: true, completion: nil)
    }
    
}
