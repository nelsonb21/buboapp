//
//  PhotoCollectionViewDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/19/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias PhotoCollectionViewDelegate = PhotoCollectionViewController

extension PhotoCollectionViewDelegate: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = (collectionViewSize.width - 2)/3.0
        collectionViewSize.height = collectionViewSize.width
        collectionViewSize.height = collectionViewSize.width
        return collectionViewSize
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = photosArray[indexPath.row]
        openDetail(with: photo, index: indexPath.row)
    }

}
