//
//  PhotoCollectionViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/19/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class PhotoCollectionViewController: AppViewController, PhotoDetailDelegate {
    
    var place: Place?
    var photosArray: [Photo] = []
    var coordinates: CLLocationCoordinate2D?
    
    @IBOutlet weak var collectionView: UICollectionView?

    //MARK: - Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if coordinates == nil {
            guard let latitude = place?.coordinates?.latitude as? CLLocationDegrees else { return }
            guard let longitude = place?.coordinates?.longitude as? CLLocationDegrees else { return }
        
            let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            getPhotos(with: coordinates)
        } else {
            getPhotos(with: coordinates!)
        }
    }
    
    func setupView() {
        title = coordinates == nil ? place?.name! : ""
        collectionView?.register(withIdentifier: "PhotoCollectionCollectionViewCell")
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    //MARK: - Methods
    
    func openDetail(with photo: Photo, index: Int) {
        guard let photoDetailViewController = UIStoryboard.viewController(withIdentifier: String(describing: PhotoDetailViewController.self)) as? PhotoDetailViewController else { return }
        photoDetailViewController.photo = photo
        photoDetailViewController.photos = photosArray
        photoDetailViewController.index = index
        photoDetailViewController.delegate = self
        photoDetailViewController.isOpenFromCollection = true
        navigationController?.restorationIdentifier = ""
        navigationController?.pushViewController(photoDetailViewController, animated: true)
    }

    //MARK: - Requests
    
    func getPhotos(with coordinates: CLLocationCoordinate2D)  {
        PhotoController.getPhotosByCoordinates(coordinates, distance: 1.0) { (photos, error) in
            if error == nil {
                guard let photos = photos as? [Photo] else { return }
                self.photosArray = photos
                self.collectionView?.reloadData()
            }
        }
    }
    
    //MARK: - PhotoDetailDelegate
    
    func deletePhoto(_ photo: Photo) {
        guard let index = photosArray.index(of: photo) else { return }
        photosArray.remove(at: index)
        collectionView?.reloadData()
    }

}
