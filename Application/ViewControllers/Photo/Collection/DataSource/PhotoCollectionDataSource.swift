//
//  PhotoCollectionDataSource.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/19/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias PhotoCollectionDataSource = PhotoCollectionViewController

extension PhotoCollectionDataSource: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionCollectionViewCell", for: indexPath) as!PhotoCollectionCollectionViewCell
        cell.setupCell(photosArray[indexPath.row])
        return cell
    }

}
