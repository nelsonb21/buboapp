//
//  PhotoCollectionCollectionViewCell.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/19/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class PhotoCollectionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView?
    @IBOutlet weak var videoIconImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(_ photo: Photo) {
        photoImageView?.image = #imageLiteral(resourceName: "icon_user_placeholder")
        let isVideoNil = photo.isVideo == nil ? false : true
        let isVideo = isVideoNil && photo.isVideo! == AssetType.isVideo.rawValue ? true : false
        
        videoIconImageView?.isHidden = !isVideo
        
        if !isVideo {
            let existVoiceNote = photo.voiceNoteUrl == "" || photo.voiceNoteUrl == nil
            videoIconImageView?.isHidden = existVoiceNote
            videoIconImageView?.image = #imageLiteral(resourceName: "icon_voiceNote_available")
        } else {
            videoIconImageView?.image = #imageLiteral(resourceName: "icon_video")
        }
        
        if isVideo {
            guard let urlString = photo.videoThumbnail, let url = URL(string: urlString) else { return }
            photoImageView?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        } else {
            guard let urlString = photo.imageUrl, let url = URL(string: urlString) else { return }
            photoImageView?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        }
    }

}
