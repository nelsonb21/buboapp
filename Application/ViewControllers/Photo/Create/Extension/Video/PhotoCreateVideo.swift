//
//  PhotoCreateVideo.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 7/29/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Photos

typealias PhotoCreateVideo = PhotoCreateViewController

extension PhotoCreateVideo  {
    
    func setupVideo() {
        photoImageView?.isHidden = true
        playerController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
        playerController.showsPlaybackControls = false
        guard let bounds = videoView?.bounds else { return }
        playerController.view.frame = bounds
        videoView?.addSubview(playerController.view)
        playVideo(playerController)
    }
    
    func prepareVideoThumbnail() -> UIImage? {
        guard let videoURL = videoURL else { return nil }
        do {
            let asset = AVURLAsset(url: videoURL , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
    func playVideo(_ playerController: AVPlayerViewController) {
        guard let url = videoURL else { return }
        let player = AVPlayer(url: url)
        player.rate = 0.4
        playerController.player = player
        playerController.player?.play()
    }
    
}
