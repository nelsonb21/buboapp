//
//  PhotoCreateAudioRecorder.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 7/29/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import AVFoundation

typealias PhotoCreateAudioRecorder = PhotoCreateViewController

extension PhotoCreateAudioRecorder: AVAudioRecorderDelegate {

    func setupAudioRecording() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession?.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession?.setActive(true)
            
            recordingSession?.requestRecordPermission({ (allowed) in
                if allowed {
                    //TODO: - Show mic button to record
                    self.recordButton?.isHidden = false
                } else {
                    //TODO: - Show message that app needs mic permission
                    // failed to record!
                    self.recordButton?.isHidden = true
                }
            })
        } catch {
            print("Error Recording Session: \(error.localizedDescription)")
        }
    }
    
    func recordTapped() {
        if audioRecorder == nil {
            startRecording()
            updateProgressView()
        } else {
            finishRecording(success: true)
            stopTimer()
        }
    }
    
    func deleteRecordedAudio() {
        resetAudioValues()
    }
    
    func playVoiceNote() {
        guard let url = audioFilename else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            let data = try Data(contentsOf: url)
            player = try AVAudioPlayer(data: data, fileTypeHint: AVFileType.m4a.rawValue)
            guard let player = player else { return }
            player.play()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    //MARK: - Private Methods
    
    private func startRecording() {
        recordSoundLabel.isHidden = true
        audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            guard let audioFilename = self.audioFilename else { return }
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
            try session.setActive(true)
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()
            
            recordButton?.setImage(#imageLiteral(resourceName: "icon_stop_record"), for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    private func finishRecording(success: Bool) {
        audioRecorder?.stop()
        audioRecorder = nil
    }
    
    //MARK: - Timer
    
    private func updateProgressView() {
        recordedProgressView?.trackTintColor = .lightGray
        if !timerIsOn {
            timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
            timerIsOn = true
        }
    }
    
    @objc private func timerRunning() {
        audioTimeCounter += 0.0001
        let completionPercentage = (audioTimeCounter * 100) / audioLimitTime
        recordedProgressView?.setProgress(Float(completionPercentage / 100), animated: true)
        if audioTimeCounter >= audioLimitTime {
            finishRecording(success: true)
            stopTimer()
        }
    }
    
    private func stopTimer() {
        timer.invalidate()
        timerIsOn = false
    }
    
    private func resetAudioValues() {
        try? AVAudioSession.sharedInstance().setActive(false)
        recordedProgressView?.setProgress(0, animated: false)
        recordedProgressView?.trackTintColor = .clear
        audioTimeCounter = 0.0
        timerIsOn = false
        isAudioPlayable = false
        recordButton?.setImage(#imageLiteral(resourceName: "icon_mic"), for: .normal)
        deleteAudioButton?.isHidden = true
        recordSoundLabel.isHidden = false
    }
    
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            recordButton?.setImage(#imageLiteral(resourceName: "icon_play"), for: .normal)
            deleteAudioButton?.isHidden = false
            isAudioPlayable = true
            guard let url = audioFilename else { return }
            do {
                let data = try Data(contentsOf: url)
                voiceNoteData = data
            } catch {
                print("Error converting note to Data: \(error.localizedDescription)")
            }
        } else {
            resetAudioValues()
        }
    }
}
