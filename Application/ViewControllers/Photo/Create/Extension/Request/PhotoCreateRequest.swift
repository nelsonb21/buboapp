//
//  PhotoCreateRequest.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 7/29/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias PhotoCreateRequest = PhotoCreateViewController

extension PhotoCreateRequest {

    func uploadImage(with photo: UIImage, videoURL: String?) {
        guard let imageData = UIImageJPEGRepresentation(photo, 0.6) else { return }
        guard let name = createUniquePhotoName() else { return }
        
        loadingImage?.startLoadingAnimation()
        PhotoController.uploadPhoto(imageData, name: name, completion: { response, error in
            if error == nil {
                guard let fileURL = response as? String else { return }
                if let _ = self.voiceNoteData {
                    self.uploadVoiceNote(fileURL, videoURL: videoURL)
                } else {
                    self.savePhotoWithUser(fileURL, videoURL: videoURL)
                }
            } else {
                self.loadingImage?.stopLoadingAnimation()
                self.showErrorAlert("Somethings when wrong! Please try again later")
            }
        })
    }
    
    func uploadVideo() {
        guard let videoURL = videoURL else { return }
        guard let videoData = try? Data(contentsOf: videoURL) else { return }
        guard let name = createUniquePhotoName() else { return }
        
        loadingImage?.startLoadingAnimation()
        PhotoController.uploadVideo(videoData, name: name) { response, error in
            if error == nil {
                guard let videoURL = response as? String else { return }
                let photo = self.prepareVideoThumbnail()
                if photo != nil {
                    self.uploadImage(with: photo!, videoURL: videoURL)
                }
            } else {
                self.loadingImage?.stopLoadingAnimation()
                self.showErrorAlert("Somethings when wrong! Please try again later")
            }
        }
    }
    
    func uploadVoiceNote(_ fileURL: String, videoURL: String?) {
        guard let voiceNoteData = voiceNoteData else { return }
        guard let name = createUniquePhotoName() else { return }
        
        PhotoController.uploadVoiceNote(voiceNoteData, name: name) { (response, error) in
            if error == nil {
                guard let voiceNoteURL = response as? String else { return }
                Analytics.logEvent(with: .photoCreateHasAudio)
                self.savePhotoWithUser(fileURL, videoURL: videoURL, voiceNoteURL: voiceNoteURL)
            } else {
                self.loadingImage?.stopLoadingAnimation()
            }
        }
    }
    
    func savePhotoWithUser(_ fileURL: String, videoURL: String?, voiceNoteURL: String = "") {
        let photo = preparePhoto(fileURL, videoURL: videoURL, voiceNoteURL: voiceNoteURL)
        
        PhotoController.savePhotoWithUser(photo, completion: { response, error in
            self.loadingImage?.stopLoadingAnimation()
            if response != nil {
                Analytics.logEvent(with: .photoCreatedPosted)
                self.closeToRoot()
            } else {
                self.showErrorAlert("Somethings when wrong! Please try again later")
            }
        })
    }
    
}
