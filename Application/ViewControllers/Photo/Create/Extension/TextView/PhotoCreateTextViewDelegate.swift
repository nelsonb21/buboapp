//
//  PhotoCreateTextViewDelegate.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/5/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias PhotoCreateTextViewDelegate = PhotoCreateViewController

extension PhotoCreateTextViewDelegate: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholderText
            textView.textColor = UIColor.lightGray
        }
    }
    
}
