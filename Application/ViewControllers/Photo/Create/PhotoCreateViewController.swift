//
//  PhotoViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/13/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PhotoCreateViewController: AppViewController {
    
    @IBOutlet weak var photoImageView: UIImageView?
    @IBOutlet weak var photoTextView: UITextView?
    @IBOutlet weak var photoAddressLabel: UILabel?
    @IBOutlet weak var videoView: UIView?
    @IBOutlet weak var recordButton: UIButton?
    @IBOutlet weak var recordedProgressView: UIProgressView?
    @IBOutlet weak var deleteAudioButton: UIButton?
    @IBOutlet weak var recordSoundLabel: UILabel!
    
    var photo: UIImage?
    var location: CLLocation?
    var address = ""
    var city = ""
    var country = ""
    
    let username = KeychainWrapper.getString(key: .CustomerTokenData)
    var newPhotoSelected: Bool?
    
    var locationManager: CLLocationManager?
    var currentLocation: CLLocationCoordinate2D?
    
    var placeholderText = "What's going on here?"
    var addressIsHidden = false
    
    let playerController = AVPlayerViewController()
    var videoURL: URL?
    var assetIsVideo = false
    
    var uploadMessage: String?
    
    //Audio vars
    var recordingSession: AVAudioSession?
    var audioRecorder: AVAudioRecorder?
    var audioTimeCounter =  0.0
    var audioLimitTime = 15.0
    var timer = Timer()
    var timerIsOn = false
    var isAudioPlayable = false
    var audioFilename: URL?
    var player: AVAudioPlayer?
    var voiceNoteData: Data?

    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addLoadingAnimation(toView: view)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadingImage?.removeFromSuperview()
    }

    //MARK: - Setup
    
    func setupView() {
        Analytics.logEvent(with: .photoCreateOpened)
        if assetIsVideo {
            recordButton?.isHidden = true
            uploadMessage = "Video Uploaded"
            setupVideo()
        } else {
            uploadMessage = "Photo Uploaded"
            photoImageView?.image = photo
        }
        
        photoTextView?.text = placeholderText
        photoTextView?.textColor = .lightGray
        photoTextView?.delegate = self
        
        setupAddress(with: location)
    }
    
    private func setupAddress(with location: CLLocation?) {
        guard let location = location else {
            self.photoAddressLabel?.text = "  Address not available"
            return
        }
        
        PlacemarkController.getReverseGeocodeLocation(with: location) { (placemark, _) in
            guard let placemark = placemark as? CLPlacemark else {
                self.photoAddressLabel?.text = "  Address not available"
                return
            }
            self.address = PlacemarkController.getAddress(of: placemark)
            self.city = PlacemarkController.getCity(of: placemark)
            self.country = PlacemarkController.getCountry(of: placemark)
            self.photoAddressLabel?.text = self.address
        }
    }

    //MARK: - IBActions
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        close()
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        if assetIsVideo {
            Analytics.logEvent(with: .photoCreateIsVideo)
            uploadVideo()
        } else {
            Analytics.logEvent(with: .photoCreateIsImage)
            uploadImage(with: photo!, videoURL: nil)
        }
    }
    
    @IBAction func hideAddressTapped(_ sender: UIButton) {
        Analytics.logEvent(with: .photoCreateHideAddress)
        hideAddress()
    }
    
    @IBAction func recordAudioTapped(_ sender: UIButton) {
        if isAudioPlayable {
            playVoiceNote()
        } else {
            recordTapped()
        }
    }
    
    @IBAction func deleteRecordedAudioTapped(_ sender: UIButton) {
        deleteRecordedAudio()
    }
    
    //MARK: - Methods
    
    func createUniquePhotoName() -> String? {
        let photoCaption = photoTextView?.text ?? ""
        let photoTitle = photoCaption != placeholderText ? photoCaption.data(using: String.Encoding.utf8)?.base64EncodedString() : ""
        let photoDateString = Date().description
        let photoNameString = username! + (photoTitle ?? "") + photoDateString
        let photoName = photoNameString.data(using: String.Encoding.utf8)
        let photoNameEncoded = photoName?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        return (photoNameEncoded?.replacingOccurrences(of: "=", with: ""))
    }
    
    func preparePhoto(_ fileURL: String, videoURL: String?, voiceNoteURL: String) -> Photo {
        let photo = Photo()
        if let title = photoTextView?.text, title != placeholderText {
            photo.title = title
        } else {
            photo.title = ""
        }
        photo.username = username
        photo.user = nil
        photo.imageUrl = assetIsVideo ? videoURL : fileURL
        photo.datetime = Date()
        photo.isVideo = assetIsVideo ? AssetType.isVideo.rawValue : AssetType.isImage.rawValue
        photo.address = addressIsHidden ? "Private address" : address
        photo.city = city
        photo.country = country
        photo.videoThumbnail = assetIsVideo ? fileURL : ""
        photo.voiceNoteUrl = voiceNoteURL
        photo.location =  GeoPoint.geoPoint(
            GEO_POINT(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!),
            categories: ["Bubo"],
            metadata: [:]
            ) as? GeoPoint
        return photo
    }
    
    override func close() {
        guard let homeNav = presentingViewController as? UINavigationController else { return }
        guard let homeAR = homeNav.viewControllers.first as? HomeAugmentedRealityViewController else { return }
        dismiss(animated: true, completion: {
            homeAR.startCameraAndTracking(notifyLocationFailure: true)
        })
    }
    
    func closeToRoot() {
        guard let homeNav = presentingViewController as? UINavigationController else { return }
        guard let homeAR = homeNav.viewControllers.first as? HomeAugmentedRealityViewController else { return }
        view.window!.rootViewController?.dismiss(animated: true, completion: {
            homeAR.startCameraAndTracking(notifyLocationFailure: true)
            homeAR.getCurrentLocation()
        })
    }
    
    private func hideAddress() {
        addressIsHidden = !addressIsHidden
        photoAddressLabel?.text = addressIsHidden ? "Private address" : address
    }
    
}
