//
//  PhotoDetailImageViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/11/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PhotoDetailImageViewController: UIViewController, UIScrollViewDelegate {

    var photo: Photo?
    var index: Int?
    weak var delegate: PhotoDetailPageDelegate?
    let playerController = AVPlayerViewController()
    @IBOutlet weak var photoDetailImageView: UIImageView?
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var videoView: UIView?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self)
        scrollView?.delegate = self
        scrollView?.minimumZoomScale = 1.0
        scrollView?.maximumZoomScale = 6.0
        
        if photo?.isVideo != nil, let isVideo = photo?.isVideo, isVideo == AssetType.isVideo.rawValue {
            setupVideo()
        } else if let imageUrlString = photo?.imageUrl, let imageUrl = URL(string: imageUrlString) {
            photoDetailImageView?.setImage(with: imageUrl)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        playerController.player?.pause()
    }
    
    //MARK: - Methods
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photoDetailImageView
    }

    private func setupVideo() {
        photoDetailImageView?.isHidden = true
        playerController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
        playerController.showsPlaybackControls = false
        guard let bounds = videoView?.bounds else {
            return
        }
        
        playerController.view.frame = bounds
        videoView?.addSubview(playerController.view)
        videoView?.isHidden = false
        playVideo(playerController)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PhotoDetailImageViewController.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerController.player?.currentItem)
    }
    
    private func playVideo(_ playerController: AVPlayerViewController) {
        guard let urlString = photo?.imageUrl, let url = URL(string: urlString) else {
            return
        }
        
        let player = AVPlayer(url: url)
        player.rate = 0.4
        playerController.player = player
        playerController.player?.play()
    }
    
    @objc private func playerItemDidReachEnd() {
        playerController.player?.seek(to: kCMTimeZero)
        playerController.player?.play()
    }
    
}
