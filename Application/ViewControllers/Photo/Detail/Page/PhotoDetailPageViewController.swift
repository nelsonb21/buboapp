//
//  PhotoDetailPageViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/11/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

protocol PhotoDetailPageDelegate: class {
    func changeInformation(with index: Int)
}

class PhotoDetailPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource  {
    
    var photos: [Photo]?
    var likedPhotos: [LikePhoto]?
    var index: Int = 0
    var option: ProfileOption?
    weak var photoDetailDelegate: PhotoDetailPageDelegate?

    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //MARK: - Methods
    
    func setupView()  {
        self.delegate = self
        self.dataSource = self
        
        guard let photo = option == .photosLiked ? likedPhotos?[index].photo : photos?[index] else { return }
        setViewControllers([setupViewController(with: photo, index: index)], direction: .forward, animated: true, completion: nil)
    }
    
    private func setupViewController(with photo: Photo, index: Int) -> UIViewController {
        guard let photoDetailImageViewController = UIStoryboard.viewController(withIdentifier: "PhotoDetailImageViewController", storyBoardIdentifier: "PhotoDetailPageViewController") as? PhotoDetailImageViewController else { return UIViewController() }
        photoDetailImageViewController.photo = photo
        photoDetailImageViewController.index = index
        photoDetailImageViewController.delegate = photoDetailDelegate
        return photoDetailImageViewController
    }
    
    //MARK: - UIPageViewControllerDataSource
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let photoDetailImageViewController = pageViewController.viewControllers?.first as? PhotoDetailImageViewController else { return nil }
        guard var previousIndex = photoDetailImageViewController.index else { return nil }
        
        if ((previousIndex == 0) || (previousIndex == NSNotFound)) { return nil }
        
        previousIndex -= 1
        guard let photo = option == .photosLiked ? likedPhotos?[previousIndex].photo : photos?[previousIndex] else { return nil }
        return setupViewController(with: photo, index: previousIndex)
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let photoDetailImageViewController = pageViewController.viewControllers?.first as? PhotoDetailImageViewController else { return nil }
        guard var nextIndex = photoDetailImageViewController.index else { return nil }
        
        if (nextIndex == NSNotFound) { return nil }
        nextIndex += 1
        if (nextIndex == (option == .photosLiked ? likedPhotos?.count : photos?.count)) { return nil }
        
        guard let photo = option == .photosLiked ? likedPhotos?[nextIndex].photo : photos?[nextIndex] else { return nil }
        return setupViewController(with: photo, index: nextIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let photoDetailImageViewController = pageViewController.viewControllers?.first as? PhotoDetailImageViewController else { return }
        photoDetailDelegate?.changeInformation(with: photoDetailImageViewController.index!)
    }

}
