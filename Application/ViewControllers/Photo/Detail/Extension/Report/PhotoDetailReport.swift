//
//  PhotoDetailReport.swift
//  Bubo
//
//  Created by Nelson Bolivar on 1/4/18.
//  Copyright © 2018 Nelson Bolivar. All rights reserved.
//

import Foundation

extension PhotoDetailViewController {
    
    func setupReportedPost(_ photo: Photo) {
        reportedPostView?.isHidden = !isPostReported(photo)
    }
    
    private func isPostReported(_ photo: Photo) -> Bool {
        let isReported = ReportPostController.reported.contains { (reported) -> Bool in
            reported.photo?.objectId == photo.objectId
        }
        return isReported
    }
    
}
