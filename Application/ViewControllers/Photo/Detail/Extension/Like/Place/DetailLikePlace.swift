//
//  DetailLikePlace.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 8/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension PhotoDetailViewController {
    
    //MARK: - Place Methods
    
    func setupBookmarkPlaceButton(_ photo: Photo) {
        let isBookmarked = PlaceController.likePlaces.contains { (place) -> Bool in
            photo.location?.longitude == place.coordinates?.longitude && photo.location?.latitude == place.coordinates?.latitude
        }
        let image = isBookmarked ? #imageLiteral(resourceName: "btn_bookmark_place_fill") : #imageLiteral(resourceName: "btn_bookmark_place_empty")
        bookmarkPlaceImageView?.image = photo.address == "Private address" ? #imageLiteral(resourceName: "btn_bookmark_place_disable") : image
        bookmarkPlaceButton?.isEnabled = photo.address != "Private address"
        buboPhotoAddressButton?.isEnabled = photo.address != "Private address"
    }
    
    func bookmarkPlace(_ photo: Photo) {
        guard let geoPoint = photo.location else { return }
        let location = CLLocation(latitude: CLLocationDegrees(geoPoint.latitude), longitude: CLLocationDegrees(geoPoint.longitude))
        bookmarkPlaceImageView?.image = #imageLiteral(resourceName: "btn_bookmark_place_fill")
        
        PlacemarkController.getReverseGeocodeLocation(with: location) { (placemark, _) in
            guard let placemark = placemark as? CLPlacemark else { return }
            let place = Place()
            if let city = placemark.locality { place.city = city }
            if let country = placemark.country { place.country = country }
            if let street = placemark.thoroughfare { place.street = street }
            if let name = placemark.name { place.name = name }
            place.coordinates = GeoPoint().getGeopoint(with: location.coordinate)
            place.imageUrl = photo.imageUrl
            place.user = nil
            
            PlaceController.bookmarkPlace(place: place) { (result, error) in
                self.bookmarkPlaceImageView?.image = error == nil ? #imageLiteral(resourceName: "btn_bookmark_place_fill") : #imageLiteral(resourceName: "btn_bookmark_place_empty")
                guard let place = result as? Place else { return }
                Analytics.logEvent(with: .photoDetailLikePlace)
                PlaceController.likePlaces.append(place)
            }
        }
    }
    
    func deleteBookmarkedPlace(_ photo: Photo) {
        let index = PlaceController.likePlaces.index { (place) -> Bool in
            photo.location?.longitude == place.coordinates?.longitude && photo.location?.latitude == place.coordinates?.latitude
        }
        
        if index != nil {
            self.bookmarkPlaceImageView?.image = #imageLiteral(resourceName: "btn_bookmark_place_empty")
            let place = PlaceController.likePlaces[index!]
            PlaceController.deletePlace(with: place) { (result, error) in
                if error == nil {
                    PlaceController.likePlaces.remove(at: index!)
                }
                Analytics.logEvent(with: .photoDetailDislikePlace)
                self.bookmarkPlaceImageView?.image = error == nil ? #imageLiteral(resourceName: "btn_bookmark_place_empty") : #imageLiteral(resourceName: "btn_bookmark_place_fill")
            }
        }
    }
    
}
