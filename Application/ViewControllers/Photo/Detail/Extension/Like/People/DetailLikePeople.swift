//
//  DetailLikePeople.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 5/2/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension PhotoDetailViewController {
    
    //MARK: - Like People Methods
    
    func setupLikePeopleButton(_ photo: Photo) {
        likePeopleIconImage?.isHidden = photo.user?.objectId == User.current.objectId
        
        let isLiked = LikePeopleController.likePeople.contains { (likesPeople) -> Bool in
            likesPeople.userLiked?.objectId == photo.user?.objectId
        }
        likePeopleIconImage?.image = isLiked ? #imageLiteral(resourceName: "btn_like_people_fill") : #imageLiteral(resourceName: "btn_like_people_empty")
    }
    
    func likePeople() {
        likePeopleIconImage?.image = #imageLiteral(resourceName: "btn_like_people_fill")
        let like = LikePeople()
        like.userLiked = photo?.user
        like.datetime = Date()
        
        LikePeopleController.likePeople(like: like) { (result, error) in
            self.likePeopleIconImage?.image = error == nil ? #imageLiteral(resourceName: "btn_like_people_fill") : #imageLiteral(resourceName: "btn_like_people_empty")
        }
    }
    
    func dislikePeople() {
        likePeopleIconImage?.image = #imageLiteral(resourceName: "btn_like_people_empty")
        
        let likePeople = LikePeopleController.likePeople.first { (likePeople) -> Bool in
            likePeople.userLiked?.objectId == self.photo!.user?.objectId
        }
        
        if likePeople != nil {
            LikePeopleController.deleteLike(with: likePeople!) { (result, error) in
                self.likePeopleIconImage?.image = error == nil ? #imageLiteral(resourceName: "btn_like_people_empty") : #imageLiteral(resourceName: "btn_like_people_fill")
            }
        }
    }
    
    func openOtherUserProfile(user: BackendlessUser) {
        guard let profileViewController = UIStoryboard.viewController(withIdentifier: "ProfileViewController") as? ProfileViewController else { return }
        profileViewController.isOtherUserProfile = user.objectId != User.current.objectId
        profileViewController.user = user
        navigationController?.pushViewController(profileViewController, animated: true)
    }
    
}
