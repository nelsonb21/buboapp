//
//  DetailLikePhoto.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 5/2/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension PhotoDetailViewController {

    //MARK: - Like Photo Methods
    
    func setupLikeButton(_ photo: Photo) {
        let isLiked = LikePhotoController.likePhoto.contains { (likesPhoto) -> Bool in
            likesPhoto.photo?.objectId == photo.objectId
        }
        likePhotoIconImage?.image = isLiked ? #imageLiteral(resourceName: "btn_like_photo_fill") : #imageLiteral(resourceName: "btn_like_photo_empty")
    }
    
    func likePhoto() {
        likePhotoIconImage?.image = #imageLiteral(resourceName: "btn_like_photo_fill")
        
        let like = LikePhoto()
        like.user = nil
        like.photo = photo
        like.datetime = Date()
        
        LikePhotoController.likePhoto(like: like) { (result, error) in
            self.likePhotoIconImage?.image = error == nil ? #imageLiteral(resourceName: "btn_like_photo_fill") : #imageLiteral(resourceName: "btn_like_photo_empty")
            Analytics.logEvent(with: .photoDetailLikePhoto)
        }
    }
    
    func dislikePhoto() {
        likePhotoIconImage?.image = #imageLiteral(resourceName: "btn_like_photo_empty")
        
        let likePhoto = LikePhotoController.likePhoto.first { (likePhoto) -> Bool in
            likePhoto.photo?.objectId == self.photo!.objectId
        }
        
        if likePhoto != nil {
            LikePhotoController.deleteLike(with: likePhoto!) { (result, error) in
                self.likePhotoIconImage?.image = error == nil ? #imageLiteral(resourceName: "btn_like_photo_empty") : #imageLiteral(resourceName: "btn_like_photo_fill")
                Analytics.logEvent(with: .photoDetailDislikePhoto)
            }
        }
    }
    
}
