//
//  PhotoDetailViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/14/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import AVFoundation
import DateHelper

protocol PhotoDetailDelegate: class {
    func deletePhoto(_ photo: Photo)
}

class PhotoDetailViewController: AppViewController, PhotoDetailPageDelegate, UITextViewDelegate {

    @IBOutlet weak var photoTextView: UITextView?
    @IBOutlet weak var footerView: UIView?
    @IBOutlet weak var likePhotoIconImage: UIImageView?
    @IBOutlet weak var userImage: UIImageView?
    @IBOutlet weak var userNameLabel: UILabel?
    @IBOutlet weak var likePeopleIconImage: UIImageView?
    @IBOutlet weak var photoAddressButton: UIButton?
    @IBOutlet weak var postDateLabel: UILabel?
    @IBOutlet weak var cityLabel: UILabel?
    
    @IBOutlet weak var imagePageView: UIView?
    @IBOutlet weak var backgroundImageView: UIImageView?
    
    @IBOutlet weak var voiceNoteButton: UIButton?
    @IBOutlet weak var voiceNoteIconImageView: UIImageView?
    
    @IBOutlet weak var showOptionsButton: UIButton?
    @IBOutlet weak var bookmarkPlaceImageView: UIImageView?
    @IBOutlet weak var bookmarkPlaceButton: UIButton?
    @IBOutlet weak var buboPhotoAddressButton: UIButton?
    @IBOutlet weak var reportedPostView: UIView?
    
    @IBOutlet weak var howToGetHereLabel: UILabel?
    @IBOutlet weak var googleMapsButton: UIButton!
    @IBOutlet weak var wazeButton: UIButton!
    
    var photo: Photo?
    var photos: [Photo] = []
    var likedPhotos: [LikePhoto] = []
    var index: Int = 0
    var option: ProfileOption?
    var shouldDisablePageScroll = false
    var location: CLLocation?
    var player: AVAudioPlayer?
    var isOpenFromCollection = false
    weak var delegate: PhotoDetailDelegate?
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addLoadingAnimation(toView: view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.tintColor = .white
    }
    
    func setupView() {
        Analytics.logEvent(with: .photoDetailOpened)
        photoTextView?.delegate = self
        setupBarButtonStyles()
        guard let photo = photo else {
            return
        }
        
        setupPageViewController(with: index)
        setupImage(photo)
        
        if navigationController?.restorationIdentifier == "PhotoDetailNavController" {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "btn_close"), target: self, action: #selector(closeButtonTapped))
        }
    }
    
    private func setupBarButtonStyles() {
        navigationBarBackgroundImageView?.backgroundColor = .clear
    }
    
    //MARK: - Setup
    
    private func setupImage(_ photo: Photo) {
        self.photo = photo
        photoTextView?.text = photo.title ?? ""
        postDateLabel?.text = photo.datetime?.toString(dateStyle: .medium, timeStyle: .short)
        
        if photo.isVideo != nil, let isVideo = photo.isVideo, isVideo == AssetType.isVideo.rawValue, let videoThumbnail = photo.videoThumbnail  {
            setupBackgroundImage(with: videoThumbnail)
        } else if let imageUrl = photo.imageUrl {
            setupBackgroundImage(with: imageUrl)
        }
        
        setupLocation(photo)
        setupAddress(photo)
        setupUserInfo(photo)
        setupLikeButton(photo)
        setupReportedPost(photo)
        setupBookmarkPlaceButton(photo)
        setupInstructions()
    }
    
    private func setupUserInfo(_ photo: Photo) {
        guard let user = photo.user else {
            return
        }
        
        if let urlString = user.getValue(key: .profileImageUrl) as? String, let url = URL(string: urlString) {
            userImage?.setImage(with: url, placeholder: #imageLiteral(resourceName: "icon_user_placeholder"))
        } else {
            userImage?.image = #imageLiteral(resourceName: "icon_user_placeholder")
        }
        
        player?.stop()
        let existVoiceNote = photo.voiceNoteUrl == "" || photo.voiceNoteUrl == nil
        voiceNoteButton?.isHidden = existVoiceNote
        voiceNoteIconImageView?.isHidden = existVoiceNote
        
        userNameLabel?.text = user.name.capitalized
        setupLikePeopleButton(photo)
    }
    
    private func setupPageViewController(with index: Int) {
        guard let pageViewController = UIStoryboard.viewController(withIdentifier: "PhotoDetailPageViewController") as? PhotoDetailPageViewController, let bounds = imagePageView?.bounds else {
            return
        }
        
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        pageViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pageViewController.option = option
        pageViewController.index = index
        if option == .photosLiked {
            pageViewController.likedPhotos = likedPhotos
        } else {
            pageViewController.photos = photos
        }
        
        pageViewController.photoDetailDelegate = self
        pageViewController.setupView()
        addChildViewController(pageViewController)
        if shouldDisablePageScroll {
            pageViewController.delegate = nil
            pageViewController.dataSource = nil
        }
        
        imagePageView?.addSubview(pageViewController.view)
    }
    
    private func setupLocation(_ photo: Photo) {
        guard let latitude = photo.location?.latitude as? CLLocationDegrees, let longitude = photo.location?.longitude as? CLLocationDegrees else {
            return
        }
        
        location = CLLocation(latitude: latitude, longitude: longitude)
    }
    
    private func setupAddress(_ photo: Photo) {
        if photo.address == "Private address" {
            photoAddressButton?.setTitle("Private address", for: .normal)
            photoAddressButton?.isEnabled = false
        } else {
            photoAddressButton?.isEnabled = true
            if photo.address == "Location name not available" {
                photoAddressButton?.setTitle("Take a look at other photos at this location", for: .normal)
            } else {
                photo.address == nil ? setupAddress(with: location) : photoAddressButton?.setTitle(photo.address, for: .normal)
            }
        }
        
        var cityCountry = ""
        if let city = photo.city {
            cityCountry = city
            if let country = photo.country {
                cityCountry = cityCountry + ", " + country
            }
        } else {
            if let country = photo.country {
                cityCountry = country
            }
        }
        
        cityLabel?.text = cityCountry
    }
    
    private func setupAddress(with location: CLLocation?) {
        guard let location = location else {
            return
        }
        
        PlacemarkController.getReverseGeocodeLocation(with: location) { (placemark, _) in
            guard let placemark = placemark as? CLPlacemark else {
                return
            }
            
            let address = PlacemarkController.getAddress(of: placemark)
            self.photoAddressButton?.setTitle(address, for: .normal)
        }
    }
    
    private func setupBackgroundImage(with stringURL: String) {
        backgroundImageView?.setImageKF(with: stringURL)
    }
    
    private func setupInstructions() {
        let isLocationAvailable = location != nil
        let isAddressPrivate = photo?.address == "Private address"
        if let googleMapsURL = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(googleMapsURL) {
            googleMapsButton?.isHidden = isLocationAvailable && isAddressPrivate
        }
        
        if let wazeURL = URL(string: "waze://"), UIApplication.shared.canOpenURL(wazeURL) {
            wazeButton?.isHidden = isLocationAvailable && isAddressPrivate
        }
        
        howToGetHereLabel?.isHidden = googleMapsButton.isHidden && wazeButton.isHidden
    }
    
    //MARK: - IBActions 
    
    @IBAction func closeButtonTapped(_ sender: UIBarButtonItem) {
        player?.stop()
        dismiss(animated: true, completion: nil)
    }

    @IBAction func likeButtonAction(_ sender: UIButton) {
        likePhotoIconImage?.image == #imageLiteral(resourceName: "btn_like_photo_fill") ? dislikePhoto() : likePhoto()
    }
    
    @IBAction func likePeopleButtonAction(_ sender: UIButton) {
        guard let user = photo?.user else {
            return
        }
        
        Analytics.logEvent(with: .photoDetailProfileOpened)
        openOtherUserProfile(user: user)
    }
    
    @IBAction func addressButtonAction(_ sender: UIButton) {
        Analytics.logEvent(with: .photoDetailAddressPress)
        openPhotoCollection(with: location?.coordinate)
    }
    
    @IBAction func voiceNoteAction(_ sender: UIButton) {
        Analytics.logEvent(with: .photoDetailAudioPlayed)
        playVoiceNote()
    }
    
    @IBAction func showOptionsAction(_ sender: UIButton) {
        showOptions()
    }
    
    @IBAction func bookmarkPlaceAction(_ sender: UIButton) {
        guard let photo = photo else {
            return
        }
        
        bookmarkPlaceImageView?.image == #imageLiteral(resourceName: "btn_bookmark_place_fill") ? deleteBookmarkedPlace(photo) : bookmarkPlace(photo)
    }
    
    @IBAction func openGoogleMaps(_ sender: UIButton) {
        openGoogleMaps()
    }
    
    @IBAction func openWaze(_ sender: UIButton) {
        openWaze()
    }
    
    //MARK: - Methods
    
    private func reloadData(_ newIndex: Int) {
        if option == .photosLiked && (newIndex >= 0 && newIndex < likedPhotos.count) {
            index = newIndex
            guard let photo = likedPhotos[newIndex].photo else {
                return
            }
            
            setupImage(photo)
        } else if newIndex >= 0 && newIndex < photos.count {
            index = newIndex
            let photo = photos[newIndex]
            setupImage(photo)
        }
    }
    
    private func openPhotoCollection(with coordinates: CLLocationCoordinate2D?) {
        guard let coordinates = coordinates, let photoCollectionViewController = UIStoryboard.viewController(withIdentifier: "PhotoCollectionViewController") as? PhotoCollectionViewController else {
            return
        }
        
        photoCollectionViewController.coordinates = coordinates
        navigationController?.pushViewController(photoCollectionViewController, animated: true)
    }
    
    private func playVoiceNote() {
        guard let stringURL = photo?.voiceNoteUrl, let url = URL(string: stringURL) else {
            return
        }
        
        do {
            let data = try Data(contentsOf: url)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(data: data, fileTypeHint: AVFileType.m4a.rawValue)
            player?.prepareToPlay()
            guard let player = player else {
                return
            }
            
            player.play()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func showOptions() {
        guard let photo = photo else {
            return
        }
        
        let alertView = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if photo.user?.objectId == User.current.objectId {
            let editAction = UIAlertAction(title: "Edit", style: .default) { _ in
                self.editCaption()
            }
            
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
                self.deletePhoto()
            }
            
            alertView.addAction(editAction)
            alertView.addAction(deleteAction)
        } else {
            let reportAction = UIAlertAction(title: "Report Post", style: .default) { _ in
                self.reportPost()
            }
            
            alertView.addAction(reportAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertView.addAction(cancelAction)
        
        if let alertView = alertView.popoverPresentationController {
            alertView.sourceView = showOptionsButton
            alertView.sourceRect = showOptionsButton?.bounds ?? CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0)
        }
        
        present(alertView, animated: true, completion: nil)
    }
    
    private func editCaption() {
        Analytics.logEvent(with: .photoDetailEditOpened)
        photoTextView?.isEditable = true
        photoTextView?.becomeFirstResponder()
    }
    
    private func deletePhoto() {
        guard let photo = photo else {
            return
        }
        
        loadingImage?.startLoadingAnimation()
        PhotoController.deletePhoto(with: photo) { (result, error) in
            self.loadingImage?.stopLoadingAnimation()
            if error == nil {
                Analytics.logEvent(with: .photoDetailDeleteCompleted)
                self.delegate?.deletePhoto(photo)
                if self.isOpenFromCollection {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.close()
                }
            }
        }
    }
    
    private func updatePhoto() {
        photoTextView?.isEditable = false
        photoTextView?.isSelectable = false
        if let title = photoTextView?.text, let photoTitle = photo?.title, photoTitle != title  {
            photo?.title = title
            guard let photo = photo else {
                return
            }
            
            Analytics.logEvent(with: .photoDetailEditCompleted)
            PhotoController.savePhotoWithUser(photo) { (_, _) in }
        }
    }
    
    private func reportPost() {
        Analytics.logEvent(with: .photoDetailReportPost)
        let reportPost = ReportPost(with: photo, user: User.current)
        ReportPostController.reportPost(post: reportPost) { (result, error) in
            if error == nil {
                guard let photo = self.photo else {
                    return
                }
                
                self.setupReportedPost(photo)
            }
        }
    }
    
    private func openGoogleMaps() {
        guard let latitude = photo?.location?.latitude as? CLLocationDegrees, let longitude = photo?.location?.longitude as? CLLocationDegrees, let url = URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving") else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func openWaze() {
        guard let latitude = photo?.location?.latitude as? CLLocationDegrees, let longitude = photo?.location?.longitude as? CLLocationDegrees, let url = URL(string: "waze://?ll=\(latitude),\(longitude)&navigate=yes") else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    //MARK: - PhotoDetailPageDelegate
    
    func changeInformation(with index: Int) {
        reloadData(index)
    }
    
    //MARK: - UITextViewDelegate
    
    func textViewDidEndEditing(_ textView: UITextView) {
        updatePhoto()
    }
    
}
