//
//  OnboardingText.swift
//  Bubo
//
//  Created by Nelson Bolivar on 10/6/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import BonMot

enum OnboardingText {
    case onboardingOne
    case onboardingTwo
    case onboardingThree
    
    var message: NSAttributedString {
        switch self {
        case .onboardingOne:
            let strings = ["Let the power of".styled(with: MessageStyles.black),
                           " augmented reality".styled(with: MessageStyles.purple),
                           " lead the way.".styled(with: MessageStyles.black)]
            return NSAttributedString().appendAttributedString(strings)
        case .onboardingTwo:
            let strings = ["Stop wandering and".styled(with: MessageStyles.black),
                           " start living it.".styled(with: MessageStyles.purple)]
            return NSAttributedString().appendAttributedString(strings)
        case .onboardingThree:
            let strings = ["Share your experiences and become a".styled(with: MessageStyles.black),
                           " citizen of the world.".styled(with: MessageStyles.purple)]
            return NSAttributedString().appendAttributedString(strings)
        }
    }
    
}
