//
//  OnBoardingImageViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class OnBoardingImageViewController: UIViewController {
    
    var onBoardingImage: UIImage?
    var onBoardingTitle: String = ""
    var onBoardingDescription: NSAttributedString = NSAttributedString(string: "")
    var index: Int?

    @IBOutlet weak var onboardingImageView: UIImageView?
    @IBOutlet weak var onboardingDescriptionLabel: UILabel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onboardingImageView?.image = onBoardingImage
        onboardingDescriptionLabel?.adjustsFontSizeToFitWidth = true
        onboardingDescriptionLabel?.attributedText = onBoardingDescription
        onboardingDescriptionLabel?.lineBreakMode = .byTruncatingTail
    }

}
