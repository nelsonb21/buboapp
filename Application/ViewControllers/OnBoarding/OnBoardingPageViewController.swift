//
//  OnBoardingPageViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

class OnBoardingPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    let images: [UIImage] = [#imageLiteral(resourceName: "on_image1"), #imageLiteral(resourceName: "on_image3"), #imageLiteral(resourceName: "on_image2")]
    let titles = ["", "", ""]
    let descriptions: [OnboardingText] = [.onboardingOne, .onboardingTwo, .onboardingThree]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        
        setViewControllers([setupViewController(with: images.first!, title: titles.first!, description: descriptions.first!, index: 0)!], direction: .forward, animated: true, completion: nil)
    }

    //MARK: - Methods
    
    private func setupViewController(with image: UIImage, title: String, description: OnboardingText, index: Int) -> UIViewController? {
        guard let onBoardingImageViewController = UIStoryboard.viewController(withIdentifier: "OnBoardingImageViewController", storyBoardIdentifier: "OnBoardingViewController") as? OnBoardingImageViewController else { return nil }
        onBoardingImageViewController.onBoardingImage = image
        onBoardingImageViewController.onBoardingTitle = title
        onBoardingImageViewController.onBoardingDescription = description.message
        onBoardingImageViewController.index = index
        return onBoardingImageViewController
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = #colorLiteral(red: 0.5215686275, green: 0.7215686275, blue: 0.8, alpha: 1)
        appearance.currentPageIndicatorTintColor = #colorLiteral(red: 0.6196078431, green: 0.5294117647, blue: 0.7568627451, alpha: 1)
        appearance.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.9725490196, blue: 1, alpha: 1)
    }
    
    //MARK: - UIPageViewControllerDataSource
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let onBoardingImageViewController = pageViewController.viewControllers?.first as? OnBoardingImageViewController else { return nil }
        guard var previousIndex = onBoardingImageViewController.index else { return nil }
        
        if ((previousIndex == 0) || (previousIndex == NSNotFound)) { return nil }
        
        previousIndex -= 1
        
        return setupViewController(with: images[previousIndex], title: titles[previousIndex], description: descriptions[previousIndex], index: previousIndex)
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let onBoardingImageViewController = pageViewController.viewControllers?.first as? OnBoardingImageViewController else { return nil }
        guard var nextIndex = onBoardingImageViewController.index else { return nil }
        
        if (nextIndex == NSNotFound) { return nil }
        nextIndex += 1
        if (nextIndex == images.count) { return nil }
        
        return setupViewController(with: images[nextIndex], title: titles[nextIndex], description: descriptions[nextIndex], index: nextIndex)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        setupPageControl()
        return images.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let onBoardingImageViewController = pageViewController.viewControllers?.first as? OnBoardingImageViewController else { return 0 }
        guard let index = onBoardingImageViewController.index else { return 0 }
        return index
    }
    
    
}
