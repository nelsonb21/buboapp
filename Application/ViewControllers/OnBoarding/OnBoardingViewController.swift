//
//  OnBoardingViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/14/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

class OnBoardingViewController: AppViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setupView() {
        Analytics.logEvent(with: .onboarding)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //MARK: IBActions
    
    @IBAction func signInButtonTapped(_ sender: UIButton) {
        guard let signInNavController = UIStoryboard.navigationController(withIdentifier: "UserSignInNavController", storyBoardIdentifier: "UserSignInViewController") else { return }
        present(signInNavController, animated: true, completion: nil)
    }
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        guard let signUpNavController = UIStoryboard.navigationController(withIdentifier: "UserSignUpNavController", storyBoardIdentifier: "UserSignUpViewController") else { return }
        present(signUpNavController, animated: true, completion: nil)
        
    }

}
