//
//  Photo.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/7/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

enum AssetType: String {
    case isVideo
    case isImage
}

@objcMembers
class Photo: NSObject {
    var objectId: String?
    var title: String?
    var location: GeoPoint?
    var imageUrl: String?
    var username: String?
    var datetime: Date?
    var address: String?
    var user: BackendlessUser?
    var isVideo: String?
    var videoThumbnail: String?
    var voiceNoteUrl: String?
    var city: String?
    var country: String?
}

class PhotoAnnotation: ARAnnotation {
    
    var imageUrl: String?
    var username: String?
    var datetime: Date?
    
    init?(location: CLLocation, imageUrl: String, username: String, datetime: Date, title: String) {
        self.imageUrl = imageUrl
        self.username = username
        self.datetime = datetime
        
        super.init(identifier: nil, title: title, location: location)
        self.identifier = identifier
        self.title = title
        self.location = location
    }
    
    init?(photo: Photo) {
        let isVideoNil = photo.isVideo == nil ? false : true
        let isVideo = isVideoNil && photo.isVideo! == AssetType.isVideo.rawValue ? true : false
        
        self.imageUrl = isVideo ? photo.videoThumbnail : photo.imageUrl
        self.username = photo.username
        self.datetime = photo.datetime
        
        let title = photo.title
        guard let latitude = photo.location?.latitude as? CLLocationDegrees else { return nil }
        guard let longitude = photo.location?.longitude as? CLLocationDegrees else { return nil }
        let coordinates = CLLocation(latitude: latitude, longitude: longitude)
        
        super.init(identifier: nil, title: title, location: coordinates)
        self.identifier = identifier
        self.title = title
        self.location = coordinates
    }
    
}
