//
//  InstructionsMessages.swift
//  Bubo
//
//  Created by Nelson Bolivar on 9/21/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import BonMot

enum InstructionsMessages {
    case noPhotosAround
    case noLocationAllowed
    
    var message: NSAttributedString {
        switch self {
        case .noPhotosAround:
            let strings = ["No photos around.".styled(with: MessageStyles.purple),
                           " Be the first one to post here!".styled(with: MessageStyles.white)]
            return NSAttributedString().appendAttributedString(strings)
        case .noLocationAllowed:
            let strings = ["Please authorize".styled(with: MessageStyles.white),
                           " Bubo ".styled(with: MessageStyles.purple),
                           "to use your location while using the app.".styled(with: MessageStyles.white)]
            return NSAttributedString().appendAttributedString(strings)
        }
    }
    
}

struct MessageStyles {
    
    static let purple = StringStyle(
        .font(UIFont(name: "CaviarDreams-Bold", size: 18)!),
        .color(#colorLiteral(red: 0.6196078431, green: 0.5294117647, blue: 0.7568627451, alpha: 1)),
        .alignment(.center)
    )
    
    static let black = StringStyle(
        .font(UIFont(name: "CaviarDreams", size: 18)!),
        .color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)),
        .alignment(.center)
    )
    
 static let white = StringStyle(
        .font(UIFont(name: "CaviarDreams", size: 18)!),
        .color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)),
        .alignment(.center)
    )
    
}
