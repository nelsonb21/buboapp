//
//  LikePeople.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

@objcMembers
class LikePeople: NSObject {

    var objectId: String?
    var userLiking: BackendlessUser?
    var userLiked: BackendlessUser?
    var datetime: Date?
    
}
