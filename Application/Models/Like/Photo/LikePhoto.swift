//
//  LikePhoto.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/22/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

@objcMembers
class LikePhoto: NSObject {

    var objectId: String?
    var user: BackendlessUser?
    var photo: Photo?
    var datetime: Date?
    
}
