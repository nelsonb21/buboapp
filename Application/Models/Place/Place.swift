//
//  Place.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

@objcMembers
class Place: NSObject {
    
    var objectId: String?
    var name: String?
    var country: String?
    var city: String?
    var street: String?
    var coordinates: GeoPoint?
    var user: BackendlessUser?
    var imageUrl: String?

}
