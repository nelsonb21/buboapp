//
//  ReportPost.swift
//  Bubo
//
//  Created by Nelson Bolivar on 1/3/18.
//  Copyright © 2018 Nelson Bolivar. All rights reserved.
//

import UIKit

@objcMembers
class ReportUser: NSObject {
    
    override init() {
        super.init()
    }
    
    init(with userReported: BackendlessUser?, user: BackendlessUser?, reason: String?) {
        super.init()
        self.userReported = userReported
        self.reportedByUser = user
        self.reason = reason
    }
    
    var objectId: String?
    var userReported: BackendlessUser?
    var reportedByUser: BackendlessUser?
    var reason: String?
    var created: Date?
    
}
