//
//  ReportUser.swift
//  Bubo
//
//  Created by Nelson Bolivar on 1/10/18.
//  Copyright © 2018 Nelson Bolivar. All rights reserved.
//

import UIKit

@objcMembers
class ReportPost: NSObject {
    
    override init() {
        super.init()
    }
    
    init(with photo: Photo?, user: BackendlessUser?) {
        super.init()
        self.photo = photo
        self.reportedByUser = user
    }
    
    var objectId: String?
    var photo: Photo?
    var reportedByUser: BackendlessUser?
    var created: Date?
    
}
