//
//  User.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct User {
    
    static var current = getCurrent()
    
    private static func getCurrent() -> BackendlessUser {
        guard let data = UserDefaults().data(forKey: UserDefaultKey.userDataArray.rawValue) else { return BackendlessUser() }
        guard let properties = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: Any] else { return BackendlessUser() }
        return BackendlessUser(properties: properties)
    }
    
    static func getValue(key: Attributes) -> Any? {
        return current.getProperty(key.rawValue)
    }
    
    static func updateUserLocally(user: BackendlessUser) {
        let data = NSKeyedArchiver.archivedData(withRootObject: user.getProperties())
        UserDefaultWrapper.setData(value: data, key: .userDataArray)
    }
    
    static func deleteUserData() {
        UserDefaultWrapper.removeData(key: .userDataArray)
    }
    
    enum Attributes: String {
        case name
        case bio
        case website
        case profileImageUrl
        case osVersion
        case osType
        case phoneModel
        case appVersion
    }
    
}
