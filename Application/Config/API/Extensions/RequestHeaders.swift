//
//  RequestHeaders.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/12/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

extension URLRequest {
    
    mutating func addHTTPHeaders(_ headers: [String: String]?) {
        guard let headers = headers else { return }
        for (key, value) in headers {
            setValue(value, forHTTPHeaderField: key)
        }
    }
    
}
