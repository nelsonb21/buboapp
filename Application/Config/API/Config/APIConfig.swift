//
//  APIConfig.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/12/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import Alamofire

enum APIConfig {
    case `default`
    case mocky
    
    var source: (host: String, headers: [String: String]?) {
        switch self {
        case .default:
            return (host: "https://api.backendless.com/v1/", headers: nil)
        case .mocky:
            return (host: "http://www.mocky.io/", headers: nil)
        }
    }
}
