//
//  Router.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/12/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import Foundation
import Alamofire

protocol Router: URLRequestConvertible {
    var config: APIConfig { get }
    var query: APIQuery { get }
}

extension Router {
    
    //MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try config.source.host.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(query.path))
        urlRequest.httpMethod = query.httpMethod.rawValue
        urlRequest.addHTTPHeaders(config.source.headers)
        urlRequest.addHTTPHeaders(query.headers)
        let encoding = query.paremeterEncoding
        print (try encoding.encode(urlRequest, with: query.parameters))
        return try encoding.encode(urlRequest, with: query.parameters)
    }
    
}
