//
//  APIQuery.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/12/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import Alamofire

struct APIQuery {
    
    var headers = ["application-id": "1E2A2619-C7DC-8017-FF81-11EA1BC91C00",
                "secret-key": "9BFD0A61-4764-C030-FFA0-58D244AFAE00",
                "Content-Type": "application/json'",
                "application-type": "REST"]
    
    var httpMethod: Alamofire.HTTPMethod
    var path: String
    var parameters: [String: Any]?
    var queryItems: [String: String]?
    var paremeterEncoding: Alamofire.ParameterEncoding = URLEncoding.default
    var cachePolicy: String = ""
    
    init(httpMethod: Alamofire.HTTPMethod, path: String) {
        self.httpMethod = httpMethod
        self.path = path
    }
    
    init(httpMethod: Alamofire.HTTPMethod, path: String, parameters: [String: Any]?) {
        self.httpMethod = httpMethod
        self.path = path
        self.parameters = parameters
    }
    
}
