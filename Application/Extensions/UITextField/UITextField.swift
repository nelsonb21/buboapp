//
//  UITextField.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

let charactersetTextField = NSCharacterSet(charactersIn: " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-_*.")

extension UITextField {

    func isValidEmail() -> Bool {
        guard let text = text else { return false }
        if !text.isEmpty {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
            
            let isValid = emailTest.evaluate(with: text)
            changeBorderColor(isValid: isValid)
            return isValid
        }
        changeBorderColor(isValid: false)
        return false
    }
    
    func isValidUrl() -> Bool {
        if let urlString = text, !urlString.isEmpty {
            let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
            let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
            let isUrlCorrect = urlTest.evaluate(with: urlString)
            changeBorderColor(isValid: isUrlCorrect)
            return isUrlCorrect
        }
        changeBorderColor(isValid: true)
        return true
    }
    
    func isValidPassword() -> Bool {
        guard let text = text else { return false }
        let result = text.isEmpty ? false : validateCharacters()
        if result { return text.count >= 6 }
        changeBorderColor(isValid: result)
        return result
    }
    
    func validate(isOptional: Bool) -> Bool {
        guard let text = text else { return false }
        if isOptional {
            let isValid = text.isEmpty ? true : validateCharacters()
            changeBorderColor(isValid: isValid)
            return isValid
        } else {
            let isValid = text.isEmpty ? false : validateCharacters()
            changeBorderColor(isValid: isValid)
            return isValid
        }
    }
    
    private func validateCharacters() -> Bool {
        return text!.rangeOfCharacter(from: charactersetTextField.inverted) == nil
    }
    
    private func changeBorderColor(isValid: Bool) {
        layer.borderColor = isValid ? #colorLiteral(red: 0.5019607843, green: 0.3960784314, blue: 1, alpha: 1).cgColor : UIColor.red.cgColor
    }
    
}
