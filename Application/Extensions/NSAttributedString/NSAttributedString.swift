//
//  NSAttributedString.swift
//  Bubo
//
//  Created by Nelson Bolivar on 10/10/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension NSAttributedString {

    func appendAttributedString(_ strings: [NSAttributedString]) -> NSAttributedString {
        let message = NSMutableAttributedString()
        strings.forEach { (string) in
            message.append(string)
        }
        return message
    }
    
}
