//
//  Storyboard.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 11/13/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static func viewController(withIdentifier identifier: String) -> UIViewController {
        return self.viewController(withIdentifier: identifier, storyBoardIdentifier: identifier)
    }
    
    static func viewController(withIdentifier identifier: String, storyBoardIdentifier: String) -> UIViewController {
        let storyBoard = UIStoryboard(name: storyBoardIdentifier, bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: identifier)
    }
    
    static func navigationController(withIdentifier identifier: String, storyBoardIdentifier: String) -> UINavigationController? {
        let storyBoard = UIStoryboard(name: storyBoardIdentifier, bundle: nil)
        guard let navigationController = storyBoard.instantiateViewController(withIdentifier: identifier) as? UINavigationController else { return nil }
        return navigationController
    }
    
}
