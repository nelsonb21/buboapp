//
//  UIViewController.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 3/5/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // MARK: - Alerts
   /*
    func showDefaultErrorAlert(_ completion: AppCompletion? = nil) {
        showErrorAlert("Something went wrong", completion: completion)
    }
    */
    func showErrorAlert(_ message: String, completion: AppCompletion? = nil) {
        showAlert("Error", message: message, completion: completion)
    }
    
    func showAlert(_ title: String? = nil,
                   message: String,
                   actionTitle: String = "OK",
                   actionStyle: UIAlertActionStyle = .default,
                   cancelTitle: String? = nil,
                   completion: AppCompletion? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: actionTitle, style: actionStyle, handler: nil))
        
        if (cancelTitle != nil) {
            alertController.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: nil))
        }
        self.present(alertController, animated: true, completion: nil)
    }

    @nonobjc static var topViewController: UIViewController? {
        var topViewController = UIApplication.shared.windows.first?.rootViewController
        while topViewController?.presentedViewController != nil {
            topViewController = topViewController?.presentedViewController
        }
        return topViewController
    }
    
    func setupStatusBar(with color: UIColor) {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = color
        }
    }
    
}
