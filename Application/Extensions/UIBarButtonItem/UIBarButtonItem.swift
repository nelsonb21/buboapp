//
//  UIBarButtonItem.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 5/6/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    convenience init (image: UIImage?, target: AnyObject?, action: Selector) {
        self.init(image: image, target: target, action: action, title: "")
    }
    
    convenience init (image: UIImage?, target: AnyObject?, action: Selector, title: String?) {
        var rect = CGRect(x: 0, y: 0, width: 0, height: 0)
        if let image = image {
            rect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        }
        self.init(image: image, target: target, action: action, title: title, frame: rect)
    }
    
    convenience init (image: UIImage?, target: AnyObject?, action: Selector, title: String?, frame: CGRect) {
        let barButton = UIButton(type: .custom)
        var frame = frame
        barButton.setTitle(title, for: .normal)
        barButton.setImage(image, for: .normal)
        barButton.setImage(image, for: .highlighted)
        barButton.setImage(image, for: .selected)
        barButton.addTarget(target, action: action, for: .touchUpInside)
        if let title = title, let font = barButton.titleLabel?.font {
            let titleSize = title.size(withAttributes: [NSAttributedStringKey.font: font])
            frame.size.width += titleSize.width
            frame.size.height = frame.size.height > 0 ? frame.size.height : titleSize.height
        }
        barButton.frame = frame
        self.init(customView: barButton)
    }
    
    var customButton: UIButton? {
        return customView as? UIButton
    }
}
