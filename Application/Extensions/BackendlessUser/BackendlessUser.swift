//
//  BackendlessUser.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension BackendlessUser {

    func getValue(key: User.Attributes) -> Any? {
        return getProperty(key.rawValue)
    }
}
