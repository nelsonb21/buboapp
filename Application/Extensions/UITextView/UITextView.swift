//
//  UITextView.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/28/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

let charactersetTextView = NSCharacterSet(charactersIn: " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-_!\"#@$%&(){}?¿=*+.,:\\")

extension UITextView {
    
    func validate(isOptional: Bool) -> Bool {
        if isOptional {
            return text!.isEmpty ? true : validateCharacters()
        } else {
            return text!.isEmpty ? false : validateCharacters()
        }
    }
    
    private func validateCharacters() -> Bool {
        return true //text!.rangeOfCharacter(from: charactersetTextView.inverted) == nil
    }

}
