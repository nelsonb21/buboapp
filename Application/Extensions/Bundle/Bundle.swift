//
//  Bundle.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 6/10/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension Bundle {

    var appVersion: String {
        guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else { return "" }
        return version
    }
    
    var appBuild: String {
        guard let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else { return "" }
        return build
    }
    
    var fullAppVersion: String {
        return appVersion + "." + appBuild
    }
    
}
