//
//  UIImageView.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 12/11/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {

    func setImageKF(with stringUrl: String) {
        guard let url = URL(string: stringUrl) else { return }
        self.kf.setImage(with: url)
    }
    
    func setImage(with url: URL) {
        self.kf.setImage(with: url)
    }
    
    func setImage(with url: URL, placeholder: UIImage) {
        self.kf.setImage(with: url, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func setForceRefreshImage(with url: URL, placeholder: UIImage?) {
        self.kf.setImage(with: url, placeholder: placeholder, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
    }
    
    func setupGif() {
        guard let urlString = Bundle.main.path(forResource: "loader", ofType: "gif") else { return }
        let url = URL(fileURLWithPath: urlString)
        let data = try! Data(contentsOf: url)
        self.kf.indicatorType = .image(imageData: data)
        self.kf.setImage(with: url)
    }
    
    func startLoadingAnimation() {
        self.isHidden = false
    }
    
    func stopLoadingAnimation() {
        self.isHidden = true
    }
    
}
