//
//  UICollectionView.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 12/11/16.
//  Copyright © 2016 Nelson Bolivar. All rights reserved.
//

import UIKit

extension UICollectionView {

    func register(withIdentifier identifier: String) {
        self.register(withIdentifier: identifier, nibIdentifier: identifier)
    }
    
    func register(withIdentifiers identifiers: [String]) {
        for identifier in identifiers {
            self.register(withIdentifier: identifier, nibIdentifier: identifier)
        }
    }
    
    func register(withIdentifier identifier: String, nibIdentifier: String) {
        self.register(UINib(nibName: nibIdentifier, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
    
    
    func deselectAllItems(animated: Bool = false) {
        for indexPath in self.indexPathsForSelectedItems ?? [] {
            self.deselectItem(at: indexPath, animated: animated)
        }
    }
    
}
