//
//  GeoPoint.swift
//  MapMyPhoto
//
//  Created by Nelson Bolivar on 2/12/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

extension GeoPoint {
    
    func getGeopoint(with coordinates: CLLocationCoordinate2D) -> GeoPoint {
        return GeoPoint.geoPoint( GEO_POINT(latitude: coordinates.latitude, longitude: coordinates.longitude), categories: ["Bubo"], metadata: [:]) as! GeoPoint
    }

}
